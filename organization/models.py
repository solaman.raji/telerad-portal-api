import uuid

from django.contrib.auth.models import User
from django.db import models
from django_countries.fields import CountryField


def facility_logo_directory_path(instance, filename):
    return 'facility_logo/{guid}/{filename}'.format(guid=instance.guid, filename=filename)


def user_signature_directory_path(instance, filename):
    return 'user_signature/{guid}/{filename}'.format(guid=instance.guid, filename=filename)


class Facility(models.Model):
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    phone = models.CharField(max_length=100, null=True, blank=True)
    email = models.CharField(max_length=100, null=True, blank=True)
    address1 = models.TextField(null=True, blank=True)
    address2 = models.TextField(null=True, blank=True)
    postal_code = models.CharField(max_length=100, null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    state = models.CharField(max_length=100, null=True, blank=True)
    country = CountryField(null=True, blank=True)
    routine = models.IntegerField(null=True, blank=True)
    urgent = models.IntegerField(null=True, blank=True)
    logo = models.FileField(
        upload_to=facility_logo_directory_path,
        max_length=255,
        null=True,
        blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'facilities'
        verbose_name_plural = 'Facilities'

    def __str__(self):
        return self.name


class UserProfile(models.Model):
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    user = models.OneToOneField(
        User,
        related_name='profile',
        on_delete=models.CASCADE
    )
    facility = models.ForeignKey(
        Facility,
        related_name='users',
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )
    phone = models.CharField(max_length=20, blank=True, null=True)
    signature = models.FileField(
        upload_to=user_signature_directory_path,
        max_length=255,
        null=True,
        blank=True
    )

    class Meta:
        db_table = 'user_profiles'
        verbose_name_plural = 'User Profiles'

    def __str__(self):
        return self.user.username

    @property
    def associated_with_facility(self):
        return self.facility_id is not None


class DestinationFacility(models.Model):
    facility = models.ForeignKey(
        Facility,
        related_name='destination_facilities',
        on_delete=models.CASCADE
    )
    destination_facility = models.ForeignKey(
        Facility,
        related_name='facility',
        on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'destination_facilities'
        verbose_name_plural = 'Destination Facilities'

    def __str__(self):
        return self.destination_facility.name


class Procedure(models.Model):
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'procedures'
        verbose_name_plural = 'Procedures'

    def __str__(self):
        return self.name


class IndependentRadiologistFacility(models.Model):
    independent_radiologist = models.ForeignKey(
        User,
        related_name='facilities',
        on_delete=models.CASCADE
    )
    facility = models.ForeignKey(
        Facility,
        related_name='independent_radiologists',
        on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'independent_radiologist_facilities'
        verbose_name_plural = 'Independent Radiologist Facilities'

    def __str__(self):
        return self.independent_radiologist.username


class BodyPart(models.Model):
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'body_parts'
        verbose_name_plural = 'Body Parts'

    def __str__(self):
        return self.name
