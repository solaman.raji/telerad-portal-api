from django.contrib.auth.models import Group
from django.test import TestCase

from organization.model_utils import create_physician
from organization.models import Facility
from organization.views import ConfigurationList
from telerad_portal_api.utils import GroupName


class PhysicianListOutputTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Group.objects.get_or_create(name=GroupName.PHYSICIAN.value)
        cls.facility = Facility.objects.create(name='Facility 1')
        cls.physician_1 = create_physician(cls.facility, 'physician1', 'f1', 'l1')
        cls.physician_2 = create_physician(cls.facility, 'physician2', 'f2', 'l2')

    def test_physician_list(self):
        physician_config_dict = ConfigurationList().get_physicians(self.facility)
        self.assertDictEqual(physician_config_dict, {str(self.physician_1.profile.guid): 'f1 l1',
                                                     str(self.physician_2.profile.guid): 'f2 l2',
                                                     })
