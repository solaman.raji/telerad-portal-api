from django.test import SimpleTestCase

from telerad_portal_api.utils import GroupName


class TestPhysicianGroup(SimpleTestCase):
    def test_radiologist_group_name_available(self):
        _RADIOGRAPHER = 'RADIOGRAPHER'
        assert hasattr(GroupName, _RADIOGRAPHER)
        assert GroupName[_RADIOGRAPHER].name == _RADIOGRAPHER
        assert GroupName[_RADIOGRAPHER].value == 'Radiographer'
        assert GroupName.get_group_name_short_form(GroupName.RADIOGRAPHER.value) == 'RG'

    def test_physician_group_name_available(self):
        _PHYSICIAN = 'PHYSICIAN'
        assert hasattr(GroupName, _PHYSICIAN)
        assert GroupName[_PHYSICIAN].name == _PHYSICIAN
        assert GroupName[_PHYSICIAN].value == 'Physician'
        assert GroupName.get_group_name_short_form(GroupName.PHYSICIAN.value) == 'PHY'
