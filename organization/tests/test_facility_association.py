from django.contrib.auth.models import User
from django.test import TestCase

from organization.models import Facility, UserProfile


class TestUserAssociation(TestCase):
    def setUp(self):
        self.facility = Facility.objects.create(name='Test Facility')
        self.facility.save()
        self.user = User.objects.create(username='username')
        self.user.save()
        self.profile = UserProfile.objects.create(user=self.user)
        self.profile.save()

    def tearDown(self):
        self.profile.delete()
        self.user.delete()
        self.facility.delete()

    def test_independent_user(self):
        assert self.profile.associated_with_facility is False

    def test_facility_user(self):
        self.profile.facility = self.facility
        self.profile.save()
        assert self.profile.associated_with_facility is True