import logging

from django.contrib.auth.models import User, Group

from organization.models import UserProfile, Facility
from telerad_portal_api.utils import GroupName

logger = logging.getLogger(__name__)


def create_facility_user(facility, username, group_name, **kwargs):
    assert group_name in [GroupName.PHYSICIAN.value,
                          GroupName.RADIOGRAPHER.value,
                          GroupName.RADIOLOGIST.value,
                          GroupName.COORDINATOR.value,
                          ]
    # Add exception handling to avoid inconsistent state, rollback changes
    user = User.objects.create_user(
        username=username,
        **kwargs
    )
    UserProfile.objects.create(user=user, facility=facility)
    user.groups.add(Group.objects.get(name=group_name))

    return user


def create_physician(facility, username, first_name, last_name, **kwargs):
    return create_facility_user(facility, username, GroupName.PHYSICIAN.value,
                                first_name=first_name, last_name=last_name, **kwargs)


def create_radiographer(facility, username, **kwargs):
    return create_facility_user(facility, group_name=GroupName.RADIOGRAPHER.value, username=username, **kwargs)


def create_radiologist(facility, username, **kwargs):
    return create_facility_user(facility, group_name=GroupName.RADIOLOGIST.value, username=username, **kwargs)


def create_facility(facility_name, facility_guid=None):
    logger.info(f'Facility create request for input name: {facility_name}, guid: {facility_guid}')
    input_dict = dict(guid=facility_guid, name=facility_name) if facility_guid else dict(name=facility_name)
    facility, created = Facility.objects.get_or_create(**input_dict)

    create_status = 'created' if created else 'existed'
    logger.info(f'Facility create request for name: {facility_name}, guid: {facility_guid} is {create_status}')
    return facility
