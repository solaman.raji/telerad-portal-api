from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_jwt.views import (
    ObtainJSONWebToken,
    refresh_jwt_token,
    verify_jwt_token
)

from organization.serializers import CustomJWTSerializer
from organization.views import (
    CreateUser,
    CheckUser,
    ConfigurationList,
    ProcedureDetail,
    FacilityList,
    FacilityDetail,
    TimelineList
)

urlpatterns = [
    path('api-token-auth/', ObtainJSONWebToken.as_view(serializer_class=CustomJWTSerializer)),
    path('api-token-refresh/', refresh_jwt_token),
    path('api-token-verify/', verify_jwt_token),
    path('users/register/', CreateUser.as_view()),
    path('users/check-user/', CheckUser.as_view()),
    path('configurations/', ConfigurationList.as_view()),
    path('procedures/<uuid:guid>/', ProcedureDetail.as_view()),
    path('facilities/', FacilityList.as_view()),
    path('facilities/<uuid:guid>/', FacilityDetail.as_view()),
    path('timeline/', TimelineList.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
