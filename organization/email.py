import logging

from django.template.loader import get_template

logger = logging.getLogger(__name__)

_welcome_email_subject = 'AlemHealth Connect Invitation'

_welcome_email_body_template = '''Hi {full_name}, 

Welcome to AlemHealth Connect

Thank you for creating an account on AlemHealth Connect. Your login credentials to the AlemHealth Connect portal are as follows:

Username: {username}

Password: entered during sign up

Login URL:  [https://connect.alem.health](https://connect.alem.health)

**Any further questions?**

Our friendly Support team is only a quick email away! You can contact us at support@alemhealth.com or by replying to this email.

 

Looking forward to helping you transmit your images everywhere. 

Your friends at AlemHealth'''

WELCOME_EMAIL_SETTINGS = {
    'from': 'support@alemhealth.com',
    'subject': _welcome_email_subject,
    'body_template': _welcome_email_body_template
}


def get_html_welcome_email(user):
    template_location = 'welcome_email/signup.html'
    template = get_template(template_location)
    context = {'user': user}

    logger.info(f'Generate Welcom invitation html email for user {user}')
    html = template.render(context=context)
    logger.info('HTML file is generated')
    return html
