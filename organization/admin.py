from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.models import User
from django.forms.models import ModelChoiceField

from organization.models import (
    Facility,
    UserProfile,
    DestinationFacility,
    Procedure,
    IndependentRadiologistFacility,
    BodyPart
)
from telerad_portal_api.utils import GroupName


class FacilityAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'guid',
        'name',
        'phone',
        'email',
        'address1',
        'address2',
        'postal_code',
        'city',
        'state',
        'country',
        'routine',
        'urgent'
    )


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'guid', 'user', 'facility')


class UserProfileInline(admin.StackedInline):
    model = UserProfile


class UserAdmin(AuthUserAdmin):
    inlines = [UserProfileInline]
    search_fields = AuthUserAdmin.search_fields + ('profile__facility__name',)
    list_filter = AuthUserAdmin.list_filter + ('profile__facility',)


class DestinationFacilityAdmin(admin.ModelAdmin):
    list_display = ('id', 'facility', 'destination_facility')


class ProcedureAdmin(admin.ModelAdmin):
    list_display = ('id', 'guid', 'name', 'description')


class IndependentRadiologistFacilityAdmin(admin.ModelAdmin):
    list_display = ('id', 'independent_radiologist', 'facility')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "independent_radiologist":
            queryset = User.objects.filter(
                groups__name=GroupName.INDEPENDENT_RADIOLOGIST.value
            )
            return ModelChoiceField(queryset)
        else:
            return super(IndependentRadiologistFacilityAdmin, self).formfield_for_foreignkey(
                db_field,
                request,
                **kwargs
            )


class BodyPartAdmin(admin.ModelAdmin):
    list_display = ('id', 'guid', 'name')


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Facility, FacilityAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(DestinationFacility, DestinationFacilityAdmin)
admin.site.register(Procedure, ProcedureAdmin)
admin.site.register(IndependentRadiologistFacility, IndependentRadiologistFacilityAdmin)
admin.site.register(BodyPart, BodyPartAdmin)
