import logging
import uuid
from datetime import timedelta

from django.contrib.auth.models import User, Group
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.core.validators import validate_email
from django.template.loader import get_template
from django.utils import timezone
from django.utils.decorators import method_decorator
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from assignment.models import Assignment
from assignment.utils import StatusChoice as AssignmentStatusChoice
from organization.decorators import check_group_validation, check_user_profile
from organization.models import (
    UserProfile,
    Facility,
    Procedure,
    IndependentRadiologistFacility,
    BodyPart,
    DestinationFacility as DestinationFacilityModel
)
from organization.serializers import (
    UserSerializer,
    DestinationFacilityListSerializer,
    RadiologistListSerializer,
    PhysicianListSerializer,
    ProcedureListSerializer,
    FacilityListSerializer,
    IndependentRadiologistListSerializer,
    BodyPartListSerializer,
    SourceFacilityListSerializer
)
from report.utils import ActionableFindingsChoice
from study.models import Study
from study.utils import (
    GenderChoice,
    ModalityChoice,
    PriorityChoice,
    SLACountdown,
    StatusChoice as StudyStatusChoice
)
from telerad_portal_api import settings
from telerad_portal_api.custom_exception import (
    AlreadyExists,
    DoesNotExist,
    InvalidParameter,
    UnauthorizedAccess
)
from telerad_portal_api.settings import (
    DEFAULT_FACILITY_GUID,
    DEFAULT_FACILITY_NAME,
    SAMPLE_STUDY_CONFIG,
)
from telerad_portal_api.utils import (
    GroupName,
    ViewMethod,
    get_group_name,
    group_validation,
    serializer_error_mapping,
    get_object_by_guid
)
from users.models import Invitation
from .email import WELCOME_EMAIL_SETTINGS as _SETTINGS, get_html_welcome_email

logger = logging.getLogger(__name__)


class CreateUser(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        email = request.data.get('email')
        phone = request.data.get('phone')
        user_group = request.data.get('user_group')
        facility = request.data.get('facility')
        invitations = request.data.get('invitations')
        facility_related_group = [
            GroupName.RADIOGRAPHER.value,
            GroupName.COORDINATOR.value,
            GroupName.RADIOLOGIST.value
        ]

        if user_group:
            try:
                group = Group.objects.get(name=user_group)
            except Group.DoesNotExist:
                raise DoesNotExist("Group '{name}' does not exist.".format(name=user_group))
        else:
            raise InvalidParameter("user_group field is required")

        if User.objects.filter(email=email).exists():
            raise AlreadyExists('User with this email already exists.')

        if user_group in facility_related_group:
            if facility:
                if not facility['name'] or not facility['phone'] or not facility['email']:
                    raise InvalidParameter("Facility name, phone, email are required fields.")

                try:
                    facility_obj = Facility.objects.create(
                        name=facility['name'],
                        phone=facility['phone'],
                        email=facility['email']
                    )
                except Exception as e:
                    raise APIException(str(e))
            else:
                raise InvalidParameter("Facility object is required.")

        serializer = UserSerializer(data=request.data)

        try:
            if serializer.is_valid(raise_exception=True):
                serializer.save()

                user = User.objects.get(id=serializer.data['id'])
                user.groups.add(group)

                if user_group in facility_related_group:
                    user_profile = UserProfile(user=user, phone=phone, facility=facility_obj)
                elif user_group == GroupName.INDEPENDENT_RADIOLOGIST.value:
                    user_profile = UserProfile(user=user, phone=phone)
                else:
                    raise UnauthorizedAccess()

                user_profile.save()

                if group.name in facility_related_group:
                    if invitations:
                        for invitation in invitations:
                            invitation_email = invitation['email']
                            invitation_user_group = invitation['user_group']

                            if invitation_email and invitation_user_group:
                                email_validation = True
                                user_group_validation = True

                                try:
                                    validate_email(invitation_email)
                                except ValidationError:
                                    email_validation = False

                                try:
                                    Group.objects.get(name=user_group)
                                except Group.DoesNotExist:
                                    user_group_validation = False

                                if email_validation and user_group_validation:
                                    try:
                                        invitation_obj = Invitation.objects.create(
                                            first_name=invitation['first_name'],
                                            last_name=invitation['last_name'],
                                            email=invitation_email,
                                            phone=invitation['phone'],
                                            user_group=GroupName(invitation_user_group).name,
                                            facility=facility_obj,
                                            expire_time=timezone.now() + timedelta(days=3),
                                            created_by=user
                                        )
                                        self.send_invitation_email(invitation_obj)
                                    except Exception as e:
                                        raise APIException(str(e))
                elif group.name == GroupName.INDEPENDENT_RADIOLOGIST.value:
                    self.create_independent_radiologist_facility(user)
                    self.create_assgined_study(user)
                    self.create_my_uploaded_study(user)
                    self.send_welcome_email(user)

                status_code = status.HTTP_201_CREATED
                data = serializer.data
                data.pop("id")
                data["guid"] = str(user_profile.guid)
                data["phone"] = user_profile.phone
                data["user_group"] = get_group_name(user)

                if user_group in facility_related_group:
                    data['facility'] = {
                        "guid": str(facility_obj.guid),
                        "name": facility_obj.name,
                        "phone": facility_obj.phone,
                        "email": facility_obj.email
                    }
        except Exception as e:
            data = {
                "error": {
                    "message": serializer_error_mapping(serializer.errors) if serializer.errors else str(e)
                }
            }
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(data, status=status_code)

    def create_independent_radiologist_facility(self, user):
        IndependentRadiologistFacility.objects.create(
            independent_radiologist=user,
            facility=Facility.objects.get(guid=DEFAULT_FACILITY_GUID)
        )

    # FIXME: https://github.com/Miserlou/Zappa#asynchronous-task-execution
    def create_assgined_study(self, user):
        sample_study_conf = SAMPLE_STUDY_CONFIG[0]
        study = Study.objects.create(
            guid=uuid.uuid4(),
            study_instance_uid=sample_study_conf["uid"],
            modality=sample_study_conf["modality"],
            patient_name="{facility_name} Patient 1".format(
                facility_name=DEFAULT_FACILITY_NAME
            ),
            facility=Facility.objects.get(guid=DEFAULT_FACILITY_GUID),
            status=StudyStatusChoice.assigned.name,
            zip_file=self._zip_file_location(sample_study_conf)
        )
        Assignment.objects.create(
            study=study,
            independent_radiologist=user,
            status=AssignmentStatusChoice.assigned.name
        )

    # FIXME: https://github.com/Miserlou/Zappa#asynchronous-task-execution
    def create_my_uploaded_study(self, user):
        sample_study_conf = SAMPLE_STUDY_CONFIG[1]
        Study.objects.create(
            guid=uuid.uuid4(),
            modality=sample_study_conf["modality"],
            study_instance_uid=sample_study_conf["uid"],
            patient_name="{facility_name} Patient 1".format(
                facility_name=DEFAULT_FACILITY_NAME
            ),
            facility_name="Sample Facility",
            facility_email="sample_facility@alemcloud.com",
            user=user,
            status=StudyStatusChoice.assigned.name,
            zip_file=self._zip_file_location(sample_study_conf)
        )

    @staticmethod
    def _zip_file_location(sample_study_conf):
        return {
            "bucket": sample_study_conf['bucket'],
            "key": sample_study_conf['zip_object_key'],
        }

    # FIXME: https://github.com/Miserlou/Zappa#asynchronous-task-execution
    @classmethod
    def send_welcome_email(cls, user):

        email_body = _SETTINGS['body_template'].format(full_name=user.get_full_name(), username=user.username)

        logger.info(f'Send welcome email to user: {user.email} ')
        try:
            _html_message = get_html_welcome_email(user)
            user.email_user(_SETTINGS['subject'], email_body, _SETTINGS['from'], html_message=_html_message)
        except Exception:
            logger.exception(f'Failed to send email to user {user}')

    @classmethod
    def send_invitation_email(cls, invitation):
        template_location = 'invitation_email/invitation.html'
        template = get_template(template_location)
        context = {
            'user': {
                'name': " ".join([invitation.first_name, invitation.last_name]).strip(),
                'group': GroupName[invitation.user_group].value,
            },
            'facility': {
                'name': invitation.facility.name
            },
            'invitation': {
                'link': settings.INVITATION_URL.format(
                    invitation_token=invitation.guid
                )
            }
        }
        html = template.render(context=context)

        try:
            send_mail(
                subject=_SETTINGS['subject'],
                message="",
                from_email=_SETTINGS['from'],
                recipient_list=[invitation.email],
                html_message=html
            )
        except Exception:
            logger.exception('Failed to send email to {invitation_email}'.format(
                invitation_email=invitation.email
            ))


class CheckUser(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        email = request.data.get('email')

        if email:
            user = User.objects.filter(email=email).first()

            if user is not None:
                if hasattr(user, 'profile') and user.profile:
                    data = {
                        "first_name": user.first_name,
                        "last_name": user.last_name,
                        "email": user.email,
                        "group": get_group_name(user)
                    }

                    if hasattr(user.profile, 'facility') and user.profile.facility:
                        facility = {
                            "guid": user.profile.facility.guid,
                            "name": user.profile.facility.name
                        }
                        data['facility'] = facility

                    return Response(data, status=status.HTTP_200_OK)
                else:
                    raise APIException('User does not have a profile.')
            else:
                msg = 'User with this email does not exist.'
                raise DoesNotExist(msg)
        else:
            raise InvalidParameter()


class ConfigurationList(APIView):
    permission_classes = (IsAuthenticated,)

    @method_decorator(check_user_profile)
    def get(self, request):
        group_name = get_group_name(request.user)
        data = {
            "gender": dict(GenderChoice.choices()),
            "modality": dict(ModalityChoice.choices()),
            "priority": self.get_priority(request.user, group_name, dict(PriorityChoice.choices())),
            "body_part": self.get_body_part(),
            "actionable_findings": ActionableFindingsChoice.dict(),
        }

        authorized_group_list = [
            GroupName.RADIOGRAPHER.value,
            GroupName.RADIOLOGIST.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value,
            GroupName.COORDINATOR.value,
            GroupName.PHYSICIAN.value
        ]

        if not group_validation(request.user, authorized_group_list):
            from django import http
            return http.HttpResponseForbidden(f'User {request.user.username} is not included in allowed groups.')

        if group_name == GroupName.RADIOGRAPHER.value:
            data["procedure"] = ProcedureList.get_procedure_list_for_select_menu(
                ProcedureList()
            )
            data["current_facility"] = self.get_current_facility(request.user)
            data["destination_facility"] = self.get_destination_facility(request.user)
            data["radiologist"] = self.get_radiologist(request.user)
            data["independent_radiologist"] = self.get_independent_radiologist(request.user)
            data["physicians"] = self.get_physicians(request.user.profile.facility)
        elif group_name == GroupName.RADIOLOGIST.value:
            data["procedure"] = ProcedureList.get_procedure_list_for_select_menu(
                ProcedureList()
            )
            data["current_facility"] = self.get_current_facility(request.user)
            data["source_facility"] = self.get_source_facility(request.user)
            data["physicians"] = self.get_physicians(request.user.profile.facility)
        elif group_name == GroupName.INDEPENDENT_RADIOLOGIST.value:
            data["procedure"] = ProcedureList.get_procedure_list_for_select_menu(
                ProcedureList()
            )
        elif group_name == GroupName.COORDINATOR.value:
            data["radiologist"] = self.get_radiologist(request.user)

        return Response(data, status=status.HTTP_200_OK)

    def get_priority(self, user, group_name, priorities):
        for priority in priorities:
            data = {
                'label': priorities[priority]
            }

            if priority == PriorityChoice.routine.name:
                if group_name != GroupName.INDEPENDENT_RADIOLOGIST.value and user.profile.facility.routine:
                    data['sla_countdown'] = user.profile.facility.routine
                else:
                    data['sla_countdown'] = SLACountdown.ROUTINE.value
            elif priority == PriorityChoice.urgent.name:
                if group_name != GroupName.INDEPENDENT_RADIOLOGIST.value and user.profile.facility.urgent:
                    data['sla_countdown'] = user.profile.facility.urgent
                else:
                    data['sla_countdown'] = SLACountdown.URGENT.value

            priorities[priority] = data

        return priorities

    def get_current_facility(self, user):
        facility = user.profile.facility
        serializer = FacilityListSerializer(facility)
        return {str(serializer.data['guid']): serializer.data['name']}

    def get_source_facility(self, user):
        source_facility_list = DestinationFacilityModel.objects.filter(
            destination_facility=user.profile.facility
        )
        serializer = SourceFacilityListSerializer(source_facility_list, many=True)
        return self.format_list(serializer.data)

    def get_destination_facility(self, user):
        destination_facility_list = user.profile.facility.destination_facilities.all()
        serializer = DestinationFacilityListSerializer(destination_facility_list, many=True)
        return self.format_list(serializer.data)

    def get_radiologist(self, user):
        radiologist_list = User.objects.filter(
            groups__name=GroupName.RADIOLOGIST.value,
            profile__facility=user.profile.facility
        )
        serializer = RadiologistListSerializer(radiologist_list, many=True)
        return self.format_list(serializer.data)

    def get_independent_radiologist(self, user):
        independent_radiologist_list = user.profile.facility.independent_radiologists.all()
        serializer = IndependentRadiologistListSerializer(independent_radiologist_list, many=True)
        return self.format_list(serializer.data)

    @classmethod
    def get_physicians(cls, facility):
        physician_query = User.objects.filter(
            groups__name=GroupName.PHYSICIAN.value,
            profile__facility=facility
        )
        serializer = PhysicianListSerializer(physician_query, many=True)
        return cls.format_list(serializer.data)

    def get_body_part(self):
        body_part_list = BodyPart.objects.all()
        serializer = BodyPartListSerializer(body_part_list, many=True)
        return self.format_list(serializer.data)

    @classmethod
    def format_list(cls, data):
        return {str(obj['guid']): obj['name'] for obj in data}


class FacilityList(APIView):
    def get(self, request):
        facility_list = Facility.objects.all()
        serializer = FacilityListSerializer(facility_list, many=True)
        data = {
            "data": serializer.data
        }
        return Response(data, status=status.HTTP_200_OK)


class FacilityDetail(APIView):
    def get(self, request, guid):
        facility = get_object_by_guid(Facility, guid)
        serializer = FacilityListSerializer(facility)
        return Response(serializer.data, status=status.HTTP_200_OK)


class RadiologistDetail(APIView):
    def validate_radiologist(self, user, radiologist):
        validate_radiologist = True

        try:
            User.objects.get(
                groups__name=GroupName.RADIOLOGIST.value,
                profile__facility=user.profile.facility,
                profile__guid=radiologist.profile.guid
            )
        except Exception:
            validate_radiologist = False

        return validate_radiologist


class DestinationFacility(APIView):
    def validate_destination_facility(self, user, facility):
        validate_destination_facility = True

        try:
            destination_facility_list = user.profile.facility.destination_facilities.filter(
                destination_facility__guid=facility.guid
            )

            if not destination_facility_list:
                validate_destination_facility = False
        except Exception:
            validate_destination_facility = False

        return validate_destination_facility


class IndependentRadiologist(APIView):
    def validate_independent_radiologist(self, user, independent_radiologist):
        validate_independent_radiologist = True

        try:
            IndependentRadiologistFacility.objects.get(
                independent_radiologist=independent_radiologist,
                facility=user.profile.facility
            )
        except Exception:
            validate_independent_radiologist = False

        return validate_independent_radiologist


class ProcedureList(APIView):
    def get_procedure_list_for_select_menu(self):
        procedure_list = Procedure.objects.all()
        serializer = ProcedureListSerializer(procedure_list, many=True)
        return {str(obj['guid']): obj['name'] for obj in serializer.data}


class ProcedureDetail(APIView):
    permission_classes = (IsAuthenticated,)

    @check_group_validation(ViewMethod.PROCEDURE_FETCH.value)
    def get(self, request, guid):
        procedure = get_object_by_guid(Procedure, guid)
        serializer = ProcedureListSerializer(procedure)
        return Response(serializer.data, status=status.HTTP_200_OK)


class BodyPartDetail(APIView):
    def get_body_part_by_name(self, name):
        try:
            return BodyPart.objects.get(name=name)
        except BodyPart.DoesNotExist:
            return False


class TimelineList(APIView):
    permission_classes = (IsAuthenticated,)

    @check_group_validation(ViewMethod.TIMELINE_LIST.value)
    @method_decorator(check_user_profile)
    def get(self, request):
        data = []
        user = request.user
        group_name = get_group_name(user)

        if group_name == GroupName.COORDINATOR.value:
            target = user.profile.facility
        else:
            target = user

        activities = target.target_actions.filter(
            description="Notification Timeline"
        ).order_by('-timestamp')

        for activity in activities:
            activity_data = {
                "verb": activity.verb,
                "created_at": activity.timestamp.isoformat()
            }
            data.append(activity_data)

        return Response(data, status=status.HTTP_200_OK)
