import uuid

from django.core.management.base import BaseCommand, CommandError

from organization.models import Facility


class Command(BaseCommand):
    def handle(self, *args, **options):
        facility_name, facility_guid = options.get('name'), options.get('guid')
        try:
            self._create_facility(facility_name, facility_guid)
        except Exception as ex:
            raise CommandError('Exception occurs: %s' % ex)

    def _create_facility(self, facility_name, facility_guid):
        try:
            Facility.objects.get(guid=facility_guid)
            raise CommandError('Facility already exists with guid "%s"' % facility_guid)
        except Facility.DoesNotExist:
            facility = Facility(guid=facility_guid, name=facility_name)
            facility.save()

            self.stdout.write(self.style.SUCCESS(
                'Successfully create facility "%s" with guid %s' % (facility_name, facility_guid)
            ))

    def add_arguments(self, parser):
        parser.add_argument('name', type=str, help='Name of the facility')
        parser.add_argument('guid', type=uuid.UUID, help='Specify guid of the facility')
