from functools import wraps

from rest_framework import status
from rest_framework.response import Response

from telerad_portal_api.utils import (
    ViewMethod,
    group_validation,
    has_user_profile
)


def check_group_validation(view_method_name=None):
    authorized_group_list = ViewMethod.get_authorized_groups_by_view_method(view_method_name)

    def _check_group_validation(view_func):
        def _decorator(view, *args, **kwargs):
            if not group_validation(view.request.user, authorized_group_list):
                data = {
                    "error": {
                        "message": "Unauthorized access"
                    }
                }
                return Response(data, status=status.HTTP_401_UNAUTHORIZED)
            return view_func(view, *args, **kwargs)
        return wraps(view_func)(_decorator)
    return _check_group_validation


def check_user_profile(view_function):
    def wrap(request, *args, **kwargs):
        if not has_user_profile(request.user):
            data = {
                "error": {
                    "message": "User has no profile"
                }
            }
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return view_function(request, *args, **kwargs)
    wrap.__doc__ = view_function.__doc__
    wrap.__name__ = view_function.__name__
    return wrap

