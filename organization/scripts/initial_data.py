import logging

from django.contrib.auth.models import User, Group
from django.db.utils import IntegrityError

from telerad_portal_api.utils import GroupName

PASSWORD = "@AlemHealth#2018"

admin_users = [
    ('issa', 'issa@alemcloud.com', PASSWORD),
    ('ashfiqur', 'ashfiqur@alemcloud.com', PASSWORD),
    ('sajjad', 'sajjad@alemcloud.com', PASSWORD),
    ('solaman', 'solaman@alemcloud.com', PASSWORD),
    ('ishtiaque', 'ishtiaque@alemcloud.com', PASSWORD),
    ('admin', 'admin@alemcloud.com', PASSWORD),
]

logger = logging.getLogger(__name__)


def create_groups():
    logger.info('Create User groups')
    for _, value in GroupName.choices():
        logger.info(f'Get or Create group {value}')
        Group.objects.get_or_create(name=value)


def create_superusers():
    admin_group = Group.objects.get(name=GroupName.ADMIN.value)
    logger.info('Create Admin users')
    for (user, email, password) in admin_users:
        try:
            logger.info(f'Create admin user {user}({email})')
            user = User.objects.create_superuser(user, email, password)
            user.groups.add(admin_group)
        except IntegrityError as ex:
            logger.error(f'Admin user {user}({email}) already exists.')


def run():
    create_groups()
    create_superusers()
