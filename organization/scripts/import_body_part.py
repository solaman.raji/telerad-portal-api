import csv
import os

from organization.models import BodyPart

BODY_PART_FILEPATH = 'organization/scripts/body_part.csv'


def _import_bulk_body_part():
    for row in _read_body_part(BODY_PART_FILEPATH):
        _create_body_part(row['Name'])


def _read_body_part(filepath):
    with open(filepath) as fp:
        reader = csv.DictReader(fp)
        for row in reader:
            yield row


def _create_body_part(body_part):
    BodyPart.objects.create(name=body_part)
    print('Body part "{}" created'.format(body_part))


def run():
    print('Body part csv file path: {}'.format(os.path.exists(BODY_PART_FILEPATH)))
    _import_bulk_body_part()
