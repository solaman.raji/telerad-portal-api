import csv
import os

from organization.models import Procedure

PROCEDURE_FILEPATH = 'organization/scripts/procedure.csv'


def _import_bulk_procedure():
    for row in _read_procedure(PROCEDURE_FILEPATH):
        _create_procedure(row['Name'])


def _read_procedure(filepath):
    with open(filepath) as fp:
        reader = csv.DictReader(fp)
        for row in reader:
            yield row


def _create_procedure(procedure):
    Procedure.objects.create(name=procedure)
    print('Procedure "{}" created'.format(procedure))


def run():
    print('Procedure csv file path: {}'.format(os.path.exists(PROCEDURE_FILEPATH)))
    _import_bulk_procedure()
