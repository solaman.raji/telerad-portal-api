from django.contrib.auth.models import User, Group
from django.core import management

from organization.models import (
    Facility,
    DestinationFacility,
    UserProfile,
    IndependentRadiologistFacility
)
from telerad_portal_api.utils import GroupName

PASSWORD = "qweqwe"
FACILITY_LIST = [
]

FACILITY_NAMES = [f'Facility {i}' for i in range(1,6)]


def create_facility():
    for facility_name in FACILITY_NAMES:
        Facility.objects.get_or_create(name=facility_name)


def _get_sample_facility_query():
    return Facility.objects.filter(name__startswith='Facility')


def create_destination_facility():
    facility_ids = _get_sample_facility_query().values_list('id', flat=True)
    facility_id_list = sorted(list(facility_ids))
    facility_id_list_len = len(facility_id_list)

    for index, facility_id in enumerate(facility_id_list):
        facility_id_index = facility_id_list.index(facility_id)

        for i in range(1, 3):
            next_facility_id_index = facility_id_index + i

            if next_facility_id_index >= facility_id_list_len:
                next_facility_id_index -= facility_id_list_len

            destination_facility_id = facility_id_list[next_facility_id_index]
            facility = Facility.objects.get(id=facility_id)
            destination_facility = Facility.objects.get(id=destination_facility_id)
            destination_facility_obj = DestinationFacility(
                facility=facility,
                destination_facility=destination_facility
            )
            destination_facility_obj.save()


def create_user():
    facility_list = _get_sample_facility_query().all()
    group_list = Group.objects.exclude(name=GroupName.ADMIN.value)

    ir_counter = 1
    for facility in facility_list:
        for group in group_list:
            for i in range(1, 3):
                if group.name == GroupName.INDEPENDENT_RADIOLOGIST.value:
                    _create_ir_user(group, ir_counter)
                    ir_counter += 1
                else:
                    _create_facility_user(facility, group, i)


def _create_facility_user(facility, group, i):
    if group.name == GroupName.RADIOGRAPHER.value:
        group_short_name = "rg"
    elif group.name == GroupName.COORDINATOR.value:
        group_short_name = "cod"
    elif group.name == GroupName.RADIOLOGIST.value:
        group_short_name = "rl"
    elif group.name == GroupName.PHYSICIAN.value:
        group_short_name = "phy"
    else:
        group_short_name = "xx"
    username = "f{facility_counter}_{group_short_name}{i}".format(
        facility_counter=facility.name[-1],
        group_short_name=group_short_name,
        i=i
    )
    print(f'username: {username}')
    first_name = facility.name
    last_name = "{group_name} {i}".format(group_name=group.name, i=i)
    user = User.objects.create_user(
        username=username,
        first_name=first_name,
        last_name=last_name,
        password=PASSWORD
    )
    user_profile = UserProfile(user=user, facility=facility)
    user_profile.save()
    user.groups.add(group)


def _create_ir_user(group, ir_counter):
    group_short_name = "ir"
    username = "{group_short_name}{ir_counter}".format(
        group_short_name=group_short_name,
        ir_counter=ir_counter
    )
    print(f'username: {username}')
    first_name = "Global"
    last_name = "{group_name} {ir_counter}".format(group_name=group.name, ir_counter=ir_counter)
    user = User.objects.create_user(
        username=username,
        first_name=first_name,
        last_name=last_name,
        password=PASSWORD
    )
    user_profile = UserProfile(user=user)
    user_profile.save()

    user.groups.add(group)


def create_independent_radiologist_facility():
    facility_list = _get_sample_facility_query().all()
    independent_radiologist_list = list(User.objects.filter(
        groups__name=GroupName.INDEPENDENT_RADIOLOGIST.value
    ))

    for facility in facility_list:
        for i in range(0, 2):
            independent_radiologist = independent_radiologist_list.pop(0)
            independent_radiologist_facility = IndependentRadiologistFacility(
                independent_radiologist=independent_radiologist,
                facility=facility
            )
            independent_radiologist_facility.save()


def run():
    create_facility()
    for (name, guid) in FACILITY_LIST:
        management.call_command('create_facility', name, guid, verbosity=0)
    create_destination_facility()

    create_user()
    create_independent_radiologist_facility()
