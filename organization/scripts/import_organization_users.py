from django.contrib.auth.models import User, Group
from django.core import management

from organization.models import (
    Facility,
    DestinationFacility,
    UserProfile,
    IndependentRadiologistFacility
)
from telerad_portal_api.settings import DEFAULT_FACILITY_NAME, DEFAULT_FACILITY_GUID

PASSWORD = "qweqwe"
_JASLOK_GUID = '9d4145d8-ab8c-46f2-886d-d2a175a0d7ef'
_SEVENHILLS_GUID = 'f6f024e3-b24a-4640-b8ea-e174b9064297'
_RICHRDSON_GUID = 'b5e10cf8-ba56-42e0-b280-2d923199de8d'

FACILITY_LIST = [
    ('Jaslok Hospital[TEST]', _JASLOK_GUID),
    ('Sevenhills Radiology Center[TEST]', _SEVENHILLS_GUID),
    ('Richardson Medical Center [TEST]', _RICHRDSON_GUID),
    (DEFAULT_FACILITY_NAME, DEFAULT_FACILITY_GUID),
]

DESTINATION_FACILITIES = {
    _JASLOK_GUID: (_SEVENHILLS_GUID,),
}

# username, first_name, last_name, facility_guid, [groups]
USERS = [
    ('rg-jaslok', 'rg', 'jaslok', _JASLOK_GUID, ['Radiographer']),
    ('cod-sevenhills', 'cod', 'sevenhills', _SEVENHILLS_GUID, ['Coordinator']),
    ('rl-sevenhills', 'rl', 'sevenhills', _SEVENHILLS_GUID, ['Radiologist']),
    ('rg-richardson', 'Chris', 'Richardson', _RICHRDSON_GUID, ['Radiographer']),
    ('richard', 'Richard', "O'Dwyer", '', ['Independent Radiologist']),
    ('brutas', 'Brutas', 'Herophilus', _JASLOK_GUID, ['Physician']),
    ('quintus', 'Quintus', 'Servilius', _JASLOK_GUID, ['Physician']),
]

INDERAD_FACILITIES = {
    'richard': (_RICHRDSON_GUID,)
}


def create_facility():
    print('Create facilities')
    for (name, guid) in FACILITY_LIST:
        management.call_command('create_facility', name, guid, verbosity=0)


def create_destination_facility():
    print('Create Destination facilities')
    for source_facility_guid, destination_facility_guids in DESTINATION_FACILITIES.items():
        source_facility = Facility.objects.get(guid=source_facility_guid)
        for guid in destination_facility_guids:
            destination_facility = Facility.objects.get(guid=guid)
            DestinationFacility.objects.get_or_create(
                facility=source_facility,
                destination_facility=destination_facility
            )


def create_user():
    print('Create Users')
    for (username, first_name, last_name, facility_guid, groups) in USERS:
        user = User.objects.create_user(
            username=username,
            first_name=first_name,
            last_name=last_name,
            password=PASSWORD
        )

        facility = Facility.objects.get(guid=facility_guid) if facility_guid else None
        # user_profile = UserProfile(user=user)
        user_profile = UserProfile(user=user, facility=facility)
        user_profile.save()

        [user.groups.add(Group.objects.get(name=group_name)) for group_name in groups]


def create_independent_radiologist_facility():
    print('Create Independent radiologist facilities')
    for indi_rad, facility_guids in INDERAD_FACILITIES.items():
        independent_radiologist = User.objects.get(username=indi_rad)
        for guid in facility_guids:
            facility = Facility.objects.get(guid=guid)
            IndependentRadiologistFacility.objects.get_or_create(
                independent_radiologist=independent_radiologist,
                facility=facility
            )


def run():
    try:
        create_facility()
        create_destination_facility()

        create_user()
        create_independent_radiologist_facility()
    except Exception as ex:
        print(f'Exceptions occurred: {ex}')
        raise
