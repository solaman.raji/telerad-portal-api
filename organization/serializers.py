from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework import serializers
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.utils import jwt_encode_handler

from organization.models import DestinationFacility, Procedure, Facility
from telerad_portal_api.custom_jwt import jwt_payload_handler


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email', 'first_name', 'last_name')
        write_only_fields = ('password',)
        read_only_fields = ('id',)

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user

    def to_representation(self, obj):
        return {
            "id": obj.id,
            "username": obj.username,
            "first_name": obj.first_name,
            "last_name": obj.last_name,
            "email": obj.email
        }


class SourceFacilityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = DestinationFacility
        fields = '__all__'

    def to_representation(self, obj):
        return {
            "guid": obj.facility.guid,
            "name": obj.facility.name
        }


class DestinationFacilityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = DestinationFacility
        fields = '__all__'

    def to_representation(self, obj):
        return {
            "guid": obj.destination_facility.guid,
            "name": obj.destination_facility.name
        }


# FIXME: Duplicate code with RadiologistListSerializer
class PhysicianListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def to_representation(self, obj):
        return {
            "guid": obj.profile.guid,
            "name": obj.first_name + " " + obj.last_name
        }


class RadiologistListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def to_representation(self, obj):
        return {
            "guid": obj.profile.guid,
            "name": obj.first_name + " " + obj.last_name
        }


class IndependentRadiologistListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def to_representation(self, obj):
        return {
            "guid": obj.independent_radiologist.profile.guid,
            "name": obj.independent_radiologist.first_name + " " + obj.independent_radiologist.last_name
        }


class ProcedureListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Procedure
        fields = '__all__'

    def to_representation(self, obj):
        return {
            "guid": obj.guid,
            "name": obj.name,
            "description": obj.description
        }


class FacilityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
        fields = ('guid', 'name')


class BodyPartListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
        fields = ('guid', 'name')


class CustomJWTSerializer(JSONWebTokenSerializer):

    def validate(self, attrs):
        username = attrs.get("username")
        password = attrs.get("password")
        user_obj = User.objects.filter(Q(email=username) | Q(username=username)).first()

        if user_obj is not None:
            credentials = {
                'username': user_obj.username,
                'password': password
            }

            user = authenticate(**credentials)

            if user:
                payload = jwt_payload_handler(user)
                return {
                    'token': jwt_encode_handler(payload)
                }
            else:
                msg = 'Unable to log in with provided credentials.'
                raise serializers.ValidationError(msg)
        else:
            msg = 'Account with this email/username does not exist.'
            raise serializers.ValidationError(msg)
