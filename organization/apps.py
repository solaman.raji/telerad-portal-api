from django.apps import AppConfig


class OrganizationConfig(AppConfig):
    name = 'organization'

    def ready(self):
        from django.contrib.auth.models import User
        from actstream import registry
        registry.register(self.get_model('Facility'))
        registry.register(User)
