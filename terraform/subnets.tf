resource "aws_subnet" "subnet_1" {
  vpc_id            = "${var.vpc_id}"
  cidr_block        = "${var.subnet_1_cidr}"
  availability_zone = "${var.az_1}"

  tags {
    Name = "Europa"
  }
}

resource "aws_subnet" "subnet_2" {
  vpc_id            = "${var.vpc_id}"
  cidr_block        = "${var.subnet_2_cidr}"
  availability_zone = "${var.az_2}"

  tags {
    Name = "Ganymede"
  }
}

resource "aws_subnet" "subnet_3" {
  vpc_id            = "${var.vpc_id}"
  cidr_block        = "${var.subnet_3_cidr}"
  availability_zone = "${var.az_1}"

  tags {
    Name = "Phobos"
  }
}

resource "aws_subnet" "subnet_4" {
  vpc_id            = "${var.vpc_id}"
  cidr_block        = "${var.subnet_4_cidr}"
  availability_zone = "${var.az_2}"

  tags {
    Name = "Deimos"
  }
}

resource "aws_subnet" "subnet_5" {
  vpc_id            = "${var.vpc_id}"
  cidr_block        = "${var.subnet_5_cidr}"
  availability_zone = "${var.az_1}"
  map_public_ip_on_launch = true

  tags {
    Name = "Moon"
  }
}


resource "aws_internet_gateway" "moon_gw" {
  #vpc_id = "${aws_vpc.main.id}"
  vpc_id = "${var.vpc_id}"

  tags {
    Name = "Moon Internet G/W"
  }
}

resource "aws_eip" "nat" {
  vpc      = true

  tags {
    Name = "NAT GW EIP"
  }
}


resource "aws_nat_gateway" "nat_gw" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${aws_subnet.subnet_5.id}"

  tags {
     Name = "gw NAT"
  }

  depends_on = ["aws_internet_gateway.moon_gw"]
}


resource "aws_route_table" "moon_rt" {
  #vpc_id = "${aws_vpc.default.id}"
  vpc_id = "${var.vpc_id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.moon_gw.id}"
  }

  tags {
    Name = "Moon Route Table"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = "${aws_subnet.subnet_5.id}"
  route_table_id = "${aws_route_table.moon_rt.id}"
}

#resource "aws_default_route_table" "helios" {
#  default_route_table_id = "${data.aws_vpc.helios.default_route_table_id}"

#  route {
#    cidr_block = "0.0.0.0/0"
#    gateway_id = "${aws_nat_gateway.nat_gw.id}"
#  }

#  tags {
#    Name = "Helios Routes"
#  }
#}
