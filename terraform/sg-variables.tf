variable "cidr_blocks" {
  default     = "0.0.0.0/0"
  #default     = "${var.base_cidr_block}"
  description = "CIDR for sg"
}

variable "sg_name" {
  #default     = "rds_sg"
  default     = "RDS Security Group"
  description = "Tag Name for sg"
}

variable "sg_ssh" {
  default     = "SSH Security Group"
  description = "Tag Name for sg"
}
