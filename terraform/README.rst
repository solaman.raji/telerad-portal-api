===============================
Run Terraform for Telerad API
===============================

This directory contains terraform related files for telerad project.

Initial setup
--------------

Install Terraform in your machine. When it is available please run the following commands
::
    terraform init
    terraform plan -var 'env_type=dev' -out dev-env
    terraform show

    terraform apply dev-env
    terraform apply -var 'env_type=dev' -auto-approve

    # staging
    terraform plan -var 'env_type=staging' -out staging-env
    terraform apply staging-env -auto-approve

