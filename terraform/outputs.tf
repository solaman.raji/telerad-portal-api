#output "vpc_id" {
#  value = "${aws_vpc.main.id}"
#}

output "subnet_group" {
  value = "${aws_db_subnet_group.default.name}"
}

output "aws_nat_gateway" {
  value = "${aws_nat_gateway.nat_gw.public_ip}"
}

output "db_instance_id_dev" {
  value = "${aws_db_instance.dev.id}"
}

output "db_instance_id_staging" {
  value = "${aws_db_instance.staging.id}"
}

output "db_instance_address_dev" {
  value = "${aws_db_instance.dev.address}"
}

output "db_instance_address_staging" {
  value = "${aws_db_instance.staging.address}"
}
