# 192.168.0.0 - 192.168.31.255   /19
# 192.168.32.0 - 192.168.63.255  /19

# 192.168.64.0 - 192.168.79.255  /20
# 192.168.80.0 - 192.168.95.255  /20
# 192.168.96.0 - 192.168.111.255  /20


variable "subnet_1_cidr" {
  default     = "192.168.0.0/19"
  description = "AP Southeast 1a"
}

variable "subnet_2_cidr" {
  default     = "192.168.32.0/19"
  description = "AP Southeast 1b"
}


variable "subnet_3_cidr" {
  default     = "192.168.64.0/20"
  description = "AP Southeast 1a"
}

variable "subnet_4_cidr" {
  default     = "192.168.80.0/20"
  description = "AP Southeast 1b"
}

variable "subnet_5_cidr" {
  default     = "192.168.96.0/20"
  description = "AP Southeast 1a"
}

variable "az_1" {
  default     = "ap-southeast-1a"
  description = "Your Az1, use AWS CLI to find your account specific"
}

variable "az_2" {
  default     = "ap-southeast-1b"
  description = "Your Az2, use AWS CLI to find your account specific"
}


variable "vpc_id" {
  default = "vpc-caf6afad"
  description = "VPC ID"
  # vpc-7fd52c1a (Earth)
}

data "aws_vpc" "helios" {
  id = "${var.vpc_id}"
}

# We need this if we define a VPC
variable "base_cidr_block" {
  default = "192.168.0.0/16"
}