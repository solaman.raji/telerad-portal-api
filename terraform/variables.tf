variable "env_type" {}

variable "access_key" {}
variable "secret_key" {}

variable "region" {
  default = "ap-southeast-1"
  description = "AWS region to launch servers."
}

variable "identifier" {
  type = "map"
  description = "Identifier for your DB"
  default = {
    dev = "telerad-portal-dev-02"
    staging = "telerad-portal-staging-02"
  }
}

variable "storage" {
  default     = "5"
  description = "Storage size in GB"
}

variable "engine" {
  default     = "postgres"
  description = "Engine type, example values mysql, postgres"
}

variable "engine_version" {
  description = "Engine version"

  default = {
    mysql    = "5.7.21"
    postgres = "9.4.11"
  }
}

variable "instance_class" {
  default     = "db.m1.small"
  description = "Instance class"
}

variable "db_name" {
  type = "map"
  description = "db name"
}

variable "username" {
  default     = "caretaker"
  description = "User name"
}

variable "password" {
  default = "zxc999zxc"
  description = "password, provide through your ENV variables"
}
