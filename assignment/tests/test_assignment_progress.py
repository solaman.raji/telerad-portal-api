from django.test import TestCase

from assignment.models import Assignment
from assignment.utils import StatusChoice, ReportingStatus


# Initial Implementation based on the guideline from the following drawing
# https://docs.google.com/drawings/d/1sofxEbntYbjN6hu_M902aWtCNSwm4Cs3rLRmz-qDwsc/edit?usp=sharing
class ReportRequestedProgressTestCase(TestCase):
    def test_report_requested_status(self):
        for _status in [StatusChoice.new, StatusChoice.assigned, StatusChoice.declined, StatusChoice.accepted]:
            assignment = Assignment(status=_status.name)
            self.assertEqual(assignment.reporting_status, ReportingStatus.report_requested)

    def test_study_rejected_status(self):
        assignment = Assignment(status=StatusChoice.rejected.name)
        self.assertEqual(assignment.reporting_status, ReportingStatus.study_rejected)

    def test_fallback_status(self):
        assignment = Assignment(status='impossible')
        self.assertEqual(assignment.reporting_status, ReportingStatus.unknown)

    def test_report_is_ready(self):
        assignment = Assignment(status=StatusChoice.new.name, report_ready=True)
        self.assertEqual(assignment.reporting_status, ReportingStatus.report_ready)

    def test_report_is_ready_in_any_assignment_status(self):
        for status in list(StatusChoice):
            assignment = Assignment(status=status.name, report_ready=True)
            self.assertEqual(assignment.reporting_status, ReportingStatus.report_ready)
