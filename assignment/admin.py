from django.contrib import admin
from django.contrib.auth.models import User
from django.forms.models import ModelChoiceField

from assignment.models import Assignment
from telerad_portal_api.utils import GroupName


class AssignmentAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'guid',
        'study',
        'facility',
        'radiologist',
        'independent_radiologist',
        'status',
        'created_by'
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "radiologist":
            queryset = User.objects.filter(
                groups__name=GroupName.RADIOLOGIST.value
            )
            return ModelChoiceField(queryset)
        elif db_field.name == "independent_radiologist":
            queryset = User.objects.filter(
                groups__name=GroupName.INDEPENDENT_RADIOLOGIST.value
            )
            return ModelChoiceField(queryset)
        else:
            return super(AssignmentAdmin, self).formfield_for_foreignkey(
                db_field,
                request,
                **kwargs
            )

admin.site.register(Assignment, AssignmentAdmin)
