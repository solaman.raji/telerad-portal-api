import logging

from django.db.models.signals import post_save
from django.dispatch import receiver

from assignment.models import Assignment
from assignment.utils import StatusChoice as AssignmentStatusChoice
from study.models import Study
from telerad_portal_api.utils import GroupName, get_group_name

logger = logging.getLogger(__name__)


def init():
    logger.info('Signals initiated for Assignment app')


@receiver(post_save, sender=Study)
def create_auto_assignment(sender, **kwargs):
    logger.info(f'post_save triggered for {sender} with kwargs: {kwargs}')

    study = kwargs['instance']
    created = kwargs['created']
    if not created or study.facility:
        return

    logger.info(f'Check if Auto assignment should be created for study {study.guid}')
    if (study.non_affiliated_facility and not hasattr(study, 'assignment') and
            study.user and GroupName.INDEPENDENT_RADIOLOGIST.value == get_group_name(study.user)):
        logger.info(f'Study {study.guid} has passed condition to create auto assignment')
        assignment = Assignment.objects.create(
            study=study,
            independent_radiologist=study.user,
            status=AssignmentStatusChoice.accepted.name
        )
        logger.info(f'Assignment {assignment.guid} is created and assigned to {study.user} for study {study.guid}')
