from enum import Enum


class StatusChoice(Enum):
    new = "New"
    assigned = "Assigned"
    declined = "Declined"
    accepted = "Accepted"
    rejected = "Rejected"
    done = "Done"

    @classmethod
    def choices(cls):
        return ((choice.name, choice.value) for choice in StatusChoice)


# TODO: Move to study?
class ReportingStatus(Enum):
    report_requested = "Report Requested"
    study_rejected = "Study Rejected"
    report_ready = "Report Ready"
    unknown = "Unknown Reporting Status"
