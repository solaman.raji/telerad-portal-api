import uuid

from django.contrib.auth.models import User
from django.db import models

from assignment.utils import StatusChoice, ReportingStatus
from organization.models import Facility
from study.models import Study


class Assignment(models.Model):
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    study = models.OneToOneField(
        Study,
        related_name='assignment',
        on_delete=models.CASCADE
    )
    facility = models.ForeignKey(
        Facility,
        related_name='assignments',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    radiologist = models.ForeignKey(
        User,
        related_name='radiologist_assignments',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    independent_radiologist = models.ForeignKey(
        User,
        related_name='independent_radiologist_assignments',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    status = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        choices=StatusChoice.choices()
    )
    report_ready = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        User,
        related_name='created_assignments',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def assigned_to(self):
        return self.radiologist or self.independent_radiologist

    @property
    def ir_reporting_for_non_affiliated_facility(self):
        # Check if the assignment is for an independent radiologist and
        # the study is associated with a non-affiliated-facility
        if self.independent_radiologist is not None:
            if self.study and self.study.non_affiliated_facility:
                return True
        return False

    @property
    def reporting_status(self):
        if self.report_ready:
            return ReportingStatus.report_ready
        elif self.status in [StatusChoice.new.name,
                             StatusChoice.assigned.name,
                             StatusChoice.accepted.name,
                             StatusChoice.declined.name]:

            return ReportingStatus.report_requested
        elif self.status in [StatusChoice.rejected.name]:
            return ReportingStatus.study_rejected
        else:
            return ReportingStatus.unknown

    class Meta:
        db_table = 'assignments'
        verbose_name_plural = 'Assignments'
