from rest_framework import serializers

from assignment.models import Assignment
from telerad_portal_api import settings


class AssignmentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assignment
        fields = '__all__'

    def create(self, validated_data):
        return Assignment.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.facility = validated_data.get("facility", instance.facility)
        instance.radiologist = validated_data.get("radiologist", instance.radiologist)
        instance.independent_radiologist = validated_data.get(
            "independent_radiologist",
            instance.independent_radiologist
        )
        instance.status = validated_data.get("status", instance.status)
        instance.save()
        return instance


class AssignmentDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assignment
        fields = '__all__'


class AssignmentListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assignment
        fields = '__all__'

    @classmethod
    def get_facility_name(cls, obj):
        if hasattr(obj.study, "facility") and obj.study.facility:
            return obj.study.facility.name
        else:
            obj.study.facility_name

    @classmethod
    def get_body_part_name(cls, obj):
        return obj.study.body_part.name if obj.study.body_part else None

    @classmethod
    def get_status(cls, obj):
        data = {
            "value": obj.status,
            "label": obj.get_status_display(),
        }
        return data

    @classmethod
    def global_status(cls, obj):
        if obj.report_ready:
            return obj.reporting_status.value
        else:
            return obj.get_status_display()

    @classmethod
    def get_study(cls, obj):
        data = None

        if hasattr(obj, "study") and obj.study:
            data = {
                "guid": obj.study.guid,
                "modality": obj.study.get_modality_display(),
                "priority": obj.study.get_priority_display(),
                "body_part": cls.get_body_part_name(obj),
                "patient_id": obj.study.patient_id,
                "patient_name": obj.study.patient_name,
                "facility_name": cls.get_facility_name(obj),
                "created_at": obj.study.created_at.isoformat(),
                "zip_file": cls.get_zip_file(obj),
                "viewer_url": cls.get_viewer_url(obj)
            }

        return data

    @classmethod
    def get_radiologist(cls, obj):
        data = None

        if hasattr(obj, "radiologist") and obj.radiologist:
            data = {
                "guid": obj.radiologist.profile.guid,
                "name": obj.radiologist.first_name + " " + obj.radiologist.last_name,
            }

        return data

    @classmethod
    def get_viewer_url(cls, obj):
        return settings.VIEWER_URL.format(obj.study.study_instance_uid) if obj.study.study_instance_uid else None

    @classmethod
    def get_zip_file(cls, obj):
        return True if obj.study.zip_file else False


class CoordinatorAssignmentListSerializer(AssignmentListSerializer):
    def to_representation(self, obj):
        return {
            "guid": obj.guid,
            "status": self.get_status(obj),
            "study": self.get_study(obj),
            "radiologist": self.get_radiologist(obj)
        }


class RadiologistAssignmentListSerializer(AssignmentListSerializer):
    def to_representation(self, obj):
        return {
            "guid": obj.guid,
            "status": self.get_status(obj),
            "study": self.get_study(obj)
        }


class CoordinatorAssignmentListFlatSerializer(AssignmentListSerializer):
    def to_representation(self, obj):
        return {
            "guid": obj.study.guid,
            "modality": obj.study.get_modality_display(),
            "priority": obj.study.get_priority_display(),
            "body_part": self.get_body_part_name(obj),
            "patient_id": obj.study.patient_id,
            "patient_name": obj.study.patient_name,
            "facility_name": self.get_facility_name(obj),
            "created_at": obj.study.created_at.isoformat(),
            "zip_file": self.get_zip_file(obj),
            "viewer_url": self.get_viewer_url(obj),
            "assignment_guid": obj.guid,
            "assignment_status": self.get_status(obj),
            "global_status": self.global_status(obj),
            "radiologist": self.get_radiologist(obj),
        }


class RadiologistAssignmentListFlatSerializer(AssignmentListSerializer):
    @classmethod
    def global_status(cls, obj):
        if hasattr(obj, "report") and obj.report:
            return obj.report.get_status_display()
        else:
            return super().global_status(obj)

    def to_representation(self, obj):
        return {
            "guid": obj.study.guid,
            "modality": obj.study.get_modality_display(),
            "priority": obj.study.get_priority_display(),
            "body_part": self.get_body_part_name(obj),
            "patient_id": obj.study.patient_id,
            "patient_name": obj.study.patient_name,
            "facility_name": self.get_facility_name(obj),
            "created_at": obj.study.created_at.isoformat(),
            "zip_file": self.get_zip_file(obj),
            "viewer_url": self.get_viewer_url(obj),
            "global_status": self.global_status(obj),
            "assignment_guid": obj.guid,
            "assignment_status": self.get_status(obj),
        }
