from assignment.models import Assignment
from assignment.utils import StatusChoice
from organization.models import User, Facility
from study.models import Study
from telerad_portal_api.utils import GroupName


def _get_test_facility_query():
    return Facility.objects.filter(name__startswith='Facility')


def create_bulk_assignments():
    facility_list = _get_test_facility_query()

    for facility in facility_list:
        studies = Study.objects.filter(facility=facility)
        destination_facility_list = facility.destination_facilities.all()
        radiographer_list = User.objects.filter(
            groups__name=GroupName.RADIOGRAPHER.value,
            profile__facility=facility
        )
        independent_radiologist_list = facility.independent_radiologists.all()

        destination_facility_counter = 0
        radiographer_counter = 0
        independent_radiologist_counter = 0
        study_counter = 0

        for study in studies:
            if destination_facility_counter >= len(destination_facility_list):
                destination_facility_counter = 0

            if radiographer_counter >= len(radiographer_list):
                radiographer_counter = 0

            if independent_radiologist_counter >= len(independent_radiologist_list):
                independent_radiologist_counter = 0

            if study_counter % 2 == 0:
                Assignment.objects.create(
                    study=study,
                    facility=destination_facility_list[destination_facility_counter].facility,
                    status=StatusChoice.new.name,
                    created_by=radiographer_list[radiographer_counter]
                )
                destination_facility_counter += 1
            else:
                Assignment.objects.create(
                    study=study,
                    independent_radiologist=independent_radiologist_list[independent_radiologist_counter].independent_radiologist,
                    status=StatusChoice.assigned.name,
                    created_by=radiographer_list[radiographer_counter]
                )
                independent_radiologist_counter += 1

            radiographer_counter += 1
            study_counter += 1


def run():
    create_bulk_assignments()
