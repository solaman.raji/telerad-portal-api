from assignment.utils import StatusChoice
from organization.models import User, Facility
from telerad_portal_api.utils import GroupName


def _get_test_facility_query():
    return Facility.objects.filter(name__startswith='Facility')


def create_bulk_assignments():
    facility_list = _get_test_facility_query()

    for facility in facility_list:
        assignments = facility.assignments.all()
        radiologist_list = User.objects.filter(
            groups__name=GroupName.RADIOLOGIST.value,
            profile__facility=facility
        )

        radiologist_counter = 0

        for assignment in assignments:
            if radiologist_counter >= len(radiologist_list):
                radiologist_counter = 0

            assignment.radiologist = radiologist_list[radiologist_counter]
            assignment.status = StatusChoice.assigned.name
            assignment.save()
            radiologist_counter += 1


def run():
    create_bulk_assignments()
