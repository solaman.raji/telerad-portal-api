from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from assignment.views import AssignmentList, AssignmentDetail, AssignmentStatus

urlpatterns = [
    path('assignments/', AssignmentList.as_view()),
    path('assignments/<guid>/', AssignmentDetail.as_view()),
    path('assignments/<guid>/<assignment_status>/', AssignmentStatus.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
