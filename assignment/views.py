from actstream import action
from django.contrib.auth.models import User
from django.db.models import Q
from django.utils.decorators import method_decorator
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.views import APIView

from assignment.filters import AssignmentFilter
from assignment.models import Assignment
from assignment.serializers import (
    AssignmentCreateSerializer,
    CoordinatorAssignmentListSerializer,
    RadiologistAssignmentListSerializer,
    CoordinatorAssignmentListFlatSerializer,
    RadiologistAssignmentListFlatSerializer
)
from assignment.utils import StatusChoice as AssignmentStatusChoice
from organization.decorators import check_group_validation, check_user_profile
from organization.models import Facility
from organization.views import (
    DestinationFacility,
    RadiologistDetail,
    IndependentRadiologist
)
from study.models import Study
from study.utils import StatusChoice as StudyStatusChoice, PriorityChoice
from study.views import StudyDetail
from telerad_portal_api.custom_exception import (
    InvalidParameter,
    UnauthorizedAccess,
    NoAssignment,
    InvalidStatus
)
from telerad_portal_api.mixins import PaginationMixin
from telerad_portal_api.utils import (
    GroupName,
    ViewMethod,
    get_group_name,
    serializer_error_mapping,
    get_object_by_guid,
    get_request_query_params
)


class AssignmentList(APIView, PaginationMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = None
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS

    @check_group_validation(ViewMethod.ASSIGNMENT_LIST.value)
    @method_decorator(check_user_profile)
    def get(self, request):
        data = {}
        multiple_choice_filter_list = ['modality', 'body_part', 'radiologist']
        response_type = request.GET.get('response_type')
        search = request.GET.get('search')
        group_name = get_group_name(request.user)
        request_query_params = get_request_query_params(
            dict(request.GET),
            multiple_choice_filter_list
        )

        if request.GET.get('ordering'):
            request_query_params['ordering'] = request.GET.get('ordering')
        if request.GET.get('patient_id'):
            request_query_params['patient_id'] = request.GET.get('patient_id')
        if request.GET.get('patient_name'):
            request_query_params['patient_name'] = request.GET.get('patient_name')
        if request.GET.get('priority'):
            request_query_params['priority'] = request.GET.get('priority')

        assignment_list = self.get_assignment_list(request.user, group_name)

        if search:
            assignment_list = assignment_list.filter(
                Q(study__patient_id__icontains=search) |
                Q(study__patient_name__icontains=search) |
                Q(study__patient_telephone_number__icontains=search)
            )
        else:
            assignment_list = AssignmentFilter(request_query_params, queryset=assignment_list).qs

        page = self.paginate_queryset(assignment_list)

        if page is not None:
            self.serializer_class = self.get_serializer(group_name, response_type)
            serializer = self.serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)

        return Response(data, status=status.HTTP_200_OK)

    @check_group_validation(ViewMethod.ASSIGNMENT_CREATE.value)
    @method_decorator(check_user_profile)
    def post(self, request):
        target = None
        group_name = get_group_name(request.user)
        self.param_validation(group_name, request.data)
        study = get_object_by_guid(Study, request.data['study_guid'])

        if hasattr(study, "assignment"):
            raise NoAssignment()

        self.study_authorization(request.user, study)
        request.data['study'] = study.id

        if group_name == GroupName.RADIOGRAPHER.value:
            if request.data.get('facility_guid'):
                facility_guid = request.data.pop('facility_guid')
                facility = get_object_by_guid(Facility, facility_guid)

                if not facility == request.user.profile.facility:
                    self.destination_facility_authorization(request.user, facility)

                request.data['facility'] = facility.id
                request.data['status'] = AssignmentStatusChoice.new.name
                target = facility

            if request.data.get('independent_radiologist_guid'):
                independent_radiologist_guid = request.data.pop('independent_radiologist_guid')
                independent_radiologist = get_object_by_guid(User, independent_radiologist_guid)
                self.independent_radiologist_authorization(request.user, independent_radiologist)
                request.data['independent_radiologist'] = independent_radiologist.id
                request.data['status'] = AssignmentStatusChoice.assigned.name
                target = independent_radiologist

        if request.data.get('radiologist_guid'):
            radiologist_guid = request.data.pop('radiologist_guid')
            radiologist = get_object_by_guid(User, radiologist_guid)
            self.radiologist_authorization(request.user, radiologist)
            request.data['radiologist'] = radiologist.id
            request.data['status'] = AssignmentStatusChoice.assigned.name
            target = radiologist

        request.data['created_by'] = request.user.id
        serializer = AssignmentCreateSerializer(data=request.data)

        try:
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                StudyDetail.update_study_status(StudyDetail(), study, StudyStatusChoice.assigned.name)
                data = serializer.data

                # Create Activity - Assignment Created
                user = request.user
                study = Study.objects.get(assignment__guid=data["guid"])
                action.send(
                    user,
                    verb="Study Assigned by {first_name} {last_name} ({group_name_short_form})".format(
                        first_name=user.first_name,
                        last_name=user.last_name,
                        group_name_short_form=GroupName.get_group_name_short_form(group_name)
                    ),
                    action_object=study,
                    target=study.facility,
                    description="Detail Timeline"
                )

                # Create Activity - Urgent
                if study.priority == PriorityChoice.urgent.name:
                    action.send(
                        user,
                        verb="Urgent - {patient_name}".format(
                            patient_name=study.patient_name
                        ),
                        action_object=study,
                        target=target,
                        description="Notification Timeline"
                    )

                return Response(data, status=status.HTTP_201_CREATED)
        except Exception:
            data = {
                "error": {
                    "message": serializer_error_mapping(serializer.errors)
                }
            }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

    def get_assignment_list(self, user, group_name):
        if group_name == GroupName.COORDINATOR.value:
            assignment_list = Assignment.objects.filter(
                facility=user.profile.facility
            ).exclude(
                status=AssignmentStatusChoice.rejected.name
            )
        elif group_name == GroupName.RADIOLOGIST.value:
            assignment_list = Assignment.objects.filter(
                radiologist=user
            ).exclude(
                status=AssignmentStatusChoice.declined.name
            ).exclude(
                status=AssignmentStatusChoice.rejected.name
            )
        elif group_name == GroupName.INDEPENDENT_RADIOLOGIST.value:
            assignment_list = Assignment.objects.filter(
                independent_radiologist=user
            ).exclude(
                status=AssignmentStatusChoice.declined.name
            ).exclude(
                status=AssignmentStatusChoice.rejected.name
            ).exclude(
                study__user=user
            )

        return assignment_list.order_by('-created_at')

    def get_serializer(self, group_name, response_type):
        if group_name == GroupName.COORDINATOR.value:
            if response_type == "flat":
                serializer = CoordinatorAssignmentListFlatSerializer
            else:
                serializer = CoordinatorAssignmentListSerializer
        elif group_name == GroupName.RADIOLOGIST.value:
            if response_type == "flat":
                serializer = RadiologistAssignmentListFlatSerializer
            else:
                serializer = RadiologistAssignmentListSerializer
        elif group_name == GroupName.INDEPENDENT_RADIOLOGIST.value:
            if response_type == "flat":
                serializer = RadiologistAssignmentListFlatSerializer
            else:
                serializer = RadiologistAssignmentListSerializer

        return serializer

    def param_validation(self, group_name, request_data):
        exception = False

        if not request_data or len(request_data) > 2:
            exception = True

        if 'study_guid' not in request_data or not request_data['study_guid']:
            exception = True

        if group_name == GroupName.RADIOGRAPHER.value:
            if (
                'facility_guid' not in request_data
                and
                'radiologist_guid' not in request_data
                and
                'independent_radiologist_guid' not in request_data
            ):
                exception = True
            elif (
                (
                    'facility_guid' in request_data
                    and
                    not request_data['facility_guid']
                )
                or
                (
                    'radiologist_guid' in request_data
                    and
                    not request_data['radiologist_guid']
                )
                or
                (
                    'independent_radiologist_guid' in request_data
                    and
                    not request_data['independent_radiologist_guid']
                )
            ):
                exception = True

        if group_name == GroupName.RADIOLOGIST.value:
            if 'radiologist_guid' not in request_data:
                exception = True
            elif 'radiologist_guid' in request_data and not request_data['radiologist_guid']:
                exception = True

        if exception:
            raise InvalidParameter()

    def study_authorization(self, user, study):
        if user.profile.facility != study.facility:
            raise UnauthorizedAccess(detail="User is not authorized to assign this study.")

    def destination_facility_authorization(self, user, facility):
        if not DestinationFacility.validate_destination_facility(DestinationFacility(), user, facility):
            raise UnauthorizedAccess(detail="User is not authorized to assign study to this facility.")

    def radiologist_authorization(self, user, radiologist):
        if not RadiologistDetail.validate_radiologist(
                RadiologistDetail(),
                user,
                radiologist
        ):
            raise UnauthorizedAccess(detail="User is not authorized to assign study to this radiologist.")

    def independent_radiologist_authorization(self, user, independent_radiologist):
        if not IndependentRadiologist.validate_independent_radiologist(
                IndependentRadiologist(),
                user,
                independent_radiologist
        ):
            raise UnauthorizedAccess(detail="User is not authorized to assign study to this independent radiologist.")


class AssignmentDetail(APIView):
    permission_classes = (IsAuthenticated,)

    @check_group_validation(ViewMethod.ASSIGNMENT_UPDATE.value)
    @method_decorator(check_user_profile)
    def put(self, request, guid):
        group_name = get_group_name(request.user)
        assignment = get_object_by_guid(Assignment, guid)
        self.assignment_authorization(request.user, group_name, assignment)
        self.param_validation(group_name, request.data)
        self.status_validation(group_name, assignment)

        if group_name == GroupName.RADIOGRAPHER.value:
            if request.data.get('facility_guid'):
                facility_guid = request.data.pop('facility_guid')
                facility = get_object_by_guid(Facility, facility_guid)

                if not facility == request.user.profile.facility:
                    self.destination_facility_authorization(request.user, facility)

                request.data['facility'] = facility.id
                request.data['radiologist'] = None
                request.data['independent_radiologist'] = None
                request.data['status'] = AssignmentStatusChoice.new.name
                target = facility

            if request.data.get('radiologist_guid'):
                radiologist_guid = request.data.pop('radiologist_guid')
                radiologist = get_object_by_guid(User, radiologist_guid)
                self.radiologist_authorization(request.user, radiologist)
                request.data['facility'] = None
                request.data['radiologist'] = radiologist.id
                request.data['independent_radiologist'] = None
                request.data['status'] = AssignmentStatusChoice.assigned.name
                target = radiologist

            if request.data.get('independent_radiologist_guid'):
                independent_radiologist_guid = request.data.pop('independent_radiologist_guid')
                independent_radiologist = get_object_by_guid(User, independent_radiologist_guid)
                self.independent_radiologist_authorization(request.user, independent_radiologist)
                request.data['facility'] = None
                request.data['radiologist'] = None
                request.data['independent_radiologist'] = independent_radiologist.id
                request.data['status'] = AssignmentStatusChoice.assigned.name
                target = independent_radiologist
        elif group_name == GroupName.COORDINATOR.value:
            if request.data.get('radiologist_guid'):
                radiologist_guid = request.data.pop('radiologist_guid')
                radiologist = get_object_by_guid(User, radiologist_guid)
                self.radiologist_authorization(request.user, radiologist)
                request.data['radiologist'] = radiologist.id
                request.data['status'] = AssignmentStatusChoice.assigned.name
                target = radiologist
        elif group_name == GroupName.RADIOLOGIST.value:
            if request.data.get('radiologist_guid'):
                radiologist_guid = request.data.pop('radiologist_guid')
                radiologist = get_object_by_guid(User, radiologist_guid)

                if radiologist != request.user:
                    raise UnauthorizedAccess(detail="User is not authorized to assign study to this radiologist.")

                request.data['radiologist'] = radiologist.id
                request.data['status'] = AssignmentStatusChoice.accepted.name
                target = radiologist

        serializer = AssignmentCreateSerializer(assignment, data=request.data, partial=True)

        try:
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                data = serializer.data

                # Create Activity - Assignment Updated
                user = request.user
                study = Study.objects.get(assignment__guid=guid)
                action.send(
                    user,
                    verb="Study Assigned by {first_name} {last_name} ({group_name_short_form})".format(
                        first_name=user.first_name,
                        last_name=user.last_name,
                        group_name_short_form=GroupName.get_group_name_short_form(group_name)
                    ),
                    action_object=study,
                    target=study.facility,
                    description="Detail Timeline"
                )

                # Create Activity - Urgent
                if study.priority == PriorityChoice.urgent.name:
                    action.send(
                        user,
                        verb="Urgent - {patient_name}".format(
                            patient_name=study.patient_name
                        ),
                        action_object=study,
                        target=target,
                        description="Notification Timeline"
                    )

                return Response(data, status=status.HTTP_200_OK)
        except Exception:
            data = {
                "error": {
                    "message": serializer_error_mapping(serializer.errors)
                }
            }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

    def assignment_authorization(self, user, group_name, assignment):
        exception = True

        if group_name == GroupName.RADIOGRAPHER.value and user.profile.facility == assignment.study.facility:
            exception = False
        elif group_name == GroupName.COORDINATOR.value and user.profile.facility == assignment.facility:
            exception = False
        elif group_name == GroupName.RADIOLOGIST.value and user.profile.facility == assignment.study.facility:
            exception = False
        elif group_name == GroupName.RADIOLOGIST.value and user.profile.facility == assignment.facility:
            exception = False
        elif group_name == GroupName.RADIOLOGIST.value and user == assignment.radiologist:
            exception = False

        if exception:
            raise UnauthorizedAccess("User is not authorized to assign this study.")

    def param_validation(self, group_name, request_data):
        exception = False

        if not request_data or len(request_data) > 1:
            exception = True

        if group_name == GroupName.RADIOGRAPHER.value:
            if (
                'facility_guid' not in request_data
                and
                'radiologist_guid' not in request_data
                and
                'independent_radiologist_guid' not in request_data
            ):
                exception = True
            elif (
                (
                    'facility_guid' in request_data
                    and
                    not request_data['facility_guid']
                )
                or
                (
                    'radiologist_guid' in request_data
                    and
                    not request_data['radiologist_guid']
                )
                or
                (
                    'independent_radiologist_guid' in request_data
                    and
                    not request_data['independent_radiologist_guid']
                )
            ):
                exception = True

        if group_name == GroupName.COORDINATOR.value or group_name == GroupName.RADIOLOGIST.value:
            if 'radiologist_guid' not in request_data:
                exception = True
            elif 'radiologist_guid' in request_data and not request_data['radiologist_guid']:
                exception = True

        if exception:
            raise InvalidParameter()

    def status_validation(self, group_name, assignment):
        exception = True

        if (
                group_name == GroupName.RADIOGRAPHER.value
                and
                assignment.facility
                and
                not assignment.radiologist
                and
                assignment.status == AssignmentStatusChoice.new.name
        ):
            exception = False
        elif (
                group_name == GroupName.RADIOGRAPHER.value
                and
                not assignment.facility
                and
                assignment.radiologist
                and
                assignment.status == AssignmentStatusChoice.assigned.name
        ):
            exception = False
        elif (
                group_name == GroupName.RADIOGRAPHER.value
                and
                assignment.independent_radiologist
                and
                assignment.status == AssignmentStatusChoice.assigned.name
        ):
            exception = False
        elif (
                group_name == GroupName.COORDINATOR.value
                and
                (
                    assignment.status == AssignmentStatusChoice.new.name
                    or
                    assignment.status == AssignmentStatusChoice.assigned.name
                    or
                    assignment.status == AssignmentStatusChoice.declined.name
                )
        ):
            exception = False
        elif (
                group_name == GroupName.RADIOLOGIST.value
                and
                (
                    assignment.status == AssignmentStatusChoice.new.name
                    or
                    assignment.status == AssignmentStatusChoice.declined.name
                )
        ):
            exception = False

        if exception:
            raise InvalidStatus(detail="Cannot assign this study.")

    def destination_facility_authorization(self, user, facility):
        if not DestinationFacility.validate_destination_facility(DestinationFacility(), user, facility):
            raise UnauthorizedAccess(detail="User is not authorized to assign study to this facility.")

    def radiologist_authorization(self, user, radiologist):
        if not RadiologistDetail.validate_radiologist(
                RadiologistDetail(),
                user,
                radiologist
        ):
            raise UnauthorizedAccess(detail="User is not authorized to assign study to this radiologist.")

    def independent_radiologist_authorization(self, user, independent_radiologist):
        if not IndependentRadiologist.validate_independent_radiologist(
                IndependentRadiologist(),
                user,
                independent_radiologist
        ):
            raise UnauthorizedAccess(detail="User is not authorized to assign study to this independent radiologist.")


class AssignmentStatus(APIView):
    permission_classes = (IsAuthenticated,)

    @check_group_validation(ViewMethod.ASSIGNMENT_STATUS.value)
    @method_decorator(check_user_profile)
    def put(self, request, guid, assignment_status):
        request_data = {}
        group_name = get_group_name(request.user)
        self.param_validation(assignment_status)
        assignment = get_object_by_guid(Assignment, guid)
        self.assignment_authorization(request.user, group_name, assignment)
        self.status_validation(assignment, assignment_status)

        if assignment_status == AssignmentStatusChoice.accepted.name:
            request_data['status'] = AssignmentStatusChoice.accepted.name
            assignment_status_label = AssignmentStatusChoice.accepted.value
        elif assignment_status == AssignmentStatusChoice.declined.name:
            request_data['status'] = AssignmentStatusChoice.declined.name
            assignment_status_label = AssignmentStatusChoice.declined.value
        elif assignment_status == AssignmentStatusChoice.rejected.name:
            request_data['status'] = AssignmentStatusChoice.rejected.name
            assignment_status_label = AssignmentStatusChoice.rejected.value

        serializer = AssignmentCreateSerializer(assignment, data=request_data, partial=True)

        try:
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                data = serializer.data

                # Create Activity - Assignment Status
                user = request.user
                study = Study.objects.get(assignment__guid=data["guid"])
                action.send(
                    user,
                    verb="Study {assignment_status_label} by {first_name} {last_name} ({group_name_short_form})".format(
                        assignment_status_label=assignment_status_label,
                        first_name=user.first_name,
                        last_name=user.last_name,
                        group_name_short_form=GroupName.get_group_name_short_form(group_name)
                    ),
                    action_object=study,
                    target=study.facility,
                    description="Detail Timeline"
                )

                return Response(data, status=status.HTTP_200_OK)
        except Exception:
            data = {
                "error": {
                    "message": serializer_error_mapping(serializer.errors)
                }
            }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

    def param_validation(self, assignment_status):
        status_list = [
            AssignmentStatusChoice.accepted.name,
            AssignmentStatusChoice.declined.name,
            AssignmentStatusChoice.rejected.name,
        ]

        if assignment_status not in status_list:
            raise InvalidParameter()

    def assignment_authorization(self, user, group_name, assignment):
        exception = False

        if group_name == GroupName.RADIOLOGIST.value and user != assignment.radiologist:
            exception = True
        elif group_name == GroupName.INDEPENDENT_RADIOLOGIST.value and user != assignment.independent_radiologist:
            exception = True

        if exception:
            raise UnauthorizedAccess(detail="User is not authorized to update status of this assignment.")

    def status_validation(self, assignment, assignment_status):
        exception = True

        if (
                assignment_status == AssignmentStatusChoice.accepted.name
                and
                assignment.status == AssignmentStatusChoice.assigned.name
        ):
            exception = False
        elif (
                (
                    assignment_status == AssignmentStatusChoice.declined.name
                    or
                    assignment_status == AssignmentStatusChoice.rejected.name
                )
                and
                (
                    assignment.status == AssignmentStatusChoice.assigned.name
                    or
                    assignment.status == AssignmentStatusChoice.accepted.name
                )
        ):
            exception = False

        if exception:
            raise InvalidStatus(detail="Cannot update status of this assignment.")
