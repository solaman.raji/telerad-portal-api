from django.apps import AppConfig


class AssignmentConfig(AppConfig):
    name = 'assignment'

    def ready(self):
        from . import signals
        signals.init()
