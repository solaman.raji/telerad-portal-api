import django_filters

from assignment.models import Assignment
from organization.models import UserProfile, BodyPart
from study.utils import ModalityChoice, PriorityChoice
from telerad_portal_api.utils import GroupName


class AssignmentFilter(django_filters.FilterSet):
    ordering = django_filters.OrderingFilter(
        fields=(
            ('created_at', 'created_at'),
        ),
    )
    patient_id = django_filters.CharFilter(
        name='study__patient_id',
        lookup_expr='icontains'
    )
    patient_name = django_filters.CharFilter(
        name='study__patient_name',
        lookup_expr='icontains'
    )
    modality = django_filters.MultipleChoiceFilter(
        name='study__modality',
        choices=tuple(ModalityChoice.choices())
    )
    body_part = django_filters.ModelMultipleChoiceFilter(
        name='study__body_part__guid',
        to_field_name='guid',
        queryset=BodyPart.objects.all(),
    )
    priority = django_filters.ChoiceFilter(
        name='study__priority',
        choices=tuple(PriorityChoice.choices())
    )
    radiologist = django_filters.ModelMultipleChoiceFilter(
        name='radiologist__profile__guid',
        to_field_name='guid',
        queryset=UserProfile.objects.filter(
            user__groups__name=GroupName.RADIOLOGIST.value
        )
    )

    class Meta:
        model = Assignment
        fields = []
