# Generated by Django 2.0.7 on 2018-07-30 13:52

from django.db import migrations, models
import django.db.models.deletion
import django_fsm
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('assignment', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('procedure', models.CharField(blank=True, max_length=100, null=True)),
                ('clinical_information', models.TextField(blank=True, null=True)),
                ('comparison', models.TextField(blank=True, null=True)),
                ('findings', models.TextField(blank=True, null=True)),
                ('actionable_findings', models.CharField(default='None', max_length=100)),
                ('impression', models.TextField(blank=True, null=True)),
                ('template_name', models.CharField(max_length=200)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('signed_at', models.DateTimeField(blank=True, null=True)),
                ('status', django_fsm.FSMField(choices=[('NEW', 'NEW'), ('DRAFT', 'DRAFT'), ('FINALISED', 'FINALISED'), ('REVISED', 'REVISED'), ('WAITING_FOR_TRANSCRIBE', 'WAITING_FOR_TRANSCRIBE'), ('TRANSCRIBING', 'TRANSCRIBING'), ('PROVISIONAL', 'PROVISIONAL')], default='NEW', max_length=50, verbose_name='Reporting Status')),
                ('assignment', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='report', to='assignment.Assignment')),
            ],
            options={
                'db_table': 'reports',
                'permissions': (('can_create', 'Create Report Permission'), ('can_edit', 'Edit Report Permission'), ('can_sign', 'Sign report Permission'), ('can_revise', 'Revise Report Permission'), ('can_edit_revised', 'Edit Revised Report Permission'), ('can_sign_revised', 'Sign Revised Report Permission')),
            },
        ),
    ]
