"""
This script will be used to send study report to non-affiliated study
"""

from django.core.mail import send_mail, EmailMessage

sample_email = '''
From : reporting@alemhealth.com

Subject: Dr. Clement has shared a report through AlemHealth Connect

Dear LabCare Diagnostic Center,

Dr. Clement has shared the CR Diagnostic Radiology report of Patient No. 123 - Salim Shah( 38/M) through AlemHealth Connect. 

The patients diagnostic report is attached to this email.

AlemHealth Connect makes it easy to share your Facilities medical imaging to any radiologists and receive reports seamlessly.  Sign up today to simplify your workflow

http://alem.health/signup

Thank you, 

Team at AlemHealth

{{ PDF Report Attached }}'''

# FROM_ADDRESS = 'reporting@alemhealth.com'
FROM_ADDRESS = 'issa@alemcloud.com'
SUBJECT_TEMPLATE = 'Dr. {doctor_name} has shared a report through AlemHealth Connect'
EMAIL_BODY_TEMPLATE = '''Dear {facility_name},

Dr. {doctor_name} has shared the {modality} Diagnostic Radiology report of Patient No. {patient_id} - {patient_name_with_age_gender} through AlemHealth Connect. 

The patients diagnostic report is attached to this email.

AlemHealth Connect makes it easy to share your Facilities medical imaging to any radiologists and receive reports seamlessly. Sign up today to simplify your workflow

http://alem.health/signup

Thank you, 

Team at AlemHealth
'''

# send_mail('Subject Here', 'Here is the message', 'reporting@alemhealth.com', ['issa@alemcloud.com'], fail_silently=False)
# send_mail('Subject Here', 'Here is the message', 'issa@alemcloud.com', ['issa@alemcloud.com'], fail_silently=False)


def send_sample_email(to):
    send_mail('Sample email', sample_email, FROM_ADDRESS, [to])


def send_signed_report(**kwargs):
    doctor_name = kwargs.get('doctor_name')
    doctor_email = kwargs.get('doctor_email')
    facility_email = kwargs.get('facility_email')
    facility_name = kwargs.get('facility_name')
    modality = kwargs.get('modality')
    patient_id = kwargs.get('patient_id')
    patient_name = kwargs.get('patient_name_with_age_gender')
    _pdf_file = kwargs.get('pdf')

    message = EmailMessage(
        subject=SUBJECT_TEMPLATE.format(**kwargs),
        body=EMAIL_BODY_TEMPLATE.format(**kwargs),
        from_email=FROM_ADDRESS,
        to=[facility_email],
        cc=[doctor_email],
        bcc=['issa@alemcloud.com', 'solaman@alemcloud.com'],
        reply_to=[doctor_email],
        # attachments=_pdf_file,
        headers={'Message-ID': 'AlemHealth Study report: 21312312312'},
    )

    message.attach_file(_pdf_file)
    message.send(fail_silently=False)

    print('Email is send successfully')
