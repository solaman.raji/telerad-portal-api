from django.contrib import admin

from report.models import Report


class ReportAdmin(admin.ModelAdmin):
    list_display = ('id', 'assignment', 'procedure', 'actionable_findings', 'template_name', 'created_at')

    fieldsets = [
        ('Report for Assignment', {'fields': ['assignment']}),
        ('Report fields', {'fields': ['procedure', 'actionable_findings',
                                      'clinical_information', 'findings', 'impression', 'comparison', 'technique']}),

    ]


admin.site.register(Report, ReportAdmin)
