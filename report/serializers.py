import logging

from rest_framework import serializers
from rest_framework.reverse import reverse

from .models import Report, ReportStatus
from .signals import ReportUpdatedSignal

logger = logging.getLogger(__name__)
# FIXME: Duplicate view names in views and serializers
GENERATE_REPORT_PDF = 'generate-report-pdf'
GENERATE_REPORT_HTML = 'generate-report-html'
SAVE_DRAFT_REPORT = 'save-draft-report'
SIGN_REPORT = 'sign-report'
REVISE_REPORT = 'revise-report'

NEXT_WORKFLOW_LINKS = {
    ReportStatus.NEW.name: (SAVE_DRAFT_REPORT, SIGN_REPORT),
    ReportStatus.DRAFT.name: (SAVE_DRAFT_REPORT, SIGN_REPORT),
    ReportStatus.FINALISED.name: (REVISE_REPORT,),
    ReportStatus.REVISED.name: (REVISE_REPORT, SIGN_REPORT,)
}


# TODO: Add docorator to add workflow links to response
def get_workflow_links(report_status, request, *args):
    # _view_names = NEXT_LINKS[report_status]
    # for view_name in _view_names:
    #     reverse(view_name)
    return [
        {
            'name': view_name,
            'url': reverse(view_name, args, request=request)
        } for view_name in NEXT_WORKFLOW_LINKS[report_status]
    ]


class ReportCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = '__all__'

    def create(self, validated_data):
        _request = self.context.get('request')
        user = _request.user if _request else None

        _created = Report.objects.create(**validated_data)
        if user:
            logger.info(f'{self.__class__}.create() is called. report {_created}, user: {user}, '
                        f'data: {validated_data}')
            ReportUpdatedSignal.send(self.__class__, user=user, action_text='Report Created', report=_created)
        else:
            logger.error(f'User not found in request context. {validated_data}')
        return _created


class ReportUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = ['procedure', 'clinical_information', 'comparison', 'findings',
                  'actionable_findings', 'impression', 'technique', 'status']

        # def update(self, instance, validated_data):
        #     return Report.objects.update(**validated_data)


class ReportDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = '__all__'

    def to_representation(self, obj):
        _request = self.context.get('request')
        return {
            "guid": obj.id,
            "clinical_information": obj.clinical_information,
            "comparison": obj.comparison,
            "findings": obj.findings,
            "actionable_findings": obj.actionable_findings,
            "impression": obj.impression,
            "technique": obj.technique,
            "procedure": obj.procedure,
            "template_name": obj.template_name,
            "status": obj.status,
            "created_at": obj.created_at.isoformat(),
            "updated_at": obj.updated_at.isoformat(),
            "assignment_guid": obj.assignment.guid,
            "_links": get_workflow_links(obj.status, _request, obj.id, )
        }
