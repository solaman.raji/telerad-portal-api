# FIXME: Use Enum
class ActionableFindingsChoice():
    _values = ['None', 'Significant Unexpected Findings', 'Urgent Findings', 'Critical Findings']

    @classmethod
    def dict(cls):
        return {value: value for value in cls._values}
