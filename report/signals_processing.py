import logging

from actstream import action
from django.dispatch import receiver

from .signals import ReportUpdatedSignal, ReportSignedSignal

logger = logging.getLogger(__name__)


def find_urgent_actionable_findings(sender, **kwargs):
    instance = kwargs['instance']
    update_fields = kwargs['update_fields']
    logger.info(f'{sender}:{instance.id} is saved with fields: {update_fields}')
    if instance.actionable_findings is None:
        return
    if instance.actionable_findings == 'Critical Findings':
        logger.info(f'Critical Findings is set to report: {instance.id}')


def add_activity_item(verb, user, action_object, target):
    logger.info(f'Add activity: {verb} by {user} on {target}/{action_object}')
    action.send(
        user,
        verb=verb,
        action_object=action_object,
        target=target,
        description="Detail Timeline"
    )


@receiver(ReportUpdatedSignal, sender=None)
def store_report_activity(sender, **kwargs):
    logger.info(f'Report updated. sender: {sender}, arguments: {kwargs}')
    user = kwargs.pop('user', None)
    report = kwargs.pop('report')
    action_text = kwargs.pop('action_text', 'Report Updated')
    assignment = report.assignment if hasattr(report, 'assignment') else {}
    if not hasattr(assignment, 'study'):
        logger.error(f'No study found with assignment: {assignment.guid}')
        return
    study = assignment.study
    facility = report.assignment.facility
    add_activity_item(verb=action_text, user=user, action_object=study, target=facility)
    add_activity_item(verb=action_text, user=user, action_object=report, target=study)

    # if report.actionable_findings is not None and report.actionable_findings == 'Critical Findings':
    #     add_activity_item(verb='Critical findings in Report', user=user, action_object=report, target=study)
    #     add_activity_item(verb='Critical findings in Report', user=user, action_object=study, target=facility)


@receiver(ReportSignedSignal, sender=None)
def send_email_to_non_affiliated_facility(sender, **kwargs):
    def collect_data_to_send_email():
        email_data = dict()
        email_data['doctor_name'] = f'{radiologist.first_name} {radiologist.last_name}'
        email_data['doctor_email'] = radiologist.email
        email_data['facility_name'], email_data['facility_email'] = study.non_affiliated_facility

        email_data['patient_id'] = study.patient_id or ''
        email_data['patient_name_with_age_gender'] = study.patient_name_age_gender or ''
        email_data['modality'] = study.modality
        return email_data

    def _generate_pdf():
        from report.pdf import generate_html, generate_pdf
        logger.info(f'Pdf generation request for report {report.id}')
        template_path = 'report/general.html'
        rendered_html = generate_html(report.assignment.study, report, template_path, print_header=False)
        logger.info('HTML is generated for report.')
        print('----------------------')
        print(rendered_html)
        print('----------------------')

        try:
            pdf_filename = f'/tmp/{report.id}.pdf'
            generate_pdf(rendered_html, pdf_filename)
            return pdf_filename
        except:
            logger.exception('Error in generating pdf for report {report.id}')
            raise

    logger.info(f'Report is signed: Sender: {sender}, arguments: {kwargs}')
    report = kwargs.pop('report')
    assignment = report.assignment
    study = assignment.study
    radiologist = kwargs.pop('radiologist')

    if assignment.ir_reporting_for_non_affiliated_facility:
        logger.info(f'Send email after {report.id} is signed by {radiologist}')
        email_data = collect_data_to_send_email()
        _pdf_file = _generate_pdf()
        from .email import send_signed_report
        send_signed_report(**email_data, pdf=_pdf_file)
        import os
        os.remove(_pdf_file)
    else:
        logger.info(f'Signed report {report.id} is not for a non-affiliated facility. Not sending email')
