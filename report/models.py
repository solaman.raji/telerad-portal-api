import logging
import uuid

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django_fsm import transition, FSMField
from enum import Enum

from assignment.models import Assignment
from report.signals import ReportSignedSignal
from .signals import ReportUpdatedSignal

logger = logging.getLogger(__name__)


class ReportStatus(Enum):
    NEW = 'New Report'
    DRAFT = 'Draft Report'
    FINALISED = 'Report Finalised'
    REVISED = 'Report Revised'
    WAITING_FOR_TRANSCRIBE = 'Waiting for Transcribe'
    TRANSCRIBING = 'Transcribing Report'
    PROVISIONAL = 'Provisional Report'

    @classmethod
    def choices(cls):
        return ((choice.name, choice.value) for choice in cls)


class Report(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    procedure = models.CharField(max_length=100, null=True, blank=True)
    clinical_information = models.TextField(null=True, blank=True)
    comparison = models.TextField(null=True, blank=True)
    findings = models.TextField(null=True, blank=True)
    actionable_findings = models.CharField(max_length=100, null=False, blank=False, default='None')
    impression = models.TextField(null=True, blank=True)
    technique = models.TextField(null=True, blank=True)

    template_name = models.CharField(max_length=200)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    signed_at = models.DateTimeField(blank=True, null=True)

    assignment = models.OneToOneField(Assignment, related_name='report', on_delete=models.CASCADE, )

    status = FSMField(choices=ReportStatus.choices(), verbose_name='Reporting Status',
                      default=ReportStatus.NEW.name, protected=False)

    # provider = models.ForeignKey(Provider, null=True, blank=True, related_name='report_templates', on_delete=models.CASCADE)
    # doctor = models.ForeignKey(Doctor, null=True, blank=True, related_name='report_templates', on_delete=models.CASCADE)

    @transition(field=status, source=[ReportStatus.NEW.name, ReportStatus.DRAFT.name], target=ReportStatus.DRAFT.name)
    def edit_draft(self, user=None):
        logger.info('Editing the report {} with state {}'.format(self.id, self.get_status_display()))
        ReportUpdatedSignal.send(self.__class__, user=user, action_text='Draft report saved', report=self)

    @transition(field=status,
                source=[ReportStatus.NEW.name, ReportStatus.DRAFT.name,
                        ReportStatus.PROVISIONAL.name, ReportStatus.REVISED.name],
                target=ReportStatus.FINALISED.name)
    def sign_report(self, user=None):
        def get_assignment_guid(report):
            return report.assignment.guid if report.assignment_id else None

        def get_study_guid(report):
            return report.assignment.study.guid if report.assignment_id and report.assignment.study_id else None

        logger.info(f"Sign report {self.id} from state: {self.get_status_display()}")
        self.signed_at = timezone.now()
        self._generate_pdf(self.id)
        ReportUpdatedSignal.send(self.__class__, user=user, action_text='Report is finalized', report=self)
        # FIMXME: Avoid accessing study guid from here.
        assignment_guid = get_assignment_guid(self)
        study_guid = get_study_guid(self)
        ReportSignedSignal.send(self.__class__, report=self, radiologist=self.assignment.assigned_to,
                                study_guid=study_guid,assignment_guid=assignment_guid)

    @transition(field=status,
                source=[ReportStatus.FINALISED.name, ReportStatus.REVISED.name],
                target=ReportStatus.REVISED.name)
    def revise_report(self, user=None):
        logger.info(f'Revise the final report {self.id}')
        ReportUpdatedSignal.send(self.__class__, user=user, action_text='Revising report', report=self)

    @classmethod
    def _generate_pdf(cls, report_id):
        # TODO: get models and create report
        pass

    def is_signed(self):
        return self.status == ReportStatus.FINALISED.name

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('report-fetch', args=[str(self.id)])

    def __str__(self):
        return str(self.id)

    class Meta:
        db_table = "reports"
        permissions = (
            ('can_create', 'Create Report Permission'),
            ('can_edit', 'Edit Report Permission'),
            ('can_sign', 'Sign report Permission'),
            ('can_revise', 'Revise Report Permission'),
            ('can_edit_revised', 'Edit Revised Report Permission'),
            ('can_sign_revised', 'Sign Revised Report Permission'),
        )


@receiver(post_save, sender=Report)
def set_report_ready(sender, **kwargs):
    logger.info(f'Report is saved. {sender}, {kwargs}')
    instance = kwargs.pop('instance')
    if instance.is_signed:
        logger.info('Report is Ready. Update assignment field')
        instance.assignment.report_ready = True
        instance.assignment.save()
