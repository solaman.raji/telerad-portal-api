import logging

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import HttpResponse
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR
from rest_framework.views import APIView

from assignment.models import Assignment
from organization.decorators import check_group_validation, check_user_profile
from report.models import Report
from report.serializers import ReportCreateSerializer, ReportDetailsSerializer, ReportUpdateSerializer
from telerad_portal_api.utils import ViewMethod
from .pdf import generate_html
from .pdf import generate_pdf

logger = logging.getLogger(__name__)

# FIXME: Duplicate view names in views and serializers
# FIXME: Use Enum instead
REPORT_CREATE = 'report-create'
REPORT_FETCH = 'report-fetch'
GENERATE_REPORT_PDF = 'generate-report-pdf'
GENERATE_REPORT_HTML = 'generate-report-html'
SAVE_DRAFT_REPORT = 'save-draft-report'
SIGN_REPORT = 'sign-report'
REVISE_REPORT = 'revise-report'


def index(request):
    return HttpResponse("Hello, world. You're at the report index.")


class ReportView(APIView):
    permission_classes = (IsAuthenticated,)

    @check_group_validation(ViewMethod.REPORT_CREATE.value)
    @method_decorator(check_user_profile)
    # FIXME: Check if the radiologist is assigned to this assignment
    def post(self, request):
        logger.info('Create Report for request {}'.format(request.data))

        try:
            assignment_guid = request.data.pop('assignment_guid')
            assignment = Assignment.objects.get(guid=assignment_guid)
            request.data['assignment'] = assignment.id
            if assignment.assigned_to != request.user:
                return Response(f'{request.user} is not assigned to report assignment: {assignment_guid}',
                                status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            return Response('Assignment guid not found to report', status=status.HTTP_400_BAD_REQUEST)
        except Assignment.DoesNotExist:
            return Response(f'Assignment not found with guid: {assignment_guid }', status=status.HTTP_400_BAD_REQUEST)

        create_serializer = ReportCreateSerializer(data=request.data, context={'request': request})

        if create_serializer.is_valid():
            create_serializer.save()
            logger.info('Report is created {}'.format(create_serializer.instance.id))
            details_serializer = ReportDetailsSerializer(create_serializer.instance, context={'request': request})
            return Response(details_serializer.data, status=status.HTTP_201_CREATED)

        return Response(create_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @check_group_validation(ViewMethod.REPORT_FETCH.value)
    @method_decorator(check_user_profile)
    def get(self, request, guid):
        report = Report.objects.get(id=guid)
        serializer = ReportDetailsSerializer(report, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    @check_group_validation(ViewMethod.REPORT_UPDATE.value)
    @method_decorator(check_user_profile)
    def put(self, request, guid):
        report = Report.objects.get(id=guid)
        logger.info('Update report: {}'.format(guid))

        if request.data.get('assignment_guid'):
            assignment_guid = request.data.pop('assignment_guid')
            assignment = Assignment.objects.get(guid=assignment_guid)
            request.data['assignment'] = assignment.id

        update_serializer = ReportUpdateSerializer(report, data=request.data, partial=True)

        if update_serializer.is_valid():
            update_serializer.save()
            logger.info('Report {} is updated'.format(guid))
            return Response(data=ReportDetailsSerializer(report, context={'request': request}).data,
                            status=status.HTTP_200_OK)

        return Response(update_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Decorating the class
# @method_decorator(check_user_profile, name='dispatch')
class ReportSaveDraftView(APIView, PermissionRequiredMixin):
    permission_classes = (IsAuthenticated,)
    permission_required = ('report.can_create', 'report.can_edit', 'report.can_sign')

    @check_group_validation(ViewMethod.REPORT_UPDATE.value)
    @method_decorator(check_user_profile)
    def put(self, request, guid):
        report = Report.objects.get(id=guid)
        request.data.pop('assignment_guid') if 'assignment_guid' in request.data else None
        logger.info(f'Save Draft report for {guid}')

        serializer = ReportUpdateSerializer(report, data=request.data, partial=True)

        if serializer.is_valid():
            report.edit_draft(user=request.user)
            logger.info('serializer validated data: {}'.format(serializer.validated_data))
            serializer.save()
            logger.info(f'Draft report for {guid} is saved')
            return Response(data=ReportDetailsSerializer(serializer.instance, context={'request': request}).data,
                            status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReportSignView(APIView, PermissionRequiredMixin):
    permission_classes = (IsAuthenticated,)
    permission_required = ('report.can_sign',)

    @check_group_validation(ViewMethod.REPORT_UPDATE.value)
    @method_decorator(check_user_profile)
    def put(self, request, guid):
        report = Report.objects.get(id=guid)
        request.data.pop('assignment_guid') if 'assignment_guid' in request.data else None
        logger.info(f'Sign report: {guid}, current status: {report.status}')

        serializer = ReportUpdateSerializer(report, data=request.data, partial=True)

        if serializer.is_valid():
            logger.info('serializer validated data: {}'.format(serializer.validated_data))
            report.sign_report(user=request.user)
            serializer.save()
            logger.info(f'Signed report: {guid}, current status: {report.status}')
            return Response(data=ReportDetailsSerializer(report, context={'request': request}).data,
                            status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReportSaveRevisedView(APIView, PermissionRequiredMixin):
    permission_classes = (IsAuthenticated,)
    permission_required = ('report.can_revise',)

    @check_group_validation(ViewMethod.REPORT_UPDATE.value)
    @method_decorator(check_user_profile)
    def put(self, request, guid):
        report = Report.objects.get(id=guid)
        logger.info(f'Revise Report {guid} for request {request.data}')
        request.data.pop('assignment_guid') if 'assignment_guid' in request.data else None

        serializer = ReportUpdateSerializer(report, data=request.data, partial=True)

        if serializer.is_valid():
            report.revise_report(user=request.user)
            logger.info('serializer data: {}'.format(serializer.validated_data))
            serializer.update(report, serializer.validated_data)
            logger.info(f'Revise report for {guid} is saved')
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReportPdfView(APIView):
    permission_classes = (IsAuthenticated,)

    # TODO: Add authentication and group checking
    # TODO: Check status when the report is ready
    def get(self, request, guid):
        template_path = 'report/general.html'
        print_header = request.GET.get('print-header', False)
        logger.info('Pdf generation request for report {}'.format(guid))
        report = Report.objects.get(id=guid)
        rendered_html = generate_html(report.assignment.study, report, template_path, print_header=print_header)
        logger.info('HTML is generated for report.')

        # Create a Django response object, and specify content_type as pdf
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = f'attachment; filename="report-{guid}.pdf"'

        try:
            generate_pdf(rendered_html, response)
            return response
        except:
            logger.exception('Error in generating pdf for report {}'.format(guid))
            return HttpResponse('We had some errors in generating pdf', status=500)


class ReportHTMLView(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = [TemplateHTMLRenderer]
    template_with_header = 'report-2/index.html'
    template_no_header = 'report-2/index-withoutheader.html'
    template_name = None

    # TODO: Add authentication and group checking
    # TODO: Check status when the report is ready
    def get(self, request, guid):
        # TODO: Get boolean for print header
        print_header = request.GET.get('print-header', False)

        self.template_name = self.template_with_header if print_header else self.template_no_header
        template_location = self.template_name
        logger.info(f'HTML generation request for report {guid} header: {print_header}, template: {self.template_name}')
        _report = Report.objects.get(id=guid)
        _study = _report.assignment.study

        try:
            context = {'study': _study,
                       'report': _report,
                       'radiologist': _report.assignment.assigned_to,
                       'radiologist_signature': _report.assignment.assigned_to.profile.signature,
                       'source_facility': _study.facility,
                       'destination_facility': _report.assignment.facility,
                       }
            logger.info(f'Generate html for study {_study.guid} and report {_report.id}')
            logger.info(f'Report generation with context: {context}')

            template = get_template(template_location)
            html = template.render(context=context)
            return HttpResponse(html)
        except Exception as ex:
            logger.error(f'Can not generate HTML for report {guid}. Error: {ex}')
            logger.exception(ex)
            return HttpResponse(f'Errors in generating html: {ex}', status=HTTP_500_INTERNAL_SERVER_ERROR)
