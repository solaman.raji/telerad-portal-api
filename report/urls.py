from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('reports', views.index, name='index'),
    path('reports/', views.ReportView.as_view(), name=views.REPORT_CREATE),
    path('reports/<uuid:guid>/', views.ReportView.as_view(), name=views.REPORT_FETCH),
    path('reports/<uuid:guid>/pdf/', views.ReportPdfView.as_view(), name=views.GENERATE_REPORT_PDF),
    path('reports/<uuid:guid>/html/', views.ReportHTMLView.as_view(), name=views.GENERATE_REPORT_HTML),
    path('reports/<uuid:guid>/save-draft/', views.ReportSaveDraftView.as_view(), name=views.SAVE_DRAFT_REPORT),
    path('reports/<uuid:guid>/sign/', views.ReportSignView.as_view(), name=views.SIGN_REPORT),
    path('reports/<uuid:guid>/revise/', views.ReportSaveRevisedView.as_view(), name=views.REVISE_REPORT),
]

urlpatterns = format_suffix_patterns(urlpatterns)
