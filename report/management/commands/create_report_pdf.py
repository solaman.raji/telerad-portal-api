import uuid

from django.core.management.base import BaseCommand, CommandError

from report.models import Report
from report.pdf import generate_html
from report.pdf import generate_pdf


class Command(BaseCommand):
    help = 'Generate report pdf from command line'

    def handle(self, *args, **options):
        report_guid = options.get('guid')
        template_path = options.get('template')
        output_filepath = options.get('output_filepath')
        print_header = options.get('print_header')
        self.stdout.write(f'Create pdf report with {report_guid} using template "{template_path}"')
        self.stdout.write('PDF will be available at "{output_filepath}" {header} header'.format(
            output_filepath=output_filepath,
            header='with' if print_header else 'without'))

        try:
            report = Report.objects.get(id=report_guid)
            rendered_html = generate_html(report.assignment.study, report, template_path, print_header=print_header)
            generate_pdf(rendered_html, output_filepath)

            self.stdout.write(self.style.SUCCESS(f'Successfully create pdf for report with {report_guid}'))
        except Report.DoesNotExist:
            raise CommandError(f'Report with "{report_guid}" does not exists')
        except Exception as ex:
            raise CommandError('Exception occurs: %s' % ex)

    def add_arguments(self, parser):
        parser.add_argument('guid', type=uuid.UUID, help='Specify report guid to print')
        parser.add_argument('output_filepath', type=str, help='Output file path for pdf')
        parser.add_argument('--template', type=str, help='Name of the facility', default='report/general.html')
        parser.add_argument('--print-header', action='store_true', dest='print_header', help='Print Report header')
