import logging

import django.dispatch
from django.core.signals import request_finished, request_started

logger = logging.getLogger(__name__)


def request_started_callback(sender, **kwargs):
    logger.info("Request started!")


def request_finished_callback(sender, **kwargs):
    logger.info("Request finished!")


request_finished.connect(request_finished_callback)
request_started.connect(request_started_callback)

ReportUpdatedSignal = django.dispatch.Signal(providing_args=['user', 'action_text', 'report'])

ReportSignedSignal = django.dispatch.Signal(providing_args=['report', 'radiologist', 'study_guid', 'assignment_guid'])
