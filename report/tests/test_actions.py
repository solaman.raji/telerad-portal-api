from django.test import TestCase

from assignment.models import Assignment
from report.models import Report, ReportStatus


class ReportModelStatusTestCase(TestCase):
    @classmethod
    def new_report(cls, status=None):
        assignment = Assignment()
        report_data = dict(procedure='procedure',
                           clinical_information='clinical_information',
                           comparison='comparison',
                           findings='findings',
                           assignment=assignment)
        if status:
            report_data['status'] = status
        report = Report(**report_data)
        return report

    def test_create_report(self):
        report = self.new_report()

        self.assertEqual(report.findings, 'findings')
        self.assertIsNotNone(report.id)
        self.assertEqual(report.status, ReportStatus.NEW.name)

    def test_save_report_to_draft(self):
        report = self.new_report()
        report.findings = 'New findings'
        report.actionable_findings = 'Urgent'

        report.edit_draft()

        self.assertEqual(report.findings, 'New findings')
        self.assertEqual(report.actionable_findings, 'Urgent')
        self.assertEqual(report.status, ReportStatus.DRAFT.name)

    def test_edit_draft_report(self):
        report = self.new_report()
        report.findings = 'New findings'
        report.edit_draft()

        report.actionable_findings = 'Urgent'
        report.findings = 'More findings'
        report.edit_draft()

        self.assertEqual(report.findings, 'More findings')
        self.assertEqual(report.actionable_findings, 'Urgent')
        self.assertEqual(report.status, ReportStatus.DRAFT.name)

    def test_sign_report(self):

        for status in [ReportStatus.NEW.name, ReportStatus.DRAFT.name, ReportStatus.PROVISIONAL.name,
                       ReportStatus.REVISED.name]:
            report = self.new_report(status)
            report.findings = 'New findings'

            self.assertEqual(report.status, status)
            report.sign_report()
            self.assertEqual(report.status, ReportStatus.FINALISED.name)

    def test_revise_report(self):

        for status in [ReportStatus.FINALISED.name, ReportStatus.REVISED.name]:
            report = self.new_report(status)
            report.findings = 'revised findings'

            self.assertEqual(report.status, status)
            report.revise_report()
            self.assertEqual(report.status, ReportStatus.REVISED.name)
