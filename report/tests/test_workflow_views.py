import json
import uuid

from django.test import TestCase
# FIXME: Get rid of assignment dependency
from rest_framework.reverse import reverse

from assignment.models import Assignment
from organization.model_utils import create_radiologist
from organization.models import Facility
from report import views
from report.models import Report, ReportStatus
from study.models import Study


class ReportWorkflowViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.radiologist_username = 'rl-sevenhills'
        cls.facility = Facility.objects.create(name='Jaslok Hospital')
        cls.radiologist = create_radiologist(cls.facility, cls.radiologist_username, password='qweqwe')
        cls.study = Study.objects.create(guid=uuid.uuid4(), study_instance_uid='study_instance_uid-1111')

        cls.assignment_guid = 'ac765ca1-9db6-4963-9ce7-2fd15f9b1672'
        Assignment.objects.create(guid=cls.assignment_guid, study=cls.study, radiologist=cls.radiologist)

    def setUp(self):
        self.report_data = dict(procedure='procedure',
                                clinical_information='clinical_information',
                                comparison='comparison',
                                findings='findings',
                                template_name='best-template',
                                assignment_guid=self.assignment_guid)

        self.client.login(username=self.radiologist_username, password='qweqwe')

    @classmethod
    def new_report(cls, status=None):
        assignment = Assignment.objects.get(guid='ac765ca1-9db6-4963-9ce7-2fd15f9b1672')
        report_data = dict(procedure='procedure',
                           clinical_information='clinical_information',
                           comparison='comparison',
                           findings='findings',
                           assignment=assignment)
        if status:
            report_data['status'] = status
        report = Report(**report_data)
        report.save()
        return report.id, report

    def test_create_report(self):
        self.client.login(username=self.radiologist_username, password='qweqwe')
        response = self.client.post('/api/reports/', data=json.dumps(self.report_data),
                                    content_type='application/json')
        print(response.content)
        self.assertEqual(response.status_code, 201)

        report_id = response.json()['guid']
        self.assertIsNotNone(report_id)

        report = Report.objects.get(id=report_id)
        self.assertEqual(report.status, ReportStatus.NEW.name)

    def test_edit_report(self):
        guid, report = self.new_report()

        self.client.login(username=self.radiologist_username, password='qweqwe')

        procedure = 'CT Cardiac Bypass Graft'
        impression = 'Abdominal pain'

        self.report_data['procedure'] = procedure
        self.report_data['impression'] = impression
        response = self.client.put(f'/api/reports/{guid}/save-draft/', data=json.dumps(self.report_data),
                                   content_type='application/json')

        updated_report = response.json()
        print(updated_report)
        self.assertEqual(updated_report.get('procedure'), procedure)
        self.assertEqual(updated_report.get('impression'), impression)
        # FIXME: Following is failing. Direct update should be restricted.
        self.assertEqual(updated_report.get('status'), ReportStatus.DRAFT.name)

        _links = [
            {'name': views.SAVE_DRAFT_REPORT,
             'url': reverse(views.SAVE_DRAFT_REPORT, [guid], request=response.wsgi_request)
             },
            {'name': views.SIGN_REPORT, 'url': reverse(views.SIGN_REPORT, [guid], request=response.wsgi_request)}
        ]
        print('Expected links', _links)
        print('Actual links ', updated_report.get('_links'))
        self.assertEqual(updated_report.get('_links'), _links)

    def test_sign_report(self):
        guid, report = self.new_report()

        self.client.login(username=self.radiologist_username, password='qweqwe')

        response = self.client.put(f'/api/reports/{guid}/sign/')
        updated_report = response.json()

        self.assertEqual(response.status_code, 200)

        signed_report = Report.objects.get(id=guid)
        self.assertEqual(signed_report.status, ReportStatus.FINALISED.name)
        _links = [
            {'name': views.REVISE_REPORT,
             'url': reverse(views.REVISE_REPORT, [guid], request=response.wsgi_request)
             },
        ]
        print('Expected links', _links)
        print('Actual links ', updated_report.get('_links'))
        self.assertEqual(updated_report.get('_links'), _links)

    def test_sign_report_with_edit(self):
        guid, report = self.new_report()

        self.client.login(username=self.radiologist_username, password='qweqwe')
        self.report_data['clinical_information'] = 'Abdominal pain'

        response = self.client.put(f'/api/reports/{guid}/sign/', data=json.dumps(self.report_data),
                                   content_type='application/json')
        self.assertEqual(response.status_code, 200)

        signed_report = Report.objects.get(id=guid)
        self.assertEqual(signed_report.clinical_information, 'Abdominal pain')
        self.assertEqual(signed_report.status, ReportStatus.FINALISED.name)
