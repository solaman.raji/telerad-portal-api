import json
import uuid

from django.test import TestCase

from assignment.models import Assignment
from organization.model_utils import create_radiologist
from organization.models import Facility
from report.models import Report, ReportStatus
from study.models import Study


class ReportViewTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.facility = Facility.objects.create(name='Jaslok Hospital')
        cls.radiologist_username = 'rl-sevenhills'
        cls.radiologist = create_radiologist(cls.facility, cls.radiologist_username, password='qweqwe')
        cls.study = Study.objects.create(guid=uuid.uuid4(), study_instance_uid='study_instance_uid-1111')

        cls.assignment_guid = 'ac765ca1-9db6-4963-9ce7-2fd15f9b1672'
        Assignment.objects.create(guid=cls.assignment_guid, study=cls.study, radiologist=cls.radiologist)

    def setUp(self):
        self.report_data = dict(procedure='procedure',
                                clinical_information='clinical_information',
                                comparison='comparison',
                                findings='findings',
                                template_name='best-template',
                                assignment_guid=self.assignment_guid)
        self.client.login(username=self.radiologist_username, password='qweqwe')

    def new_report(self, status=None):
        assignment = Assignment.objects.get(guid=self.assignment_guid)
        report_data = dict(procedure='procedure',
                           clinical_information='clinical_information',
                           comparison='comparison',
                           findings='findings',
                           assignment=assignment)
        if status:
            report_data['status'] = status
        report = Report(**report_data)
        report.save()
        return report.id, report

    def test_create_report(self):
        response = self.client.post('/api/reports/', data=json.dumps(self.report_data),
                                    content_type='application/json')
        print(response.content)
        self.assertEqual(response.status_code, 201)

        report_guid = response.json()['guid']
        self.assertIsNotNone(report_guid)

    def test_get_report(self):
        guid, report = self.new_report()

        response = self.client.get(f'/api/reports/{guid}/')
        report_dict = response.json()
        self.assertEqual(report_dict['procedure'], report.procedure)
        self.assertEqual(report_dict['status'], ReportStatus.NEW.name)

    def test_update_report(self):
        guid, report = self.new_report()

        new_procedure = 'CT Cardiac Bypass Graft'
        self.report_data['procedure'] = new_procedure
        response = self.client.put(f'/api/reports/{guid}/', data=json.dumps(self.report_data),
                                   content_type='application/json')

        self.assertContains(response, status_code=200, text='procedure')
        updated_report = response.json()
        self.assertEqual(updated_report.get('procedure'), new_procedure)
        # FIXME: Following is failing. Direct update should be restricted.
        # self.assertEqual(updated_report.get('status'), ReportStatus.DRAFT.name)

    def test_critical_actionable_findings_activity(self):
        guid, report = self.new_report()

        actionable_findings = 'Critical Findings'
        self.report_data['actionable_findings'] = actionable_findings
        response = self.client.put(f'/api/reports/{guid}/', data=json.dumps(self.report_data),
                                   content_type='application/json')

        self.assertContains(response, status_code=200, text='actionable_findings')
        updated_report = response.json()
        self.assertEqual(updated_report.get('actionable_findings'), actionable_findings)
        # self.assertEqual(updated_report.get('status'), ReportStatus.DRAFT.name)

        # FIXME: Check activity for actionable_findings
