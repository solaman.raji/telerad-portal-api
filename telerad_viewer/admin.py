from django.contrib import admin

from telerad_viewer.models import StudyRef, UserRef

admin.site.register(StudyRef)
admin.site.register(UserRef)
