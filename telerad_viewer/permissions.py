from rest_framework import permissions


class HasUserProfilePermission(permissions.BasePermission):
    """
    The request is authenticated if the user has a profile
    """

    def has_permission(self, request, view):
        return (
            hasattr(request.user, 'profile')
        )
