from django.apps import AppConfig


class TeleradViewerConfig(AppConfig):
    name = 'telerad_viewer'
    verbose_name = 'Study Access for clinicians'

    def ready(self):
        # from . import signals
        import telerad_viewer.signals
