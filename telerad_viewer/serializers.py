from rest_framework import serializers

from telerad_viewer.models import StudyRef

REPORT_FETCH = 'report-fetch'


class StudyRefSerializer(serializers.HyperlinkedModelSerializer):
    self = serializers.HyperlinkedIdentityField(view_name='studyref-detail', lookup_field='guid')
    study_details = serializers.HyperlinkedIdentityField(view_name='study-details', lookup_field='guid')
    report_details = serializers.HyperlinkedRelatedField(view_name=REPORT_FETCH, lookup_field='report_guid',
                                                         lookup_url_kwarg='guid', read_only=True)

    class Meta:
        model = StudyRef
        fields = ('guid', 'priority', 'modality', 'body_part',
                  'patient_name', 'patient_gender', 'patient_date_of_birth', 'patient_age',
                  'study_date', 'study_time', 'number_of_dicom_images', 'viewer_url', 'self', 'study_details',
                  'report_guid', 'actionable_findings', 'report_details',
                  )
