import logging

from django.db.models import signals
from django.dispatch import receiver

from study.models import Study
from telerad_viewer.models import StudyRef, UserRef
from report.models import Report
from report.signals import ReportSignedSignal

logger = logging.getLogger(__name__)


@receiver(signals.post_save, sender=Study)
def sync_referred_study(sender, **kwargs):
    instance = kwargs.pop('instance')
    created = kwargs.pop('created')
    raw = kwargs.pop('raw')
    using = kwargs.pop('using')
    update_fields = kwargs.pop('update_fields')
    logger.info(f'Study post_save signal triggered for {instance}, created: {created}, raw: {raw}, using: {using}')
    logger.info(f'Updated fields: {update_fields}')

    defaults = {
        'study_instance_uid': instance.study_instance_uid,
        'priority': instance.priority,
        'modality': instance.modality,
        'body_part': instance.body_part.name if instance.body_part_id else None,
        'patient_name': instance.patient_name,
        'patient_gender': instance.patient_gender,
        'patient_date_of_birth': instance.patient_date_of_birth,
        'patient_year_of_birth': instance.patient_year_of_birth,
        'study_date': instance.study_date,
        'study_time': instance.study_time,
        'number_of_dicom_images': instance.number_of_dicom_images,
        'zip_file': instance.zip_file,
    }
    if instance.referring_physician_id:
        logger.info(f'Referring physician {instance.referring_physician} is attached with study')
        user_ref_obj, _ = UserRef.objects.get_or_create(guid=instance.referring_physician.profile.guid)
        defaults.update({'referring_physician': user_ref_obj})
    elif instance.referring_physician_name:
        logger.info(f'Referring physician name {instance.referring_physician_name} is set.')
        # defaults.update({'referring_physician_name': instance.referring_physician_name})
        defaults.update({'referring_physician': None})
    else:
        logger.info(f'No Referring physician information is found for study {instance.guid}')
        defaults.update({'referring_physician': None})

    StudyRef.objects.update_or_create(guid=instance.guid, defaults=defaults)


@receiver(signals.post_delete, sender=Study)
def remove_deleted_study(sender, **kwargs):
    instance = kwargs.pop('instance')
    logger.info(f'Study post_delete signal triggered for study {instance.guid}')
    StudyRef.objects.filter(guid=instance.guid).delete()


@receiver(ReportSignedSignal, sender=Report)
def sync_signed_report(sender, **kwargs):
    logger.info(f'Report is signed. sender: {sender}, arguments: {kwargs}')
    print(f'Report is signed. sender: {sender}, arguments: {kwargs}')

    report = kwargs.pop('report')
    radiologist = kwargs.pop('radiologist')
    study_guid = kwargs.pop('study_guid')
    assignment_guid = kwargs.pop('assignment_guid')
    logger.info(f'Report signed signal triggered for {report} signed by {radiologist}, study: {study_guid}')
    logger.info(f'Report signed for study: {study_guid} and assignment: {assignment_guid}')

    if not study_guid:
        logger.warning('Study guid not found for signed report')
        return
    study_ref = StudyRef.objects.get(guid=study_guid)
    study_ref.report_guid = report.id
    study_ref.actionable_findings = report.actionable_findings
    study_ref.assignment_guid = assignment_guid
    study_ref.save()
    logger.info('Signed report is synced to StudyRef')
