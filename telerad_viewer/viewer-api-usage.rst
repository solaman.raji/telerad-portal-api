==========================
Telerad Viewer API Usage
==========================


Set environment variable
-------------------------

An environment variable should be set to navigate the api against different servers
::

    export BASE_DOMAIN=http://localhost:8000
    export BASE_DOMAIN=https://dev-telerad-portal-api.alem.health
    export BASE_DOMAIN=https://staging-telerad-portal-api.alem.health

Point to what is the destination server.

Explore browsable API
======================

In your favourite browser, open to the following url and login using a valid clinician credential.
In current implementation, physician is a clinician.

::
    http://localhost:8000/viewer/api/
    https://dev-telerad-portal-api.alem.health/viewer/api/
    https://staging-telerad-portal-api.alem.health/viewer/api/

    http://localhost:8000/viewer/auth/login/?next=/viewer/api
    https://dev-telerad-portal-api.alem.health/viewer/auth/login/?next=/viewer/api
    https://staging-telerad-portal-api.alem.health/viewer/auth/login/?next=/viewer/api


--------------------
Authenticate a user
--------------------
Authentication process for user is same as common api described at `telerad portal api`_.


Get JWT token
-------------
::

    curl -v -H "Content-Type:application/json" -d '{"username": "f1_phy1", "password":"qweqwe"}' $BASE_DOMAIN/api/api-token-auth/
    curl -v -H "Content-Type:application/json" -d '{"username": "f1_phy2", "password":"qweqwe"}' $BASE_DOMAIN/api/api-token-auth/

    # Copy the returned token string
    TOKEN_STR=eyJ0...
    JWT_TOKEN="JWT $TOKEN_STR"

Verify the token
-----------------
::

    json_data={\"token\":\"$TOKEN_STR\"}
    curl -v -H "Content-Type:application/json" -d $json_data $BASE_DOMAIN/api/api-token-verify/

    echo '{"token": "$TOKEN_STR"}' | curl -v -H "Content-Type:application/json"  $BASE_DOMAIN/api/api-token-verify/

    curl -v -H "Authorization:$JWT_TOKEN" $BASE_DOMAIN/api/configurations/




-----------
Root API
-----------

Use the following to get the list of endpoint for viewer
::
    curl -v -H "Authorization:$JWT_TOKEN" -H "Content-Type:application/json" $BASE_DOMAIN/viewer/api/

    curl -H "Authorization:$JWT_TOKEN" -H "Content-Type:application/json" $BASE_DOMAIN/viewer/api/ | python -m json.tool


Get list of studies
--------------------
::

    curl -v -H "Authorization:$JWT_TOKEN" -H "Content-Type:application/json" $BASE_DOMAIN/viewer/api/studies/

    curl -H "Authorization:$JWT_TOKEN" -H "Content-Type:application/json" $BASE_DOMAIN/viewer/api/studies/ | python -m json.tool


Study details API
-----------------

Get study ref details
::

    curl -v -H "Authorization:$JWT_TOKEN" -H "Content-Type:application/json" $BASE_DOMAIN/viewer/api/studies/91cf4aa4-c5f9-4aff-8411-98a61ac78f65

Get study details from telerad-api
-----------------------------------
::

    curl -v -H "Authorization:$JWT_TOKEN" -H "Content-Type:application/json" $BASE_DOMAIN/api/studies/e831b5a6-eca9-4f9a-8cb6-2c534c7a7b47/


Report API
-------------

work in progress

.. _`postman`: https://documenter.getpostman.com/view/324290/RW86KpX9
.. _`telerad portal api`: https://documenter.getpostman.com/view/324290/RW86KpX9
