from django.http import HttpResponse
from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

from telerad_viewer.views import StudyList, StudyRefDetail, ApiRoot, PhysicianList

urlpatterns = [
    path('auth/', include('rest_framework.urls'), ),

    path('auth-token/', obtain_jwt_token, name='auth-token'),
    path('refresh-token/', refresh_jwt_token, name='refresh-token'),
    path('verify-token/', verify_jwt_token, name='verify-token'),

    path('api/physicians/', PhysicianList.as_view(), name='physicians'),

    path('api/', ApiRoot.as_view()),
    path('api/studies/', StudyList.as_view(), name='study-list'),
    path('api/studies/<uuid:guid>', StudyRefDetail.as_view(), name='studyref-detail'),

    path('api/health-check/', lambda r: HttpResponse()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
