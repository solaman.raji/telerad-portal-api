from enum import Enum

# TODO: Duplicate data with study choices. How to merge them?


class GenderChoice(Enum):
    M = 'Male'
    F = 'Female'

    @classmethod
    def choices(cls):
        return ((choice.name, choice.value) for choice in GenderChoice)


class ModalityChoice(Enum):
    CT = 'CT'
    NM = 'NM'
    MR = 'MR'
    DS = 'DS'
    DR = 'DR'
    US = 'US'
    OT = 'OT'
    HSG = 'HSG'
    CR = 'CR'
    OP = 'OP'
    DX = 'DX'
    MG = 'MG'
    ECG = 'ECG'

    @classmethod
    def choices(cls):
        return ((choice.name, choice.value) for choice in ModalityChoice)


class PriorityChoice(Enum):
    routine = 'Routine'
    urgent = 'Urgent'

    @classmethod
    def choices(cls):
        return ((choice.name, choice.value) for choice in PriorityChoice)

