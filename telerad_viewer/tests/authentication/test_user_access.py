from django.contrib.auth.models import User
from django.test import TestCase

from organization.model_utils import create_physician, create_facility


class ClinicianJwtToken(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.password = 'qweqwe'
        cls.facility = create_facility('facility 1')
        cls.physician = create_physician(cls.facility, 'physician1', first_name='f1', last_name='l1')
        cls.physician.set_password(cls.password)
        cls.physician.save()
        cls.user = User.objects.create(username='user1')
        cls.user.set_password(cls.password)
        cls.user.save()

    def test_clinician_can_get_token(self):
        response = self.client.post('/viewer/auth-token/',
                                    data='{"username":"physician1", "password": "qweqwe"}',
                                    content_type='application/json')
        print(response.status_code)
        print(response.content)
        self.assertContains(response, 'token', status_code=200)

    def test_non_clinician_does_not_get_token(self):
        response = self.client.post('/viewer/auth-token/',
                                    data='{"username":"user1", "password": "qweqwe"}',
                                    content_type='application/json')
        print(response.status_code)
        print(response.content)
        self.assertContains(response, 'token', status_code=200)
