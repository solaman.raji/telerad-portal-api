from django.test import TestCase
from django.utils import timezone
from freezegun import freeze_time

from organization.model_utils import create_physician, create_facility
from study.models import Study
from telerad_viewer.models import StudyRef
from . import dummy_study_dict


@freeze_time("2015-05-25")
class StudyUpdateSync(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.study_data = dummy_study_dict()
        cls.study_obj = Study.objects.create(**cls.study_data)
        cls.study_guid = cls.study_obj.guid

    @classmethod
    def tearDownClass(cls):
        cls.study_obj.delete()

    def test_new_study_ref_created_for_study(self):
        with freeze_time("Jun 15th, 2016"):
            self.study_obj.priority = 'Urgent'
            self.study_obj.save()

        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)
        self.assertEqual(study_ref.guid, self.study_guid)
        self.assertEqual(study_ref.created_at, timezone.datetime(2015, 5, 25, tzinfo=timezone.utc))
        self.assertEqual(study_ref.updated_at, timezone.datetime(2016, 6, 15, tzinfo=timezone.utc))

    def test_new_study_has_modality_body_part(self):
        self.study_obj.modality = 'CR'
        self.study_obj.save()

        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertEqual(study_ref.modality, 'CR')
        self.assertEqual(study_ref.body_part, None)

    def test_new_study_has_patient_info(self):
        self.study_obj.patient_name = 'Bose Meghnad'
        self.study_obj.patient_gender = 'F'
        self.study_obj.patient_date_of_birth = '2003-05-01'
        self.study_obj.save()

        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)

        self.assertEqual(study_ref.patient_name, 'Bose Meghnad')
        self.assertEqual(study_ref.patient_gender, 'F')
        self.assertEqual(study_ref.patient_date_of_birth.isoformat(), '2003-05-01')

    def test_new_study_has_dicom_info(self):
        self.study_obj.number_of_dicom_images = 33
        self.study_obj.patient_gender = 'F'
        self.study_obj.patient_date_of_birth = '2003-05-01'
        self.study_obj.save()

        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)

        self.assertEqual(study_ref.number_of_dicom_images, 33)
        self.assertEqual(study_ref.zip_file, self.study_data['zip_file'])

    def test_new_study_has_referring_physician(self):
        facility = create_facility('facility 1')
        physician = create_physician(facility, 'roland', 'Roland', 'Beaulieu')

        self.study_obj.referring_physician = physician
        self.study_obj.referring_physician_name = None
        self.study_obj.save()

        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)

        self.assertIsNotNone(study_ref.referring_physician)
        self.assertEqual(study_ref.referring_physician.guid, physician.profile.guid)


@freeze_time("2015-05-25")
class StudyUpdateReferringPhysician(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.study_data = dummy_study_dict()
        cls.study_obj = Study.objects.create(**cls.study_data)
        cls.study_guid = cls.study_obj.guid

    @classmethod
    def tearDownClass(cls):
        cls.study_obj.delete()

    def setUp(self):
        facility = create_facility('facility 1')
        self.physician = create_physician(facility, 'roland', 'Roland', 'Beaulieu')
        self.physician_2 = create_physician(facility, 'stuart', 'Dan', 'Stuart')

        self.study_obj.referring_physician = self.physician
        self.study_obj.referring_physician_name = None
        self.study_obj.save()

    def tearDown(self):
        StudyRef.objects.all().delete()

    def test_referring_physician_is_updated_to_empty(self):
        self.study_obj.referring_physician = None
        self.study_obj.referring_physician_name = None
        self.study_obj.save()

        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)

        self.assertIsNone(study_ref.referring_physician)

    def test_referring_physician_name_is_set(self):
        self.study_obj.referring_physician = None
        self.study_obj.referring_physician_name = 'Harry Potter'
        self.study_obj.save()

        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)

        self.assertIsNone(study_ref.referring_physician)

    def test_update_referring_physician_user_overriding_name(self):
        self.study_obj.referring_physician = None
        self.study_obj.referring_physician_name = 'Harry Potter'
        self.study_obj.save()

        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)
        self.assertIsNone(study_ref.referring_physician)

        self.study_obj.referring_physician = self.physician_2
        self.study_obj.referring_physician_name = ''
        self.study_obj.save()

        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)
        self.assertIsNotNone(study_ref.referring_physician)
        self.assertEqual(study_ref.referring_physician.guid, self.physician_2.profile.guid)
