from django.test import TestCase
from django.utils import timezone
from freezegun import freeze_time

from study.models import Study
from telerad_viewer.models import StudyRef
from . import dummy_study_dict


@freeze_time("2012-01-14")
class StudyCreateSync(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.study_data = dummy_study_dict()

    def setUp(self):
        self.study_obj = Study.objects.create(**self.study_data)
        self.study_guid = self.study_obj.guid

    def tearDown(self):
        self.study_obj.delete()

    def test_new_study_ref_created_for_study(self):
        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)

        self.assertEqual(study_ref.guid, self.study_guid)
        self.assertEqual(study_ref.created_at, timezone.datetime(2012, 1, 14, tzinfo=timezone.utc))
        self.assertEqual(study_ref.updated_at, timezone.datetime(2012, 1, 14, tzinfo=timezone.utc))

    def test_new_study_has_modality_body_part(self):
        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)

        self.assertEqual(study_ref.modality, 'HSG')
        self.assertEqual(study_ref.body_part, None)

    def test_new_study_has_patient_info(self):
        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)

        self.assertEqual(study_ref.patient_name, 'Patient 1000')
        self.assertEqual(study_ref.patient_gender, 'M')
        self.assertEqual(study_ref.patient_date_of_birth.isoformat(), '2001-02-07')

    def test_new_study_has_dicom_info(self):
        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)

        self.assertEqual(study_ref.number_of_dicom_images, 199)
        self.assertEqual(study_ref.zip_file, self.study_data['zip_file'])

    def test_new_study_has_referring_physician(self):
        study_ref = StudyRef.objects.get(guid=self.study_guid)
        self.assertIsNotNone(study_ref)

        self.assertIsNone(study_ref.referring_physician)
