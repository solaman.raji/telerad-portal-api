from django.contrib.auth.models import User
from django.test import TestCase
from freezegun import freeze_time
from mixer.backend.django import mixer

from assignment.models import Assignment
from organization.models import Facility
from report.models import Report
from study.models import Study
from telerad_viewer.models import StudyRef
from . import dummy_study_dict


@freeze_time("2012-01-14")
class SignedReportSync(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.facility = mixer.blend(Facility)
        cls.radiologist_username = 'rl-sevenhills'
        cls.radiologist = mixer.blend(User)

        cls.study_data = dummy_study_dict()
        cls.study_obj = Study.objects.create(**cls.study_data)
        cls.study_guid = cls.study_obj.guid

        cls.assignment_guid = 'ac765ca1-9db6-4963-9ce7-2fd15f9b1672'
        # assignment = Assignment.objects.create(guid=cls.assignment_guid)
        assignment = Assignment.objects.create(guid=cls.assignment_guid, study=cls.study_obj,
                                               radiologist=cls.radiologist)

        cls.report_data = dict(procedure='procedure',
                               clinical_information='clinical_information',
                               comparison='comparison',
                               findings='findings',
                               actionable_findings='actionable_findings_111',
                               template_name='best-template',
                               assignment=assignment)

    def setUp(self):
        self.report = Report.objects.create(**self.report_data)

    def tearDown(self):
        Report.objects.all().delete()

    @classmethod
    def tearDownClass(cls):
        cls.study_obj.delete()

    def test_report_ref_created(self):
        self.report.sign_report()

        self.assertEqual(Report.objects.count(), 1)
        study_ref = StudyRef.objects.all().first()
        self.assertEqual(study_ref.actionable_findings, 'actionable_findings_111')
        self.assertEqual(study_ref.report_guid, self.report.id)

        # FIXME: AssertionError: UUID('ac765ca1-9db6-4963-9ce7-2fd15f9b1672') != 'ac765ca1-9db6-4963-9ce7-2fd15f9b1672'
        # self.assertEqual(study_ref.assignment_guid, self.report.assignment.guid)
        self.assertEqual(str(study_ref.assignment_guid), self.report.assignment.guid)
