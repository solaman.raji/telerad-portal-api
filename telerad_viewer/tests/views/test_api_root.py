from django.contrib.auth.models import User
from django.test import TestCase
from mixer.backend.django import mixer
from rest_framework.reverse import reverse

from organization.model_utils import create_radiologist, create_physician
from organization.models import Facility


class PhysiciansListCheck(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.facility = mixer.blend(Facility)
        cls.radiologist_username = 'rl-sevenhills'
        cls.radiologist = create_radiologist(cls.facility, cls.radiologist_username, password='qweqwe')

        cls.physician_name = 'roland'
        cls.physician = create_physician(cls.facility, cls.physician_name, 'Roland', 'Beaulieu', password='qweqwe')

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def assert_common_links(self, response_body, response):
        self.assertEqual(response_body['studies'], reverse('study-list', request=response.wsgi_request))
        self.assertEqual(response_body['auth-token'], reverse('auth-token', request=response.wsgi_request))
        self.assertEqual(response_body['refresh-token'], reverse('refresh-token', request=response.wsgi_request))
        self.assertEqual(response_body['verify-token'], reverse('verify-token', request=response.wsgi_request))

    def test_common_links_included_for_general_user(self):
        self.client.login(username=self.radiologist_username, password='qweqwe')
        response = self.client.get('/viewer/api/')
        self.assertEqual(response.status_code, 200)

        response_body = response.json()
        self.assert_common_links(response_body, response)

    def test_physician_links_included_for_physician_user(self):
        self.client.login(username=self.physician_name, password='qweqwe')
        response = self.client.get('/viewer/api/')
        self.assertEqual(response.status_code, 200)

        response_body = response.json()

        self.assert_common_links(response_body, response)

        self.assertTrue('physicians' in response_body)
        self.assertEqual(response_body['physicians'], reverse('physicians', request=response.wsgi_request))

    def test_physician_links_not_included_for_user_without_profile(self):
        User.objects.create(username='john', password='qweqwe')
        self.client.login(username='john', password='qweqwe')

        response = self.client.get('/viewer/api/')
        self.assertEqual(response.status_code, 200)

        response_body = response.json()

        self.assert_common_links(response_body, response)

        self.assertFalse('physicians' in response_body)
