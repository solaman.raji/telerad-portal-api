from django.test import TestCase
from mixer.backend.django import mixer

from assignment.models import Assignment
from organization.model_utils import create_radiologist
from organization.models import Facility
from report.models import Report
from study.models import Study
from . import dummy_study_dict


class StudyListView(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.facility = mixer.blend(Facility)
        cls.radiologist_username = 'rl-sevenhills'
        cls.radiologist = create_radiologist(cls.facility, cls.radiologist_username, password='qweqwe')

        cls.study_data = dummy_study_dict()

        cls.study_data['referring_physician'] = cls.radiologist
        cls.study_data['referring_physician_name'] = None
        cls.study_obj = Study.objects.create(**cls.study_data)
        cls.study_guid = cls.study_obj.guid

        cls.assignment_guid = 'ac765ca1-9db6-4963-9ce7-2fd15f9b1672'
        # assignment = Assignment.objects.create(guid=cls.assignment_guid)
        assignment = Assignment.objects.create(guid=cls.assignment_guid,
                                               study=cls.study_obj, radiologist=cls.radiologist)

        cls.report_data = dict(procedure='procedure',
                               clinical_information='clinical_information',
                               comparison='comparison',
                               findings='findings',
                               template_name='best-template',
                               assignment=assignment)

    def setUp(self):
        self.report = Report.objects.create(**self.report_data)
        self.client.login(username=self.radiologist_username, password='qweqwe')

    def test_study_list_without_report(self):
        response = self.client.get('/viewer/api/studies/')
        self.assertContains(response, text='results', status_code=200)
        result = response.json()['results'][0]
        print('result:', result)

        self.assertEqual(result['guid'], str(self.study_obj.guid))
        self.assertEqual(result['patient_name'], self.study_obj.patient_name)
        self.assertIsNotNone(result['study_details'])

        self.assertEqual(result['actionable_findings'], 'None')
        self.assertFalse('report_details' in result)

    def test_study_list_with_report(self):
        self.report.sign_report()
        response = self.client.get('/viewer/api/studies/')
        self.assertContains(response, text='results', status_code=200)
        result = response.json()['results'][0]
        print('Result with report:', result)

        self.assertEqual(result['guid'], str(self.study_obj.guid))
        self.assertEqual(result['patient_name'], self.study_obj.patient_name)
        self.assertIsNotNone(result['study_details'])

        self.assertEqual(result['report_guid'], str(self.report.id))
        self.assertEqual(result['actionable_findings'], self.report.actionable_findings)

        self.assertIsNotNone(result['report_details'])
        self.assertTrue(str(self.report.id) in result['report_details'])

