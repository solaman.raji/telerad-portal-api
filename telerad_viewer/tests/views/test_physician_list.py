from django.test import TestCase
from mixer.backend.django import mixer

from organization.model_utils import create_radiologist, create_physician
from organization.models import Facility


class PhysiciansListCheck(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.facility = mixer.blend(Facility)
        cls.radiologist_username = 'rl-sevenhills'
        cls.radiologist = create_radiologist(cls.facility, cls.radiologist_username, password='qweqwe')

        cls.physician_name = 'roland'
        cls.physician = create_physician(cls.facility, cls.physician_name, 'Roland', 'Beaulieu', password='qweqwe')

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_common_links_included_for_general_user(self):
        self.client.login(username=self.radiologist_username, password='qweqwe')
        response = self.client.get('/viewer/api/physicians/')
        self.assertEqual(response.status_code, 200)

        response_body = response.json()
        self.assertDictEqual(response_body, {str(self.physician.profile.guid): self.physician.get_full_name()})
