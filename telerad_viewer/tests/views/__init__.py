import uuid


def dummy_study_dict():
    guid = uuid.uuid4()
    study_instance_uid = '1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639'
    accession_number = 'AH-01-01'
    study_description = 'Knee (R)'
    institution_name = 'Crestview'
    station_name = 'Victoria Island',
    modality = "HSG"
    patient_id = 'P-1000'
    patient_name = 'Patient 1000'
    patient_gender = 'M'
    patient_date_of_birth = '2001-02-07'
    patient_telephone_number = '1234567890'
    referring_physician_name = 'Physician 2',
    additional_patient_history = 'This is additional patient history'
    number_of_dicom_images = 199
    zip_file = {'key': 'key1/key2/key3', 'bucket': 'bucket-name'}

    study_data = {
        'guid': guid,
        'study_instance_uid': study_instance_uid,
        'accession_number': accession_number,
        'study_description': study_description,
        'institution_name': institution_name,
        'station_name': station_name,
        'modality': modality,
        'patient_id': patient_id,
        'patient_name': patient_name,
        'patient_gender': patient_gender,
        'patient_date_of_birth': patient_date_of_birth,
        'patient_telephone_number': patient_telephone_number,
        'referring_physician_name': referring_physician_name,
        'additional_patient_history': additional_patient_history,
        'number_of_dicom_images': number_of_dicom_images,
        'zip_file': zip_file,
    }
    return study_data
