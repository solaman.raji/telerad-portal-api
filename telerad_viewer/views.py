from rest_framework import generics
from rest_framework import permissions
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from organization.views import ConfigurationList
from telerad_viewer.models import StudyRef
from telerad_viewer.serializers import StudyRefSerializer
from .permissions import HasUserProfilePermission


class ApiRoot(generics.GenericAPIView):
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication, BasicAuthentication)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    @staticmethod
    def add_common_links(links, request):
        assert links is not None, 'links should not be None'
        assert type(links) == dict, 'links should be of type dict'

        _common_links = {
            'studies': reverse('study-list', request=request),
            'auth-token': reverse('auth-token', request=request),
            'refresh-token': reverse('refresh-token', request=request),
            'verify-token': reverse('verify-token', request=request),
        }
        links.update(_common_links)

    @staticmethod
    def add_physicians_link(links, request):
        assert type(links) == dict, 'links should be of type dict'
        assert request.user is not None, 'User should not be None in request'

        user = request.user
        if hasattr(user, 'profile') and hasattr(user.profile, 'facility'):
            _facility = user.profile.facility
            links['physicians'] = reverse('physicians', request=request)

    def get(self, request):
        links = dict()
        self.add_common_links(links, request)
        self.add_physicians_link(links, request)
        return Response(links)


class PhysicianList(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, HasUserProfilePermission)

    def get(self, request):
        return Response(ConfigurationList.get_physicians(request.user.profile.facility), status=status.HTTP_200_OK)


class StudyList(generics.ListAPIView):
    queryset = StudyRef.objects.all()
    serializer_class = StudyRefSerializer
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication, BasicAuthentication)
    permission_classes = (permissions.IsAuthenticated, HasUserProfilePermission)

    def get_queryset(self):
        user = self.request.user
        if user.is_anonymous:
            return None
        return StudyRef.objects.filter(referring_physician__guid=user.profile.guid).all()


class StudyRefDetail(generics.RetrieveAPIView):
    queryset = StudyRef.objects.all()
    serializer_class = StudyRefSerializer
    permission_classes = (permissions.IsAuthenticated, HasUserProfilePermission)
    lookup_field = 'guid'
