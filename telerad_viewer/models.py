import logging

from django.conf import settings
from django.db import models
from django.utils import timezone, dateparse
from jsonfield import JSONField

from .choices import GenderChoice, ModalityChoice, PriorityChoice

logger = logging.getLogger(__name__)


class UserRef(models.Model):
    guid = models.UUIDField(unique=True, editable=False)

    class Meta:
        verbose_name_plural = 'Viewer Profile References'

    def __str__(self):
        return str(self.guid)


class StudyRef(models.Model):
    guid = models.UUIDField(unique=True, editable=False)
    study_instance_uid = models.CharField(max_length=100, blank=True)
    modality = models.CharField(max_length=10, null=True, blank=True, choices=ModalityChoice.choices())
    priority = models.CharField(max_length=10, null=True, blank=True, choices=PriorityChoice.choices(),
                                default=PriorityChoice.routine.name)
    body_part = models.CharField(max_length=30, null=True, blank=True)

    patient_name = models.CharField(max_length=50, null=True, blank=True)
    patient_gender = models.CharField(max_length=2, null=True, blank=True, choices=GenderChoice.choices())
    patient_date_of_birth = models.DateField(null=True, blank=True)
    patient_year_of_birth = models.CharField(max_length=4, null=True, blank=True)

    study_date = models.DateField(null=True, blank=True)
    study_time = models.TimeField(null=True, blank=True)

    number_of_dicom_images = models.IntegerField(null=True, blank=True)
    zip_file = JSONField(null=True, blank=True)

    referring_physician = models.ForeignKey(UserRef, related_name='referred_studies', null=True, blank=True,
                                            on_delete=models.SET_NULL)

    report_guid = models.UUIDField(editable=False, null=True, blank=True)
    actionable_findings = models.CharField(max_length=100, null=False, blank=False, default='None')

    assignment_guid = models.UUIDField(editable=False, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Study References'

    def __str__(self):
        return str(self.guid)

    @property
    def patient_age(self):
        # Write testcase to verify this method
        try:
            if self.patient_date_of_birth:
                # FIXME: The patient_date_of_birth should be a date field
                _born = dateparse.parse_date(self.patient_date_of_birth)
                return self._calculate_age(_born)
            elif self.patient_year_of_birth:
                # FIXME: patient_year_of_birth should be a integer field
                return timezone.now().year - int(self.patient_year_of_birth)
            else:
                return None
        except Exception:
            logger.exception('Exception in calculating age')
            return None

    @staticmethod
    def _calculate_age(born):
        # Adapted from https://stackoverflow.com/questions/2217488/age-from-birthdate-in-python#9754466
        today = timezone.now().today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    @property
    def viewer_url(self):
        # FIXME: Write testcase to verify this
        return settings.VIEWER_URL.format(self.study_instance_uid) if self.study_instance_uid else None
