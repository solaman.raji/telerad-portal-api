Telerad Portal API
===========================

Prepare Development environment
-------------------------------

###### Install docker

Install form `https://www.docker.com/get-docker` if not

Check if docker is installed
```bash
    docker info
    docker version
```

Run the docker compose
```bash
    docker-compose up
```

Run the docker compose with detach
```bash
    docker-compose up -d
```

Restart docker container
```bash
    docker restart <container NAME | ID>
```

To check the container log
```bash
    docker logs -f <container Name | ID>
```

Python command outside of container
```bash
    docker-compose run <container Name | ID> python manage.py migrate
```

Login In container
```bash
    docker exec -it <container Name | ID> sh
```

Remove Container
```bash
    docker-compose down
```

##### Populate local SQLite database
```bash
docker run -it -v $(pwd):/var/task -v $(pwd)/.venv:/var/venv --rm telerad-zappa-local
python manage.py makemigrations
python manage.py showmigrations
python manage.py migrate
python manage.py runscript initial_data
python manage.py loaddata api-key-dumpdata.json
python manage.py runscript import_procedure
python manage.py runscript import_body_part
python manage.py runscript import_report_templates

python manage.py runscript populate_facility_users
python manage.py runscript create_bulk_studies
python manage.py runscript create_bulk_assignments
python manage.py runscript import_organization_users

```