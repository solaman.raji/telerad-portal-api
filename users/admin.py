from django.contrib import admin

from users.models import Invitation, ResetPassword


class InvitationAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'guid',
        'first_name',
        'last_name',
        'email',
        'phone',
        'user_group',
        'facility',
        'expire_time',
        'created_at',
        'created_by',
        'is_deleted',
        'deleted_at'
    )


class ResetPasswordAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'guid',
        'email',
        'user',
        'expire_time',
        'created_at',
        'is_deleted',
        'deleted_at'
    )

admin.site.register(Invitation, InvitationAdmin)
admin.site.register(ResetPassword, ResetPasswordAdmin)
