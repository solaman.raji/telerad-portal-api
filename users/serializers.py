from rest_framework import serializers

from telerad_portal_api.utils import GroupName
from users.models import Invitation
from users.models import ResetPassword


class InvitationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invitation
        fields = '__all__'

    def get_facility_name(self, obj):
        return obj.facility.name if obj.facility else None

    def to_representation(self, obj):
        return {
            "guid": obj.guid,
            "first_name": obj.first_name,
            "last_name": obj.last_name,
            "email": obj.email,
            "phone": obj.phone,
            "user_group": GroupName[obj.user_group].value,
            "facility_name": self.get_facility_name(obj),
        }


class ResetPasswordListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResetPassword
        fields = '__all__'

    def to_representation(self, obj):
        return {
            "guid": obj.guid,
            "email": obj.email,
        }
