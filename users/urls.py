from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from users.views import (
    InvitationDetail,
    InvitationCreateUser,
    ResetPasswordDetail,
    ForgetPassword
)

urlpatterns = [
    path('invitations/register/', InvitationCreateUser.as_view()),
    path('invitations/<uuid:guid>/', InvitationDetail.as_view()),
    path('users/reset-password/', ResetPasswordDetail.as_view()),
    path('users/reset-password/<uuid:guid>/', ResetPasswordDetail.as_view()),
    path('users/forget-password/', ForgetPassword.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
