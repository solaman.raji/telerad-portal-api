import uuid

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from organization.models import Facility
from telerad_portal_api.utils import GroupName


class Invitation(models.Model):
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    first_name = models.CharField(max_length=150, null=True, blank=True)
    last_name = models.CharField(max_length=150, null=True, blank=True)
    email = models.EmailField()
    phone = models.CharField(max_length=20, blank=True, null=True)
    user_group = models.CharField(
        max_length=30,
        choices=GroupName.choices()
    )
    facility = models.ForeignKey(
        Facility,
        related_name='invitations',
        on_delete=models.CASCADE
    )
    expire_time = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        User,
        related_name='created_invitations',
        on_delete=models.CASCADE
    )
    is_deleted = models.BooleanField(default=False)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'invitations'
        verbose_name_plural = 'Invitations'

    def __str__(self):
        return self.email

    def is_valid(self):
        if not self.is_deleted and self.expire_time > timezone.now():
            return True
        else:
            return False

    def soft_delete(self):
        self.is_deleted = True
        self.deleted_at = timezone.now()
        self.save()


class ResetPassword(models.Model):
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    email = models.EmailField()
    user = models.ForeignKey(
        User,
        related_name='forget_password',
        on_delete=models.CASCADE
    )
    expire_time = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_deleted = models.BooleanField(default=False)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'forget_password'
        verbose_name_plural = 'Forget Passwords'

    def __str__(self):
        return self.email

    def is_valid(self):
        if not self.is_deleted and self.expire_time > timezone.now():
            return True
        else:
            return False

    def soft_delete(self):
        self.is_deleted = True
        self.deleted_at = timezone.now()
        self.save()
