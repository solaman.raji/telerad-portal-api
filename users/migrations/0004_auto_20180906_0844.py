# Generated by Django 2.0.7 on 2018-09-06 08:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_resetpassword'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='resetpassword',
            options={'verbose_name_plural': 'Forget Passwords'},
        ),
    ]
