import logging
from datetime import timedelta

from django.contrib.auth.models import User, Group
from django.core.mail import send_mail
from django.core.validators import validate_email
from django.template.loader import get_template
from django.utils import timezone
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from organization.models import UserProfile
from organization.serializers import UserSerializer
from telerad_portal_api import settings
from telerad_portal_api.custom_exception import DoesNotExist
from telerad_portal_api.utils import (
    get_object_by_guid,
    GroupName,
    get_group_name,
    serializer_error_mapping
)
from users.models import Invitation
from users.models import ResetPassword
from users.serializers import InvitationListSerializer, ResetPasswordListSerializer

logger = logging.getLogger(__name__)


class InvitationDetail(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, guid):
        invitation = get_object_by_guid(Invitation, guid)

        if invitation.is_valid():
            serializer = InvitationListSerializer(invitation)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            raise DoesNotExist("Invitation token is invalid.")


class InvitationCreateUser(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = {}
        request_data = request.data
        password = request_data.get('password')
        confirm_password = request_data.get('confirm_password')
        invitation_guid = request_data.get('invitation_guid')

        if password and confirm_password and not password == confirm_password:
            data["detail"] = "Passwords do not match."
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        invitation = get_object_by_guid(Invitation, invitation_guid)

        if not invitation.is_valid():
            raise DoesNotExist("Invitation token is invalid.")

        group_name = GroupName[invitation.user_group].value
        group = Group.objects.get(name=group_name)

        request_data['username'] = invitation.email
        request_data['email'] = invitation.email
        serializer = UserSerializer(data=request_data)

        try:
            if serializer.is_valid(raise_exception=True):
                serializer.save()

                user = User.objects.get(id=serializer.data['id'])
                user.groups.add(group)

                user_profile = UserProfile(
                    user=user,
                    facility=invitation.facility,
                    phone=invitation.phone
                )
                user_profile.save()

                invitation.soft_delete()

                status_code = status.HTTP_201_CREATED
                data = serializer.data
                data.pop("id")
                data["guid"] = str(user_profile.guid)
                data["user_group"] = get_group_name(user)
                data['facility'] = {
                    "guid": str(invitation.facility.guid),
                    "name": invitation.facility.name,
                    "phone": invitation.facility.phone,
                    "email": invitation.facility.email
                }
        except Exception as e:
            data = {
                "error": {
                    "message": serializer_error_mapping(serializer.errors) if serializer.errors else str(e)
                }
            }
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(data, status=status_code)


class ResetPasswordDetail(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = {}
        request_data = request.data
        password = request_data.get('password')
        confirm_password = request_data.get('confirm_password')
        reset_password_guid = request_data.get('reset_password_guid')

        if password and confirm_password and not password == confirm_password:
            data["detail"] = "Passwords do not match."
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        reset_password = get_object_by_guid(ResetPassword, reset_password_guid)

        if not reset_password.is_valid():
            raise DoesNotExist("Reset password token is invalid.")

        reset_password.user.set_password(password)
        reset_password.user.save()
        reset_password.soft_delete()

        data["message"] = "Reset password successfully done."
        return Response(data, status=status.HTTP_200_OK)

    def get(self, request, guid):
        reset_password = get_object_by_guid(ResetPassword, guid)

        if reset_password.is_valid():
            serializer = ResetPasswordListSerializer(reset_password)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            raise DoesNotExist("Reset password token is invalid.")


class ForgetPassword(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        email = request.data.get('email')

        if email:
            try:
                validate_email(email)
            except Exception:
                APIException("Invalid email address.")

            user = User.objects.filter(email=email).first()

            if user is not None:
                reset_password_obj = ResetPassword.objects.create(
                    email=email,
                    user=user,
                    expire_time=timezone.now() + timedelta(hours=1),
                )
                self.send_reset_password_email(reset_password_obj)

                data = {
                    "message": "Reset password email successfully sent."
                }
                return Response(data, status=status.HTTP_200_OK)
            else:
                raise DoesNotExist("Email does not exist.")

    @classmethod
    def send_reset_password_email(cls, reset_password):
        template_location = 'reset_password_email/reset_password.html'
        template = get_template(template_location)
        context = {
            'user': {
                'first_name': reset_password.user.first_name,
            },
            'reset_password': {
                'link': settings.RESET_PASSWORD_URL.format(
                    reset_password_token=reset_password.guid
                )
            }
        }
        html = template.render(context=context)
        print(html)
        try:
            send_mail(
                subject="Reset Password",
                message="",
                from_email="support@alemhealth.com",
                recipient_list=[reset_password.email],
                html_message=html
            )
        except Exception:
            logger.exception('Failed to send email to {reset_password_email}'.format(
                reset_password_email=reset_password.email
            ))
