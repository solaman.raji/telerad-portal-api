import uuid

from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone, dateparse
from jsonfield import JSONField

from organization.models import (
    Facility,
    User,
    Procedure,
    BodyPart
)
from study.utils import (
    GenderChoice,
    ModalityChoice,
    PriorityChoice,
    StatusChoice
)


class FacilityTag(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    facility_name = models.CharField(max_length=100, null=True, blank=True)
    facility_email = models.CharField(max_length=100, null=True, blank=True)
    user = models.ForeignKey(User, related_name='facility_tags', null=True, blank=True, on_delete=models.SET_NULL)

    @classmethod
    def get_or_create_tag(cls, user, facility_name, facility_email):
        obj, created = FacilityTag.objects.get_or_create(
            user=user,
            facility_name=facility_name,
            facility_email=facility_email,
        )
        return obj.guid

    @property
    def guid(self):
        return self.id


class Study(models.Model):
    guid = models.UUIDField(unique=True)
    study_instance_uid = models.CharField(max_length=100, null=True, blank=True)
    accession_number = models.CharField(max_length=20, null=True, blank=True)
    study_date = models.DateField(null=True, blank=True)
    study_time = models.TimeField(null=True, blank=True)
    study_description = models.TextField(null=True, blank=True)
    institution_name = models.CharField(max_length=50, null=True, blank=True)
    station_name = models.CharField(max_length=50, null=True, blank=True)
    modality = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        choices=ModalityChoice.choices()
    )
    priority = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        choices=PriorityChoice.choices(),
        default=PriorityChoice.routine.name
    )
    patient_id = models.CharField(max_length=20, null=True, blank=True)
    patient_name = models.CharField(max_length=50, null=True, blank=True)
    patient_gender = models.CharField(
        max_length=2,
        null=True,
        blank=True,
        choices=GenderChoice.choices()
    )
    patient_date_of_birth = models.CharField(max_length=10, null=True, blank=True)
    patient_year_of_birth = models.CharField(max_length=4, null=True, blank=True)
    patient_telephone_number = models.CharField(max_length=20, blank=True, null=True)
    body_part = models.ForeignKey(
        BodyPart,
        related_name='studies',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    referring_physician_name = models.CharField(max_length=50, null=True, blank=True)
    referring_physician = models.ForeignKey(
        User,
        related_name='referred_studies',
        null=True,
        blank=True,
        on_delete=models.SET_NULL)
    additional_patient_history = models.TextField(null=True, blank=True)
    questionnaire = JSONField(null=True, blank=True)
    number_of_dicom_images = models.IntegerField(null=True, blank=True)
    procedure = models.ForeignKey(
        Procedure,
        related_name='studies',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    facility = models.ForeignKey(
        Facility,
        related_name='studies',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    facility_name = models.CharField(max_length=100, null=True, blank=True)
    facility_email = models.CharField(max_length=100, null=True, blank=True)
    user = models.ForeignKey(
        User,
        related_name='studies',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    status = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        choices=StatusChoice.choices(),
        default=StatusChoice.created.name
    )
    zip_file = JSONField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'studies'
        verbose_name_plural = 'Studies'

    def save(self, *args, **kwargs):
        if self.referring_physician_name and self.referring_physician:
            raise ValidationError('Either Referring Physician name or user field should have value')

        super().save(*args, **kwargs)

    @property
    def patient_name_age_gender(self):
        _name = self.patient_name or '-'
        _age = f'{self.patient_age}/' if self.patient_age else ''
        _gender = self.patient_gender or ''
        return f'{_name} ({_age}{_gender})' if _age or _gender else f'{_name}'

    @property
    def patient_age(self):
        # Write testcase to verify this method
        if self.patient_date_of_birth:
            # FIXME: The patient_date_of_birth should be a date field
            _born = dateparse.parse_date(self.patient_date_of_birth)
            return self._calculate_age(_born)
        elif self.patient_year_of_birth:
            # FIXME: patient_year_of_birth should be a integer field
            return timezone.now().year - int(self.patient_year_of_birth)
        else:
            return None

    @staticmethod
    def _calculate_age(born):
        # Adapted from https://stackoverflow.com/questions/2217488/age-from-birthdate-in-python#9754466
        today = timezone.now().today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    @property
    def non_affiliated_facility(self):
        if self.facility is not None:
            return ()
        else:
            return self.facility_name, self.facility_email

    def __str__(self):
        return self.guid.hex
