from enum import Enum


class GenderChoice(Enum):
    M = "Male"
    F = "Female"

    @classmethod
    def choices(cls):
        return ((choice.name, choice.value) for choice in GenderChoice)


class ModalityChoice(Enum):
    CT = "CT"
    NM = "NM"
    MR = "MR"
    DS = "DS"
    DR = "DR"
    US = "US"
    OT = "OT"
    HSG = "HSG"
    CR = "CR"
    OP = "OP"
    DX = "DX"
    ECG = "ECG"
    MG = "MG"
    RF = "RF"
    XA = "XA"

    @classmethod
    def choices(cls):
        return ((choice.name, choice.value) for choice in ModalityChoice)


class PriorityChoice(Enum):
    routine = "Routine"
    urgent = "Urgent"

    @classmethod
    def choices(cls):
        return ((choice.name, choice.value) for choice in PriorityChoice)


class SLACountdown(Enum):
    ROUTINE = 8
    URGENT = 3


class StatusChoice(Enum):
    created = "Created"
    stored = "Stored"
    assigned = "Assigned"
    archived = "Archived"

    @classmethod
    def choices(cls):
        return ((choice.name, choice.value) for choice in StatusChoice)