import uuid

from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.test import TestCase
from django.utils import timezone

from organization.models import Facility, BodyPart
from study.models import Study
from study.utils import ModalityChoice


class StudyModelTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.pk = 1
        guid = uuid.uuid4()
        study_instance_uid = "1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639"
        accession_number = "AH-01-01"
        study_date = timezone.now(),
        study_time = None,
        study_description = "Knee (R)"
        institution_name = "Crestview"
        station_name = "Victoria Island",
        modality = ModalityChoice.HSG.value
        patient_id = "P-1000"
        patient_name = "Patient 1000"
        patient_gender = "M"
        patient_date_of_birth = "1911-11-11"
        patient_age = "042Y"
        patient_telephone_number = "1234567890"
        # body_part = BodyPart.objects.create(name="Body Part 1")
        referring_physician_name = "Physician 1",
        additional_patient_history = "This is additional patient history"
        cls.facility = Facility.objects.create(name="Facility 1")

        cls.data = {
            "guid": guid,
            "study_instance_uid": study_instance_uid,
            "accession_number": accession_number,
            # "study_date": study_date,
            # "study_time": study_time,
            "study_description": study_description,
            "institution_name": institution_name,
            "station_name": station_name,
            "modality": modality,
            "patient_id": patient_id,
            "patient_name": patient_name,
            "patient_gender": patient_gender,
            "patient_date_of_birth": patient_date_of_birth,
            # "patient_age": patient_age,
            "patient_telephone_number": patient_telephone_number,
            # "body_part_id": body_part.id,
            "referring_physician_name": referring_physician_name,
            "additional_patient_history": additional_patient_history,
            "facility_id": cls.facility.id
        }

    def setUp(self):
        Study.objects.create(**self.data)

    def test_fetch_study(self):
        study = Study.objects.get(pk=self.pk)
        self.assertEqual(study.id, self.pk)

    def test_unique(self):
        with self.assertRaises(IntegrityError):
            Study.objects.create(**self.data)

    def test_without_guid_integrity_error(self):
        data = {
            "facility_id": self.facility.id
        }
        with self.assertRaises(IntegrityError):
            Study.objects.create(**data)

    def test_guid_uuid_validation_error(self):
        data = {
            "guid": "123",
            "facility_id": self.facility.id
        }
        with self.assertRaises(ValidationError):
            Study.objects.create(**data)
