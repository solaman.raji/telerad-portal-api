import json

from django.test import TestCase
from rest_framework.test import RequestsClient

from organization.model_utils import create_radiographer
from organization.models import Facility
from study.models import Study


class PatientInvalidDateOfBirth(TestCase):
    fixtures = ['api-key-dumpdata.json']

    @classmethod
    def setUpTestData(cls):
        cls.guid = '44c57ed6-62ef-41a3-b85e-85f9694cca48'
        study_instance_uid = '1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639'
        accession_number = 'AH-01-01'
        study_date = '2018-02-28'
        study_time = '16:32:54'
        study_description = 'Knee (R)'
        institution_name = 'Jaslok Hospital'
        station_name = 'Victoria Island'
        modality = 'HSG'
        patient_id = 'P-1000'
        patient_name = 'Patient 1000'
        patient_gender = 'M'
        patient_date_of_birth = None
        patient_age = '042Y'
        patient_telephone_number = '1234567890'
        body_part = 'CERVICAL SPINE'
        referring_physician_name = 'Physician 1'
        additional_patient_history = 'This is additional patient history'

        cls.api_key = '43a4d71fdd136879fff5faaedcf7fbbc8d9fb91d'
        cls.facility = Facility.objects.create(name='Facility 1', guid='5a3179ab-3fb2-4422-a73a-af842b45c660')
        create_radiographer(cls.facility, 'admin')

        cls.study_dict = {
            'guid': cls.guid,
            'facility_guid': cls.facility.guid,
            'study_instance_uid': study_instance_uid,
            'accession_number': accession_number,
            'study_date': study_date,
            'study_time': study_time,
            'study_description': study_description,
            'institution_name': institution_name,
            'station_name': station_name,
            'modality': modality,
            'patient_id': patient_id,
            'patient_name': patient_name,
            'patient_gender': patient_gender,
            'patient_date_of_birth': patient_date_of_birth,
            'patient_age': patient_age,
            'patient_telephone_number': patient_telephone_number,
            'body_part': body_part,
            'referring_physician_name': referring_physician_name,
            'additional_patient_history': additional_patient_history,
        }

    def setUp(self):
        self.req_client = RequestsClient()
        self.req_client.headers.update({'Api-Key': self.api_key})

    def test_create_study_with_valid_date(self):
        self.study_dict['patient_date_of_birth'] = '2000-03-12'
        response = self.req_client.post('http://testserver/api/studies/create/', data=json.dumps(self.study_dict),
                                        headers={'Content-Type': 'application/json'})
        self.assertEqual(response.status_code, 201)

        study = Study.objects.get(guid=self.guid)
        self.assertEqual(study.patient_date_of_birth, '2000-03-12')

    def test_create_study_invalid_birth_date(self):
        self.study_dict['patient_date_of_birth'] = '1/1/1988 12:00:00 AM'
        response = self.req_client.post('http://testserver/api/studies/create/', data=json.dumps(self.study_dict),
                                        headers={'Content-Type': 'application/json'})
        self.assertEqual(response.status_code, 201)

        study = Study.objects.get(guid=self.guid)
        self.assertIsNone(study.patient_date_of_birth)
