import json

from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.test import TestCase
from rest_framework.test import RequestsClient

from organization.model_utils import create_radiographer, create_physician
from organization.models import Facility
from study.models import Study
from telerad_portal_api.utils import GroupName

study_json = '''{
  "facility_guid": "5a3179ab-3fb2-4422-a73a-af842b45c660",
  "guid": "44c57ed6-62ef-41a3-b85e-85f9694cca48",
  "study_instance_uid": "1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639",
  "accession_number": null,
  "study_date": "2018-02-28",
  "study_time": "16:32:54",
  "study_description": "Knee (R)",
  "institution_name": null,
  "station_name": null,
  "modality": "MR",
  "patient_id": "0",
  "patient_name": "Anonymized",
  "patient_gender": null,
  "patient_date_of_birth": null,
  "patient_age": "000Y",
  "patient_telephone_number": null,
  "body_part": null,
  "referring_physician_name": null,
  "additional_patient_history": null
}'''


class StudyCreate(TestCase):
    fixtures = ['api-key-dumpdata.json']

    @classmethod
    def setUpTestData(cls):
        cls.api_key = '43a4d71fdd136879fff5faaedcf7fbbc8d9fb91d'
        Group.objects.get_or_create(name=GroupName.RADIOGRAPHER.value)
        cls.facility = Facility.objects.create(name='Facility 1', guid='5a3179ab-3fb2-4422-a73a-af842b45c660')
        # cls.physician_user = create_physician(cls.facility, 'roland', 'Roland', 'Beaulieu')
        cls.radiographer = create_radiographer(cls.facility, 'roland', first_name='Roland', last_name='Beaulieu')
        create_radiographer(cls.facility, 'admin')
        cls.physician_name = 'Dr. Michael Giuffre'

    def setUp(self):
        self.req_client = RequestsClient()
        self.req_client.headers.update({'Api-Key': self.api_key})

    def test_create_study_with_basic_json(self):
        response = self.req_client.post('http://testserver/api/studies/create/', data=study_json,
                                        headers={'Content-Type': 'application/json'})
        self.assertEqual(response.status_code, 201)


class StudyUpdateReferringPhysician(TestCase):
    fixtures = ['api-key-dumpdata.json']

    @classmethod
    def setUpTestData(cls):
        cls.api_key = '43a4d71fdd136879fff5faaedcf7fbbc8d9fb91d'
        Group.objects.get_or_create(name=GroupName.RADIOGRAPHER.value)
        Group.objects.get_or_create(name=GroupName.PHYSICIAN.value)
        cls.facility = Facility.objects.create(name='Facility 1', guid='5a3179ab-3fb2-4422-a73a-af842b45c660')
        cls.physician_user = create_physician(cls.facility, 'michael', first_name='Michael', last_name='Giuffre')
        cls.radiographer = create_radiographer(cls.facility, 'roland', first_name='Roland', last_name='Beaulieu')
        create_radiographer(cls.facility, 'admin')
        cls.physician_name = 'Dr. Michael Giuffre'

    def setUp(self):
        self.study_dict = json.loads(study_json)
        self.req_client = RequestsClient()
        self.req_client.headers.update({'Api-Key': self.api_key})
        response = self.req_client.post('http://testserver/api/studies/create/', data=study_json,
                                        headers={'Content-Type': 'application/json'})
        self.study_guid = response.json()['guid']

    def test_null_referring_physician(self):
        self.study_dict['referring_physician_name'] = None

        self.client.force_login(self.radiographer)
        response = self.client.put(f'/api/studies/{self.study_guid}/',
                                   data=json.dumps(self.study_dict), content_type='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertIsNone(response.json()['referring_physician_name'])

    def test_referring_physician_user(self):
        self.study_dict['referring_physician_name'] = ""
        self.study_dict['referring_physician_guid'] = str(self.physician_user.profile.guid)

        self.client.force_login(self.radiographer)
        response = self.client.put(f'/api/studies/{self.study_guid}/',
                                   data=json.dumps(self.study_dict), content_type='application/json')

        response_json = response.json()
        print('PUT response', response_json)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(not response_json['referring_physician_name'])

        self.assertEquals(response_json['referring_physician_guid'], str(self.physician_user.profile.guid))
        self.assertEquals(Study.objects.get(guid=self.study_guid).referring_physician, self.physician_user)

    def test_referring_physician_in_get_response(self):
        self.study_dict['referring_physician_name'] = None
        self.study_dict['referring_physician_guid'] = str(self.physician_user.profile.guid)

        self.client.force_login(self.radiographer)
        response = self.client.put(f'/api/studies/{self.study_guid}/',
                                   data=json.dumps(self.study_dict), content_type='application/json')

        self.assertEqual(response.status_code, 200)

        response = self.client.get(f'/api/studies/{self.study_guid}/')
        self.assertEqual(response.status_code, 200)

        response_json = response.json()
        print('GET response', response_json)

        self.assertEquals(response_json['referring_physician_guid'], str(self.physician_user.profile.guid))
        self.assertEquals(response_json['referring_physician_name'], 'Michael Giuffre')

    def test_populate_name_guid_field_together_invalid(self):
        self.study_dict['referring_physician_name'] = self.physician_name
        self.study_dict['referring_physician_guid'] = str(self.physician_user.profile.guid)

        self.client.force_login(self.radiographer)
        with self.assertRaises(ValidationError):
            self.client.put(f'/api/studies/{self.study_guid}/',
                                       data=json.dumps(self.study_dict), content_type='application/json')

    def test_populate_user_after_name_was_set(self):
        study = Study.objects.get(guid=self.study_guid)
        study.referring_physician_name = self.physician_name
        study.save()

        self.study_dict['referring_physician_name'] = None
        self.study_dict['referring_physician_guid'] = str(self.physician_user.profile.guid)

        self.client.force_login(self.radiographer)
        response = self.client.put(f'/api/studies/{self.study_guid}/',
                                   data=json.dumps(self.study_dict), content_type='application/json')

        response_json = response.json()
        print('PUT response', response_json)
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(response_json['referring_physician_name'])
        self.assertEquals(response_json['referring_physician_guid'], str(self.physician_user.profile.guid))

        self.assertEquals(Study.objects.get(guid=self.study_guid).referring_physician_name, None)
        self.assertEquals(Study.objects.get(guid=self.study_guid).referring_physician, self.physician_user)

    def test_populate_name_after_user_was_set(self):
        study = Study.objects.get(guid=self.study_guid)
        study.referring_physician = self.physician_user
        study.save()

        self.study_dict['referring_physician_name'] = self.physician_name
        self.study_dict['referring_physician_guid'] = None

        self.client.force_login(self.radiographer)
        response = self.client.put(f'/api/studies/{self.study_guid}/',
                                   data=json.dumps(self.study_dict), content_type='application/json')

        response_json = response.json()
        print('PUT response', response_json)
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(response_json['referring_physician_guid'])
        self.assertEquals(response_json['referring_physician_name'], self.physician_name)

        self.assertIsNone(Study.objects.get(guid=self.study_guid).referring_physician)
