import uuid

from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.test import TestCase

from organization.model_utils import create_physician
from organization.models import Facility
from study.models import Study
from study.utils import ModalityChoice
from telerad_portal_api.utils import GroupName


def study_data_init():
    guid = uuid.uuid4()
    study_instance_uid = '1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.79379639'
    accession_number = 'AH-01-01'
    study_description = 'Knee (R)'
    institution_name = 'Crestview'
    station_name = 'Victoria Island',
    modality = ModalityChoice.HSG.value
    patient_id = 'P-1000'
    patient_name = 'Patient 1000'
    patient_gender = 'M'
    patient_date_of_birth = '1911-11-11'
    patient_telephone_number = '1234567890'
    referring_physician_name = 'Physician 2',
    additional_patient_history = 'This is additional patient history'

    study_data = {
        'guid': guid,
        'study_instance_uid': study_instance_uid,
        'accession_number': accession_number,
        'study_description': study_description,
        'institution_name': institution_name,
        'station_name': station_name,
        'modality': modality,
        'patient_id': patient_id,
        'patient_name': patient_name,
        'patient_gender': patient_gender,
        'patient_date_of_birth': patient_date_of_birth,
        'patient_telephone_number': patient_telephone_number,
        'referring_physician_name': referring_physician_name,
        'additional_patient_history': additional_patient_history,
    }
    return study_data


class ReferringPhysicianFieldsInStudy(TestCase):
    @classmethod
    def setUpTestData(cls):
        Group.objects.get_or_create(name=GroupName.PHYSICIAN.value)
        cls.facility = Facility.objects.create(name='Facility 1')
        cls.physician_user = create_physician(cls.facility, 'roland', 'Roland', 'Beaulieu')
        cls.physician_name = 'Dr. Michael Giuffre'

    def setUp(self):
        self.study_data = study_data_init()

    # def tearDown(self):
    #     self.study_obj.delete()

    def test_no_physician_name_saved(self):
        self.study_data['referring_physician_name'] = None
        self.study_obj = Study.objects.create(**self.study_data)
        self.assertIsNone(self.study_obj.referring_physician_name)

    def test_physician_name_is_stored(self):
        self.study_data['referring_physician_name'] = self.physician_name
        self.study_obj = Study.objects.create(**self.study_data)
        self.assertEqual(self.study_obj.referring_physician_name, self.physician_name)

    def test_physician_user_saved(self):
        self.study_data['referring_physician_name'] = None
        self.study_data['referring_physician'] = self.physician_user
        self.study_obj = Study.objects.create(**self.study_data)
        self.assertIsNone(self.study_obj.referring_physician_name)
        self.assertEqual(self.study_obj.referring_physician_id, self.physician_user.id)

    def test_name_user_value_raise_exception(self):
        self.study_data['referring_physician_name'] = self.physician_name
        self.study_data['referring_physician'] = self.physician_user
        with self.assertRaises(ValidationError):
            Study.objects.create(**self.study_data)
