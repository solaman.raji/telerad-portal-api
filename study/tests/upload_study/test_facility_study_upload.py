import boto3
from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIRequestFactory

from moto import mock_s3

from organization.models import Facility, UserProfile
from study.views import FacilityStudyUploadUrl
from telerad_portal_api.custom_exception import InvalidParameter


class FacilityStudyUploadUrlTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(username='richard')
        cls.user.set_password('qweqwe')
        cls.user.save()
        cls.facility = Facility.objects.create(name='Jaslok Hospital')
        UserProfile.objects.create(user=cls.user, facility=cls.facility).save()

    def setUp(self):
        self.factory = APIRequestFactory()

    def test_upload_path_for_facility(self):
        user_guid = 'user-guid'
        facility_guid = 'facility-guid'
        study_guid = 'study-guid'
        file_ext = 'json'
        path = FacilityStudyUploadUrl._upload_path(user_guid, facility_guid, study_guid, file_ext)
        print(path)

        self.assertEqual(path, f'{facility_guid}/{study_guid}/{study_guid}.{file_ext}')

    def test_get_facility_for_facility_users(self):
        request = self.factory.post('/study/upload/facility/',{})

        request.user = self.user
        request.data = {}

        facility_guid = FacilityStudyUploadUrl()._get_facility_guid(request)
        self.assertIsNotNone(facility_guid)
        self.assertEqual(facility_guid, self.facility.guid)

    def test_get_facility_raise_exception_on_request_body(self):
        request = self.factory.post('/study/upload/facility/', {'email': 'abcd@alemcloud.com'}, format='json')

        request.user = self.user
        request.data = {'email': 'abcd@alemcloud.com'}

        with self.assertRaises(InvalidParameter):
            FacilityStudyUploadUrl()._get_facility_guid(request)


class FacilityStudyUploadTestCase(TestCase):

    @classmethod
    @mock_s3
    def setUpTestData(cls):
        cls.user = User.objects.create(username='richard')
        cls.user.set_password('qweqwe')
        cls.user.save()
        cls.facility = Facility.objects.create(name='Jaslok Hospital')
        UserProfile.objects.create(user=cls.user, facility=cls.facility).save()
        cls.response_keys = sorted(['json_url', 'facility_guid', 'study_guid', 'zip_url'])
        s3 = boto3.resource('s3')
        s3.create_bucket(Bucket=settings.AH_CONNECT_UPSTREAM_BUCKET)

    def setUp(self):
        self.client.login(username=self.user.username, password='qweqwe')

    def tearDown(self):
        self.client.logout()

    @mock_s3
    def test_study_upload_response(self):
        response = self.client.post('/api/studies/upload/facility/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func.__name__, FacilityStudyUploadUrl.as_view().__name__)

        self.assertListEqual(self.response_keys, sorted(response.json().keys()))

    @mock_s3
    def test_facility_not_allowed_on_upload(self):
        response = self.client.post('/api/studies/upload/facility/', data='{"facility_name": "Jaslok"}',
                                    content_type='application/json')

        self.assertEqual(response.resolver_match.func.__name__, FacilityStudyUploadUrl.as_view().__name__)
        self.assertContains(response, 'Request body is not allowed for this request', count=1, status_code=400)

    @mock_s3
    def test_non_affiliated_user_cannot_upload_study_for_facility(self):
        self.user.profile.facility = None
        self.user.profile.save()

        self.assertIsNone(self.user.profile.facility)

        response = self.client.post('/api/studies/upload/facility/')
        print(response.content)

        self.assertContains(response, 'Not allowed to upload study for facility', count=1, status_code=400)
