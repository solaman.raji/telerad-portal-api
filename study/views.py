import json
import logging

import boto3
import os
from actstream import action
from botocore.client import Config
from django.contrib.auth.models import User
from django.db.models import Q
from django.utils import dateparse
from django.utils.decorators import method_decorator
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.views import APIView

from organization.decorators import check_group_validation, check_user_profile
from organization.models import Facility, Procedure, BodyPart
from organization.views import BodyPartDetail
from study.filters import StudyFilter
from study.models import Study, FacilityTag
from study.serializers import (
    StudyCreateSerializer,
    StudyListSerializer,
    StudyDetailsSerializer
)
from study.utils import ModalityChoice, StatusChoice, PriorityChoice
from telerad_portal_api import settings
from telerad_portal_api.custom_exception import InvalidParameter, UnauthorizedAccess
from telerad_portal_api.mixins import PaginationMixin
from telerad_portal_api.utils import (
    GroupName,
    ViewMethod,
    get_group_name,
    serializer_error_mapping,
    get_object_by_guid,
    get_request_query_params
)

logger = logging.getLogger(__name__)


class StudyAHConnect(APIView):
    def post(self, request):
        self.param_validation(request.data)
        logger.info(f'Create Study with {request.data}')
        facility_guid = request.data.pop('facility_guid')
        facility = None
        try:
            facility = get_object_by_guid(Facility, facility_guid)
            request.data['facility'] = facility.id
            user = User.objects.get(username="admin")
        except:
            logger.warning(f'Facility not found for guid: {facility_guid}. Check in facility Tag list')
            facility_tag = FacilityTag.objects.get(id=facility_guid)
            request.data['facility_name'] = facility_tag.facility_name
            request.data['facility_email'] = facility_tag.facility_email
            request.data['user'] = facility_tag.user.id
            user = facility_tag.user

        body_part = BodyPartDetail.get_body_part_by_name(
            BodyPartDetail(),
            request.data.pop('body_part')
        )

        if body_part:
            request.data['body_part'] = body_part.id

        # FIXME: make it a method and write unit test for this
        if request.data.get('patient_date_of_birth'):
            try:
                date_of_birth = request.data.pop('patient_date_of_birth')
                _date = dateparse.parse_date(date_of_birth) or dateparse.parse_datetime(date_of_birth)
                if _date:
                    request.data['patient_date_of_birth'] = date_of_birth
                else:
                    raise ValueError
            except ValueError:
                logger.critical(f'patient_date_of_birth is not valid: {date_of_birth}')

        serializer = StudyCreateSerializer(data=request.data)

        data = status_code = None
        try:
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                status_code = status.HTTP_201_CREATED
                data = serializer.data

                # Create Activity - Study Created
                study = Study.objects.get(guid=data["guid"])
                action.send(
                    user,
                    verb="Study Created",
                    action_object=study,
                    target=facility if facility else study,
                    description="Detail Timeline"
                )
        except Exception as ex:
            logger.error(f'Study Creation is failed for {ex}')
            logger.exception(ex)
            data = {
                "error": {
                    "message": serializer_error_mapping(serializer.errors)
                }
            }
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(data, status=status_code)

    def put(self, request, guid):
        study = get_object_by_guid(Study, guid)
        study.zip_file = json.loads(request.body)
        study.save()
        StudyDetail.update_study_status(StudyDetail(), study, StatusChoice.stored.name)

        # Create Activity - Study Stored
        user = User.objects.get(username="admin")
        study = Study.objects.get(guid=guid)
        action.send(
            user,
            verb="DICOM Images Uploaded",
            action_object=study,
            target=study.facility,
            description="Detail Timeline"
        )

        return Response(status=status.HTTP_200_OK)

    def param_validation(self, request_data):
        if 'facility_guid' not in request_data or not request_data['facility_guid']:
            raise InvalidParameter(detail="facility_guid field is required and cannot be null or empty.")


class StudyList(APIView, PaginationMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = StudyListSerializer
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS

    @check_group_validation(ViewMethod.STUDY_LIST.value)
    @method_decorator(check_user_profile)
    def get(self, request):
        data = {}
        multiple_choice_filter_list = ['modality', 'body_part', 'facility']
        group_by = request.GET.get('group_by')
        search = request.GET.get('search')
        request_user = request.user
        request_user_facility = request_user.profile.facility
        group_name = get_group_name(request_user)
        request_query_params = get_request_query_params(
            dict(request.GET),
            multiple_choice_filter_list
        )

        if request.GET.get('ordering'):
            request_query_params['ordering'] = request.GET.get('ordering')
        if request.GET.get('patient_id'):
            request_query_params['patient_id'] = request.GET.get('patient_id')
        if request.GET.get('patient_name'):
            request_query_params['patient_name'] = request.GET.get('patient_name')
        if request.GET.get('accession_number'):
            request_query_params['accession_number'] = request.GET.get('accession_number')
        if request.GET.get('priority'):
            request_query_params['priority'] = request.GET.get('priority')
        if request.GET.get('facility_name'):
            request_query_params['facility_name'] = request.GET.get('facility_name')

        if group_name == GroupName.RADIOGRAPHER.value:
            study_list = Study.objects.filter(
                facility=request_user_facility
            )
        elif group_name == GroupName.RADIOLOGIST.value:
            study_list = Study.objects.filter(
                Q(facility=request_user_facility) |
                Q(assignment__facility=request_user_facility) |
                Q(assignment__radiologist=request_user)
            )
        elif group_name == GroupName.INDEPENDENT_RADIOLOGIST.value:
            study_list = Study.objects.filter(
                user=request_user
            )

        study_list = study_list.order_by('-created_at').exclude(
            status=StatusChoice.archived.name
        )

        if search:
            study_list = study_list.filter(
                Q(patient_id__icontains=search) |
                Q(patient_name__icontains=search) |
                Q(patient_telephone_number__icontains=search)
            )
        else:
            study_list = StudyFilter(request_query_params, queryset=study_list).qs

        if group_by and group_by == "modality":
            serializer = self.serializer_class(study_list, many=True)
            data["results"] = self.get_study_list_group_by_modlaity(serializer.data)
            return Response(data, status=status.HTTP_200_OK)

        page = self.paginate_queryset(study_list)

        if page is not None:
            serializer = self.serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)

        return Response(data, status=status.HTTP_200_OK)

    def get_study_list_group_by_modlaity(self, studies):
        data = {}
        modality_choices = dict(ModalityChoice.choices()).keys()

        for modality_choice in modality_choices:
            data[modality_choice] = {
                "count": 0,
                "results": []
            }

        for study in studies:
            data[study['modality']]['results'].append(study)
            data[study['modality']]['count'] += 1

        return data


class StudyDetail(APIView):
    permission_classes = (IsAuthenticated,)

    @check_group_validation(ViewMethod.STUDY_FETCH.value)
    @method_decorator(check_user_profile)
    def get(self, request, guid):
        group_name = get_group_name(request.user)
        study = get_object_by_guid(Study, guid)
        self.study_authorization(request.user, group_name, study)
        serializer = StudyDetailsSerializer(study)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @check_group_validation(ViewMethod.STUDY_UPDATE.value)
    @method_decorator(check_user_profile)
    def put(self, request, guid):
        group_name = get_group_name(request.user)
        study = get_object_by_guid(Study, guid)
        self.study_authorization(request.user, group_name, study)

        if request.data.get('procedure_guid'):
            procedure_guid = request.data.pop('procedure_guid')
            procedure = get_object_by_guid(Procedure, procedure_guid)
            request.data['procedure'] = procedure.id

        if request.data.get('body_part_guid'):
            body_part_guid = request.data.pop('body_part_guid')
            body_part = get_object_by_guid(BodyPart, body_part_guid)
            request.data['body_part'] = body_part.id

        if request.data.get('referring_physician_guid'):
            referring_physician_guid = request.data.pop('referring_physician_guid')
            physician = get_object_by_guid(User, referring_physician_guid)
            request.data['referring_physician'] = physician.id
        else:
            request.data['referring_physician'] = None

        serializer = StudyCreateSerializer(study, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            data = serializer.data

            # Create Activity - Urgent
            if data['priority'] == PriorityChoice.urgent.name:
                user = request.user
                study = Study.objects.get(guid=data["guid"])
                action.send(
                    user,
                    verb="Urgent - {patient_name}".format(
                        patient_name=study.patient_name
                    ),
                    action_object=study,
                    target=study.facility,
                    description="Notification Timeline"
                )

            return Response(data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @check_group_validation(ViewMethod.STUDY_DELETE.value)
    @method_decorator(check_user_profile)
    def delete(self, request, guid, format=None):
        study = get_object_by_guid(Study, guid)

        # study_authorization, error, status_code = self.study_authorization(request.user, study)
        #
        # if not study_authorization:
        #     return Response(data=error, status=status_code)

        self.update_study_status(study, StatusChoice.archived.name)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def study_authorization(self, user, group_name, study):
        exception = True

        if group_name == GroupName.RADIOGRAPHER.value and user.profile.facility == study.facility:
            exception = False
        elif group_name == GroupName.RADIOLOGIST.value and user.profile.facility == study.facility:
            exception = False
        elif group_name == GroupName.RADIOLOGIST.value and hasattr(study, 'assignment') and user.profile.facility == study.assignment.facility:
            exception = False
        elif group_name == GroupName.RADIOLOGIST.value and hasattr(study, 'assignment') and user == study.assignment.radiologist:
            exception = False
        elif group_name == GroupName.INDEPENDENT_RADIOLOGIST.value and hasattr(study, 'assignment') and user == study.assignment.independent_radiologist:
            exception = False
        elif group_name == GroupName.PHYSICIAN.value and study.referring_physician_id and user == study.referring_physician:
            exception = False

        if exception:
            raise UnauthorizedAccess()

    def update_study_status(self, study, status):
        study.status = status
        study.save()


class StudyZip(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, guid):
        data = {}
        study = get_object_by_guid(Study, guid)

        s3 = boto3.client(
            's3',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            region_name=settings.AWS_REGION,
        )
        url = s3.generate_presigned_url(
            ClientMethod='get_object',
            Params={
                'Bucket': study.zip_file['bucket'],
                'Key': study.zip_file['key']
            }
        )
        data['data'] = {
            'zip_file_url': url
        }

        return Response(data, status=status.HTTP_200_OK)


class StudyUploadUrl(APIView):
    permission_classes = (IsAuthenticated,)

    def _get_or_create_facility_tag(self, user, facility_name=None, facility_email=None):
        facility_name = 'Default Facility' if not facility_name else facility_name
        facility_email = 'info@alemcloud.com' if not facility_email else facility_email
        logger.info(f'Get or Create facility Tag for user: {user} with {facility_name} and {facility_email}')
        return FacilityTag.get_or_create_tag(user, facility_name, facility_email)

    def _get_facility_guid(self, request):
        _profile = request.user.profile

        if not _profile.associated_with_facility:
            facility_name = request.data.get('facility_name')
            facility_email = request.data.get('facility_email')
            return self._get_or_create_facility_tag(request.user, facility_name, facility_email)
        else:
            _msg = f'The user {request.user} ({_profile.guid}) is associated with facility. ' \
                   f'Not allowed to upload study for another facility'
            logger.error(_msg)
            raise InvalidParameter(_msg)

    def post(self, request):
        _user_guid = request.user.profile.guid
        logger.info(f'Study Upload request to {request.path} by user {request.user.username}(request.user.email)')
        _facility_guid = self._get_facility_guid(request)

        import uuid
        _study_guid = uuid.uuid4()
        json_url = self._generate_url(self._upload_path(_user_guid, _facility_guid, _study_guid, 'json'))
        zip_url = self._generate_url(self._upload_path(_user_guid, _facility_guid, _study_guid, 'zip'))

        logger.info(f'Generate Upload URL for new study: {_study_guid} by user: {request.user} ({_user_guid})')

        data = {
            'json_url': json_url,
            'zip_url': zip_url,
            'study_guid': str(_study_guid),
            'facility_guid': str(_facility_guid)
        }

        return Response(data, status=status.HTTP_200_OK)

    @classmethod
    def _upload_path(cls, _user_guid, _facility_guid, _study_guid, file_type):
        return os.path.join(str(_user_guid), str(_study_guid), f'{str(_study_guid)}.{file_type}')

    def _generate_url(self, _upload_path):
        s3 = boto3.client(
            's3',
            config=Config(signature_version='s3v4'),
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            region_name=settings.AWS_REGION,
        )
        url = s3.generate_presigned_url(ClientMethod='put_object',
                                        Params={'Bucket': settings.AH_CONNECT_UPSTREAM_BUCKET, 'Key': _upload_path},
                                        ExpiresIn=300, HttpMethod='PUT')
        return url


class FacilityStudyUploadUrl(StudyUploadUrl):

    def _get_facility_guid(self, request):
        _profile = request.user.profile

        if request.data:
            raise InvalidParameter(f'Request body is not allowed for this request: {request.data}')

        if _profile.associated_with_facility:
            return _profile.facility.guid
        else:
            _msg = f'The user {request.user} ({_profile.guid}) is not associated with facility. ' \
                   f'Not allowed to upload study for facility'
            logger.error(_msg)
            raise InvalidParameter(_msg)

    @classmethod
    def _upload_path(cls, _user_guid, _facility_guid, _study_guid, file_type):
        return os.path.join(str(_facility_guid), str(_study_guid), f'{str(_study_guid)}.{file_type}')


class StudyTimelineList(APIView):
    permission_classes = (IsAuthenticated,)

    @check_group_validation(ViewMethod.STUDY_TIMELINE_LIST.value)
    @method_decorator(check_user_profile)
    def get(self, request, guid):
        data = []
        study = get_object_by_guid(Study, guid)
        activities = study.action_object_actions.filter(
            description="Detail Timeline"
        ).order_by('-timestamp')

        for activity in activities:
            activity_data = {
                "verb": activity.verb,
                "created_at": activity.timestamp.isoformat()
            }
            data.append(activity_data)

        return Response(data, status=status.HTTP_200_OK)
