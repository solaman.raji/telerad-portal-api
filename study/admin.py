from django.contrib import admin

from study.models import Study, FacilityTag


class StudyAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'guid',
        'patient_name',
        'modality',
        'priority',
        'body_part',
        'facility',
        'facility_name',
        'facility_email',
        'user',
        'status'
    )


class FacilityInfoAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'user',
        'facility_name',
        'facility_email',
    )


admin.site.register(Study, StudyAdmin)
admin.site.register(FacilityTag, FacilityInfoAdmin)
