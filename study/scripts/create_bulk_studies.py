import uuid

import random
from django.contrib.auth.models import User
from faker import Faker

from organization.models import Facility, BodyPart, UserProfile
from study.models import Study
from study.utils import ModalityChoice, GenderChoice


def _generate_study_instance_uid():
    random_number = str(random.random())[2:]
    return f'1.2.826.0.1.3680043.8.1055.1.20111103111148288.98361414.{random_number}'


def _get_physicians(facility):
    physician_query = UserProfile.objects.filter(
        facility=facility).filter(
        user__in=User.objects.filter(
            groups__name='Physician'))
    return [profile.user for profile in physician_query]


def create_bulk_studies():
    facility_list = Facility.objects.filter(name__startswith='Facility').all()
    body_part_list = BodyPart.objects.all()
    modality_list = dict(ModalityChoice.choices()).keys()
    fake = Faker()

    for facility in facility_list:
        counter = 1
        physicians = _get_physicians(facility)
        for modality in modality_list:
            for body_part in body_part_list:
                study_dict = dict(
                    guid=uuid.uuid4(),
                    accession_number=fake.credit_card_number(card_type=None),
                    facility=facility,
                    modality=modality,
                    patient_name="{facility_name} Patient {counter}".format(
                        facility_name=facility.name,
                        counter=counter
                    ),
                    patient_gender=GenderChoice.M.name if fake.boolean() else GenderChoice.F.name,
                    patient_date_of_birth=fake.date(pattern="%Y-%m-%d", end_datetime=None),
                    patient_telephone_number=fake.phone_number(),
                    body_part=body_part,
                    study_instance_uid=_generate_study_instance_uid(),
                    study_date=fake.date(pattern="%Y-%m-%d", end_datetime=None),
                    study_time=fake.time(pattern="%H:%M:%S", end_datetime=None),
                    study_description=fake.text(),
                    additional_patient_history=fake.text(),
                    number_of_dicom_images=random.randint(1, 1000),
                    institution_name=fake.company(),
                )
                rand = random.randint(0, 5)
                if rand == 1 or rand == 0:
                    study_dict['referring_physician'] = physicians[rand]
                else:
                    study_dict['referring_physician_name'] = fake.name()

                Study.objects.create(
                    **study_dict
                )
                counter += 1


def run():
    create_bulk_studies()
