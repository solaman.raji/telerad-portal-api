#!/bin/bash

# declare a variable as an indexed array
declare -a S3_KEYS

# S3 keys of sample studies
S3_KEYS=(
9d4145d8-ab8c-46f2-886d-d2a175a0d7ef/1.3.6.1.4.1.5962.99.1.2210544426.361133416.1535513901866.31.0/1.3.6.1.4.1.5962.99.1.2210544426.361133416.1535513901866.31.0.json
9d4145d8-ab8c-46f2-886d-d2a175a0d7ef/1.3.6.1.4.1.5962.99.1.2210544426.361133416.1535513901866.31.0/1.3.6.1.4.1.5962.99.1.2210544426.361133416.1535513901866.31.0.zip
9d4145d8-ab8c-46f2-886d-d2a175a0d7ef/1.3.6.1.4.1.5962.99.1.2210544426.361133416.1535513901866.4.0/1.3.6.1.4.1.5962.99.1.2210544426.361133416.1535513901866.4.0.json
9d4145d8-ab8c-46f2-886d-d2a175a0d7ef/1.3.6.1.4.1.5962.99.1.2210544426.361133416.1535513901866.4.0/1.3.6.1.4.1.5962.99.1.2210544426.361133416.1535513901866.4.0.zip
)


echo "Details of S3 keys"
declare -p S3_KEYS

SRC_BUCKET=ah-connect-upstream-staging-backup


upload_studies ()
{
    if [ -z $1 ]
    then
        echo destination bucket is not passed as an argument
        return
    fi

    local DST_BUCKET=$1
    echo "S3 objects will be copied from $SRC_BUCKET to destination bucket $DST_BUCKET"

    for KEY in ${S3_KEYS[*]}
    do
      echo Restore $KEY
      local _COMMAND="aws s3 cp s3://$SRC_BUCKET/$KEY s3://$DST_BUCKET/$KEY"
      echo Copy command: $_COMMAND
      ${_COMMAND}
    done
}

upload_studies $1