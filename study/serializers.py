from collections import defaultdict

from rest_framework import serializers

from study.models import Study
from telerad_portal_api import settings


class StudyCreateSerializer(serializers.ModelSerializer):
    questionnaire = serializers.JSONField(allow_null=True, required=False)

    class Meta:
        model = Study
        fields = '__all__'

    def create(self, validated_data):
        return Study.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.accession_number = validated_data.get("accession_number", instance.accession_number)
        instance.study_description = validated_data.get("study_description", instance.study_description)
        instance.modality = validated_data.get("modality", instance.modality)
        instance.priority = validated_data.get("priority", instance.priority)
        instance.patient_id = validated_data.get("patient_id", instance.patient_id)
        instance.patient_name = validated_data.get("patient_name", instance.patient_name)
        instance.patient_gender = validated_data.get("patient_gender", instance.patient_gender)
        instance.patient_date_of_birth = validated_data.get("patient_date_of_birth", instance.patient_date_of_birth)
        instance.patient_telephone_number = validated_data.get("patient_telephone_number", instance.patient_telephone_number)
        instance.body_part = validated_data.get("body_part", instance.body_part)
        instance.referring_physician_name = validated_data.get("referring_physician_name", instance.referring_physician_name)
        instance.referring_physician = validated_data.get("referring_physician", instance.referring_physician)
        instance.additional_patient_history = validated_data.get("additional_patient_history", instance.additional_patient_history)
        instance.questionnaire = validated_data.get("questionnaire", instance.questionnaire)
        instance.procedure = validated_data.get("procedure", instance.procedure)
        instance.save()
        return instance

    def get_facility_name(self, obj):
        return obj.facility.name if obj.facility else obj.facility_name

    def get_procedure_guid(self, obj):
        if hasattr(obj, "procedure") and obj.procedure:
            return obj.procedure.guid
        else:
            return None

    @classmethod
    def get_status(cls, obj):
        data = {
            "value": obj.status,
            "label": obj.get_status_display(),
        }
        return data

    @classmethod
    def get_body_part_guid(cls, obj):
        return obj.body_part.guid if obj.body_part else None

    @classmethod
    def get_referring_physician_guid(cls, obj):
        return obj.referring_physician.profile.guid if obj.referring_physician_id else None

    def to_representation(self, obj):
        return {
            "guid": obj.guid,
            "study_instance_uid": obj.study_instance_uid,
            "accession_number": obj.accession_number,
            "study_date": obj.study_date,
            "study_time": obj.study_time,
            "study_description": obj.study_description,
            "modality": obj.modality,
            "priority": obj.priority,
            "patient_id": obj.patient_id,
            "patient_name": obj.patient_name,
            "patient_age": None,
            "patient_gender": obj.patient_gender,
            "patient_date_of_birth": obj.patient_date_of_birth,
            "patient_telephone_number": obj.patient_telephone_number,
            "body_part_guid": self.get_body_part_guid(obj),
            "referring_physician_name": obj.referring_physician_name,
            "referring_physician_guid": self.get_referring_physician_guid(obj),
            "additional_patient_history": obj.additional_patient_history,
            "questionnaire": obj.questionnaire,
            "created_at": obj.created_at.isoformat(),
            "facility_name": self.get_facility_name(obj),
            "procedure_guid": self.get_procedure_guid(obj),
            "status": self.get_status(obj),
        }


class StudyListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Study
        fields = '__all__'

    @classmethod
    def get_facility_name(cls, obj):
        return obj.facility.name if obj.facility else obj.facility_name

    @classmethod
    def get_status(cls, obj):
        data = {
            "value": obj.status,
            "label": obj.get_status_display(),
        }
        return data

    @classmethod
    def global_status(cls, obj):
        if hasattr(obj, "assignment"):
            return obj.assignment.reporting_status.value
        else:
            return obj.get_status_display()

    @classmethod
    def get_body_part_name(cls, obj):
        return obj.body_part.name if obj.body_part else None

    @classmethod
    def get_zip_file(cls, obj):
        return True if obj.zip_file else False

    @classmethod
    def get_viewer_url(cls, obj):
        return settings.VIEWER_URL.format(obj.study_instance_uid) if obj.study_instance_uid else None

    @classmethod
    def get_assignment_status(cls, obj):
        data = {
            "value": obj.assignment.status,
            "label": obj.assignment.get_status_display(),
        }
        return data

    def get_report_guid(self, obj):
        if hasattr(obj.assignment, "report") and obj.assignment.report:
            return obj.assignment.report.id
        else:
            return None

    def get_assignment(self, obj):
        data = None

        if hasattr(obj, "assignment"):
            data = defaultdict(dict)
            data["guid"] = obj.assignment.guid
            data["status"] = self.get_assignment_status(obj)
            data['report_guid'] = self.get_report_guid(obj)

        return data

    def to_representation(self, obj):
        return {
            "guid": obj.guid,
            "accession_number": obj.accession_number,
            "modality": obj.get_modality_display(),
            "priority": obj.get_priority_display(),
            "patient_id": obj.patient_id,
            "patient_name": obj.patient_name,
            "patient_gender": obj.patient_gender,
            "patient_age": None,
            "body_part": self.get_body_part_name(obj),
            "number_of_dicom_images": obj.number_of_dicom_images,
            "facility_name": self.get_facility_name(obj),
            "status": self.get_status(obj),
            "created_at": obj.created_at.isoformat(),
            "zip_file": self.get_zip_file(obj),
            "viewer_url": self.get_viewer_url(obj),
            "assignment": self.get_assignment(obj),
            "global_status": self.global_status(obj),
        }


class StudyDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Study
        fields = '__all__'

    def get_facility_name(self, obj):
        return obj.facility.name if obj.facility else obj.facility_name

    def get_facility(self, obj):
        if hasattr(obj.assignment, "facility") and obj.assignment.facility:
            data = {
                "guid": obj.assignment.facility.guid,
                "name": obj.assignment.facility.name
            }
            return data
        else:
            return None

    def get_procedure_guid(self, obj):
        if hasattr(obj, "procedure") and obj.procedure:
            return obj.procedure.guid
        else:
            return None

    @classmethod
    def get_status(cls, obj):
        data = {
            "value": obj.status,
            "label": obj.get_status_display(),
        }
        return data

    @classmethod
    def get_assignment_status(cls, obj):
        data = {
            "value": obj.assignment.status,
            "label": obj.assignment.get_status_display(),
        }
        return data

    def get_radiologist(self, obj):
        if hasattr(obj.assignment, "radiologist") and obj.assignment.radiologist:
            radiologist = obj.assignment.radiologist
            data = {
                "guid": radiologist.profile.guid,
                "name": radiologist.first_name + " " + radiologist.last_name
            }
            return data
        else:
            return None

    def get_independent_radiologist(self, obj):
        if hasattr(obj.assignment, "independent_radiologist") and obj.assignment.independent_radiologist:
            independent_radiologist = obj.assignment.independent_radiologist
            data = {
                "guid": independent_radiologist.profile.guid,
                "name": independent_radiologist.first_name + " " + independent_radiologist.last_name
            }
            return data
        else:
            return None

    def get_report_guid(self, obj):
        if hasattr(obj.assignment, "report") and obj.assignment.report:
            return obj.assignment.report.id
        else:
            return None

    def get_assignment(self, obj):
        data = None

        if hasattr(obj, "assignment"):
            data = defaultdict(dict)
            data["guid"] = obj.assignment.guid
            data["status"] = self.get_assignment_status(obj)
            data['facility'] = self.get_facility(obj)
            data['radiologist'] = self.get_radiologist(obj)
            data['independent_radiologist'] = self.get_independent_radiologist(obj)
            data['report_guid'] = self.get_report_guid(obj)

        return data

    def get_user(self, obj):
        data = None

        if hasattr(obj, "user") and hasattr(obj.user, "profile"):
            data = defaultdict(dict)
            data["guid"] = obj.user.profile.guid

        return data

    @classmethod
    def get_body_part_guid(cls, obj):
        return obj.body_part.guid if obj.body_part else None

    # FIXME: Duplicate code with another serializer?
    @classmethod
    def get_referring_physician_guid(cls, obj):
        return obj.referring_physician.profile.guid if obj.referring_physician_id else None

    @classmethod
    def get_referring_physician_name(cls, obj):
        if obj.referring_physician_id:
            return obj.referring_physician.get_full_name()
        else:
            return obj.referring_physician_name

    @classmethod
    def get_zip_file(cls, obj):
        return True if obj.zip_file else False

    @classmethod
    def get_viewer_url(cls, obj):
        return settings.VIEWER_URL.format(obj.study_instance_uid) if obj.study_instance_uid else None

    def to_representation(self, obj):
        return {
            "guid": obj.guid,
            "study_instance_uid": obj.study_instance_uid,
            "accession_number": obj.accession_number,
            "study_date": obj.study_date,
            "study_time": obj.study_time,
            "study_description": obj.study_description,
            "modality": obj.modality,
            "priority": obj.priority,
            "patient_id": obj.patient_id,
            "patient_name": obj.patient_name,
            "patient_age": None,
            "patient_gender": obj.patient_gender,
            "patient_date_of_birth": obj.patient_date_of_birth,
            "patient_telephone_number": obj.patient_telephone_number,
            "body_part_guid": self.get_body_part_guid(obj),
            "referring_physician_name": self.get_referring_physician_name(obj),
            "referring_physician_guid": self.get_referring_physician_guid(obj),
            "additional_patient_history": obj.additional_patient_history,
            "questionnaire": obj.questionnaire,
            "created_at": obj.created_at.isoformat(),
            "zip_file": self.get_zip_file(obj),
            "viewer_url": self.get_viewer_url(obj),
            "facility_name": self.get_facility_name(obj),
            "procedure_guid": self.get_procedure_guid(obj),
            "user": self.get_user(obj),
            "status": self.get_status(obj),
            "assignment": self.get_assignment(obj),
        }
