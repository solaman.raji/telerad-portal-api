from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from study.views import (
    StudyAHConnect,
    StudyList,
    StudyDetail,
    StudyZip,
    StudyUploadUrl,
    FacilityStudyUploadUrl,
    StudyTimelineList
)

urlpatterns = [
    path('studies/create/', StudyAHConnect.as_view()),
    path('studies/', StudyList.as_view()),
    path('studies/upload/', StudyUploadUrl.as_view()),
    path('studies/upload/facility/', FacilityStudyUploadUrl.as_view()),
    path('studies/<guid>/', StudyDetail.as_view(), name='study-details'),
    path('studies/<guid>/store/', StudyAHConnect.as_view()),
    path('studies/<guid>/zip/', StudyZip.as_view()),
    path('studies/<guid>/timeline/', StudyTimelineList.as_view()),
    path('my-uploads/', StudyList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
