from django.apps import AppConfig


class StudyConfig(AppConfig):
    name = 'study'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('Study'))
