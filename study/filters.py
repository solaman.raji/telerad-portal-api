import django_filters

from organization.models import BodyPart, Facility
from study.models import Study
from study.utils import ModalityChoice, PriorityChoice


class StudyFilter(django_filters.FilterSet):
    ordering = django_filters.OrderingFilter(
        fields=(
            ('created_at', 'created_at'),
        ),
    )
    modality = django_filters.MultipleChoiceFilter(
        choices=tuple(ModalityChoice.choices())
    )
    patient_id = django_filters.CharFilter(lookup_expr='icontains')
    patient_name = django_filters.CharFilter(lookup_expr='icontains')
    accession_number = django_filters.CharFilter(lookup_expr='icontains')
    body_part = django_filters.ModelMultipleChoiceFilter(
        name='body_part__guid',
        to_field_name='guid',
        queryset=BodyPart.objects.all(),
    )
    priority = django_filters.ChoiceFilter(
        choices=tuple(PriorityChoice.choices())
    )
    facility = django_filters.ModelMultipleChoiceFilter(
        name='facility__guid',
        to_field_name='guid',
        queryset=Facility.objects.all(),
    )
    facility_name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Study
        fields = []
