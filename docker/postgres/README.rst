PostGres Docker image
=======================


Build an image from the Dockerfile assign it a name.

::
    $ docker build -t telerad-postgresql .

Run the PostgreSQL server container (in the foreground):

::
    $ docker run --rm -P --name pg_test telerad-postgresql

Please check the following document for details

.. https://docs.docker.com/engine/examples/postgresql_service/