DJango Docker
===============

Build the image

::
    docker build -t telerad-portal-django .

Run the image

::
    docker run
