from django.db.models import Q
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from organization.decorators import check_group_validation
from report_template.models import ReportTemplate
from report_template.serializers import ReportTemplateListSerializer
from telerad_portal_api.custom_exception import UnauthorizedAccess
from telerad_portal_api.utils import ViewMethod, get_object_by_guid


class ReportTemplateList(APIView):
    permission_classes = (IsAuthenticated,)

    @check_group_validation(ViewMethod.REPORT_TEMPLATE_FETCH.value)
    def get(self, request):
        report_templates = self.get_report_template_list_for_select_menu(request.user)
        return Response(report_templates, status=status.HTTP_200_OK)

    def get_report_template_list_for_select_menu(self, user):
        global_templates = ReportTemplate.objects.filter(Q(facility=None) & Q(user=None))
        report_template_list = ReportTemplate.objects.all()

        if hasattr(user, "profile") and user.profile:
            if hasattr(user.profile, "facility") and user.profile.facility:
                report_template_list = report_template_list.filter(Q(facility=user.profile.facility) | Q(user=user))
            else:
                report_template_list = report_template_list.filter(Q(user=user))

        report_template_list = global_templates.union(report_template_list)
        serializer = ReportTemplateListSerializer(report_template_list, many=True)
        return {str(obj['guid']): obj['template_name'] for obj in serializer.data}


class ReportTemplateDetail(APIView):
    permission_classes = (IsAuthenticated,)

    @check_group_validation(ViewMethod.REPORT_TEMPLATE_FETCH.value)
    def get(self, request, guid):
        report_template = get_object_by_guid(ReportTemplate, guid)
        self.report_template_authorization(request.user, report_template)
        serializer = ReportTemplateListSerializer(report_template)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def report_template_authorization(self, user, report_template):
        report_template_list = ReportTemplateList.get_report_template_list_for_select_menu(
            ReportTemplateList(),
            user
        )

        if str(report_template.guid) not in report_template_list.keys():
            raise UnauthorizedAccess()
