from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from report_template.views import (
    ReportTemplateDetail,
    ReportTemplateList)

urlpatterns = [
    path('report-templates/', ReportTemplateList.as_view()),
    path('report-templates/<guid>/', ReportTemplateDetail.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
