from django.contrib import admin

from report_template.models import ReportTemplate


class ReportTemplateAdmin(admin.ModelAdmin):
    list_display = ('id', 'guid', 'template_name', 'facility', 'user')

admin.site.register(ReportTemplate, ReportTemplateAdmin)
