from django.contrib.auth.models import User

from organization.models import Facility
from report_template.models import ReportTemplate
from telerad_portal_api.utils import GroupName


def create_bulk_report_templates():
    counter = 1
    facility_list = Facility.objects.all()
    radiologist_list = User.objects.filter(
        groups__name=GroupName.RADIOLOGIST.value
    )
    independent_radiologist_list = User.objects.filter(
        groups__name=GroupName.INDEPENDENT_RADIOLOGIST.value
    )

    for facility in facility_list:
        ReportTemplate.objects.create(
            template_name="Template {counter} for {facility_name}".format(
                counter=counter,
                facility_name=facility.name
            ),
            procedure="Procedure {counter} for {facility_name}".format(
                counter=counter,
                facility_name=facility.name
            ),
            clinical_information="Clinical Information {counter} for {facility_name}".format(
                counter=counter,
                facility_name=facility.name
            ),
            comparison="Comparison {counter} for {facility_name}".format(
                counter=counter,
                facility_name=facility.name
            ),
            findings="Findings {counter} for {facility_name}".format(
                counter=counter,
                facility_name=facility.name
            ),
            impression="Impression {counter} for {facility_name}".format(
                counter=counter,
                facility_name=facility.name
            ),
            facility=facility
        )
        counter += 1

    for radiologist in radiologist_list:
        name = "{first_name} {last_name}".format(
            first_name=radiologist.first_name,
            last_name=radiologist.last_name
        )
        ReportTemplate.objects.create(
            template_name="Template {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            procedure="Procedure {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            clinical_information="Clinical Information {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            comparison="Comparison {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            findings="Findings {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            impression="Impression {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            technique="Technique {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            user=radiologist
        )
        counter += 1

    for independent_radiologist in independent_radiologist_list:
        name = "{first_name} {last_name}".format(
            first_name=independent_radiologist.first_name,
            last_name=independent_radiologist.last_name
        )
        ReportTemplate.objects.create(
            template_name="Template {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            procedure="Procedure {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            clinical_information="Clinical Information {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            comparison="Comparison {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            findings="Findings {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            impression="Impression {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            technique="Technique {counter} for {name}".format(
                counter=counter,
                name=name
            ),
            user=independent_radiologist
        )
        counter += 1


def run():
    create_bulk_report_templates()
