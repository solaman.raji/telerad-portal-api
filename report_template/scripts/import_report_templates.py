import csv

import os

from organization.models import Facility
from report_template.models import ReportTemplate

TEMPLATE_FILEPATH = 'report_template/scripts/templates.csv'


def _import_bulk_report_templates(facility=None):
    for row in _read_report_templates(TEMPLATE_FILEPATH):
        print('Template guid {}'.format(row['guid']))
        del row['guid']
        _import_template(row, facility)


def _read_report_templates(filepath):
    with open(filepath) as fp:
        reader = csv.DictReader(fp)
        for row in reader:
            yield row


def _import_template(template, facility):
    ReportTemplate.objects.create(facility=facility, **template)
    print('Report template "{}" created'.format(template['template_name']))


def run(*args):
    facility = None
    if len(args):
        print('Args: {}'.format(args))
        facility_guid = args[0]
        facility = Facility.objects.get(guid=facility_guid)
        print('Add templates for facility {}'.format(facility.name))

    print('Template csv file path: {}'.format(os.path.exists(TEMPLATE_FILEPATH)))
    _import_bulk_report_templates(facility)
