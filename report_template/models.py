import uuid

from django.contrib.auth.models import User
from django.db import models
from tinymce.models import HTMLField

from organization.models import Facility


class ReportTemplate(models.Model):
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    template_name = models.CharField(max_length=200)
    procedure = models.CharField(max_length=100, null=True, blank=True)
    clinical_information = HTMLField(null=True, blank=True)
    comparison = HTMLField(null=True, blank=True)
    findings = HTMLField(null=True, blank=True)
    impression = HTMLField(null=True, blank=True)
    technique = HTMLField(null=True, blank=True)
    facility = models.ForeignKey(
        Facility,
        related_name='report_templates',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    user = models.ForeignKey(
        User,
        related_name='report_templates',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'report_templates'
        verbose_name_plural = 'Report Templates'

    def __str__(self):
        return self.template_name
