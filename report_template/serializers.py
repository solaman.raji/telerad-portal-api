from rest_framework import serializers

from report_template.models import ReportTemplate


class ReportTemplateListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportTemplate
        fields = '__all__'

    def to_representation(self, obj):
        return {
            "guid": obj.guid,
            "template_name": obj.template_name,
            "procedure": obj.procedure,
            "clinical_information": obj.clinical_information,
            "comparison": obj.comparison,
            "findings": obj.findings,
            "impression": obj.impression,
            "technique": obj.technique
        }
