from django.apps import AppConfig


class ReportTemplateConfig(AppConfig):
    name = 'report_template'
