==========================
Telerad Portal API Usage
==========================

Details api usage is documented in a `postman`_ doc


Set environment variable
-------------------------

An environment variable should be set to navigate the api for different cases
::

    export BASE_DOMAIN=http://localhost:8000
    export BASE_DOMAIN=https://dev-telerad-portal-api.alem.health

--------------------
Authenticate a user
--------------------

Get JWT token
-------------
::

    curl -v -H "Content-Type:application/json" -d '{"username": "rl-sevenhills", "password":"qweqwe"}' $BASE_DOMAIN/api/api-token-auth/

    # Copy the returned token string
    TOKEN_STR=eyJ0...
    JWT_TOKEN="JWT $TOKEN_STR"

Verify the token
-----------------
::

    json_data={\"token\":\"$TOKEN_STR\"}
    curl -v -H "Content-Type:application/json" -d $json_data $BASE_DOMAIN/api/api-token-verify/
    echo '{"token": "$TOKEN_STR"}' | curl -v -H "Content-Type:application/json"  $BASE_DOMAIN/api/api-token-verify/

    curl -v -H "Authorization:$JWT_TOKEN" $BASE_DOMAIN/api/configurations/
    curl -v -H "Authorization:$JWT_TOKEN" $BASE_DOMAIN/api/configurations/ | python -m json.tool



-----------
Study API
-----------

Check the `postman`_ doc for details

--------------
Assignment API
--------------

Check the `postman`_ doc for details

-----------
Report API
-----------

Create a report

::

    assignment_guid=48929277-106e-4e0a-bb9b-f51bfec89c52
    curl -v -H "Authorization:$JWT_TOKEN" -H "Content-Type:application/json" -d '{"procedure": "procedure 1",
        "findings": "findings 1", "impression": "impression 1", "comparison": "comparison 1",
        "template_name": "test template", "assignment_guid": "ac765ca1-9db6-4963-9ce7-2fd15f9b1672"
        }' $BASE_DOMAIN/api/reports/


Get a report

::

    curl -v -H "Authorization:$JWT_TOKEN" $BASE_DOMAIN/api/reports/86207af2-acb1-4d0b-9402-30232d278c14


    curl -v -X POST -H "Authorization:$JWT_TOKEN" $BASE_DOMAIN/api/reports/86207af2-acb1-4d0b-9402-30232d278c14/sign/



.. _`postman`: https://documenter.getpostman.com/view/324290/RW86KpX9