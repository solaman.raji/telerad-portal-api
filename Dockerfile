FROM lambci/lambda:build-python3.6

ENV AWS_DEFAULT_REGION ap-southeast-1

RUN pip install pip==9.0.3

RUN yum install -y sqlite-devel

WORKDIR /var/task

ADD requirements.txt /tmp/

# Fancy prompt to remind you are in zappashell
RUN echo 'export PS1="\[\e[36m\]zappashell>\[\e[m\] "' >> /root/.bashrc

RUN virtualenv /var/venv && \
    source /var/venv/bin/activate && \
    pip install pip==9.0.3 && \
    deactivate

RUN source /var/venv/bin/activate && pip install -r /tmp/requirements.txt

# COPY bashrc /root/.bashrc
RUN echo 'source /var/venv/bin/activate' >> /root/.bashrc

CMD ["bash"]


# Build a new docker image
# docker build -t telerad-zappa .

# Deploy from new docker image
# docker run  -v $(pwd):/var/task -v ~/.aws:/root/.aws  --rm telerad-zappa  bash -c ". /var/venv/bin/activate && zappa update dev"

# check pip version in docker
# docker run  -v $(pwd):/var/task -v ~/.aws:/root/.aws -it --rm telerad-zappa   bash -c "pip -V"

# Run DB migration
# docker run  -v $(pwd):/var/task -v ~/.aws:/root/.aws -it --rm telerad-zappa  bash -c ". /var/venv/bin/activate && python manage.py makemigrations && python manage.py migrate"
# docker run  -v $(pwd):/var/task -v ~/.aws:/root/.aws -it --rm telerad-zappa  bash -c ". /var/venv/bin/activate && python manage.py runscript initial_data"

# Create superuser (not needed if run initial_data script)
# docker run  -v $(pwd):/var/task -v ~/.aws:/root/.aws -it --rm telerad-zappa  bash -c ". /var/venv/bin/activate && python manage.py create_admin_user"

# Run initial script
# docker run  -v $(pwd):/var/task -v ~/.aws:/root/.aws -it --rm telerad-zappa  bash -c ". /var/venv/bin/activate && python manage.py runscript initial_data"

# Run the django server
# docker run  -v $(pwd):/var/task -v ~/.aws:/root/.aws -it -p 8000:8000  --rm telerad-zappa  bash -c ". /var/venv/bin/activate && python manage.py runserver -v 2 --traceback 0:8000"

# Add alias to access docker easily
# alias telerad-shell='docker run -it -v $(pwd):/var/task -v ~/.aws/:/root/.aws  --rm telerad-zappa'
# alias telerad-shell >> ~/.bash_profile

# SQLIte3 issue (For the Curious Joe)

# import sysconfig; print(sysconfig.get_config_var('EXT_SUFFIX'))
# https://stackoverflow.com/questions/44281547/they-could-not-be-imported-sqlite3-python3-6-centos6
# https://www.reddit.com/r/Python/comments/3docsa/how_do_i_upgrade_the_sqlite_lib_in_python34/

# docker run -it --rm telerad-zappa   python -c "import sysconfig; print(sysconfig.get_config_var('EXT_SUFFIX'))"
# _sqlite3.so file should be copied to python 3.6 directory with the suffix returned by the above command.
# cp -v /usr/lib64/python3.4/lib-dynload/_sqlite3.cpython-34m.so  /var/lang/lib/python3.6/lib-dynload/_sqlite3.cpython-36m-x86_64-linux-gnu.so
# Copy to virtualenv (Not essential)
# cp -v /usr/lib64/python3.4/lib-dynload/_sqlite3.cpython-34m.so  /var/venv/lib/python3.6/lib-dynload/_sqlite3.cpython-36m-x86_64-linux-gnu.so


# A recommended solution by zappa guide
# https://edgarroman.github.io/zappa-django-guide/walk_database/#sqlite-issues-with-python-3