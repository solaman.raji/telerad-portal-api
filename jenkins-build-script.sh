#!/bin/bash -ex

# Needed variables TARGET, ACTION, $BUILD_TAG

echo "Build Telerad portal api on $TARGET environment. Execute action $ACTION"

echo "User group info"
id -nG

echo "docker info"
docker info

DOCKER_IMAGE=portal-zappa-$BUILD_TAG

echo "Build docker image"
docker build -t $DOCKER_IMAGE . -f Dockerfile.jenkins


function upload_sample_studies {
  DST_BUCKET=ah-connect-upstream-$TARGET
  . study/scripts/upload-sample-studies.sh $DST_BUCKET
}


if [ "$ACTION" != "undeploy" ]; then
    echo "Run $ACTION on $TARGET "
    docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa $ACTION $TARGET"

    echo "Migrate Database"
    docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa manage $TARGET migrate"
    docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa manage $TARGET 'runscript initial_data'"
fi

if [ "$ACTION" == "deploy" ]; then
    echo "Run Initial data script on new deployment on $TARGET"
    docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa certify $TARGET -y"
    docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa manage $TARGET 'loaddata api-key-dumpdata.json'"
    docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa manage $TARGET 'runscript import_procedure'"
    docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa manage $TARGET 'runscript import_body_part'"
    docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa manage $TARGET 'runscript import_report_templates'"

    if [ "$TARGET" == "dev" ]; then
        docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa manage $TARGET 'runscript populate_facility_users'"
        docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa manage $TARGET 'runscript create_bulk_studies'"
        docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa manage $TARGET 'runscript create_bulk_assignments'"
    fi

    docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa manage $TARGET 'runscript import_organization_users'"

    upload_sample_studies

elif [ "$ACTION" == "undeploy" ]; then
    echo "Undeploy the $TARGET environment"
    docker run -e ENV_TYPE=$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && zappa undeploy $TARGET -y"
else
    echo "Collect static files"
    docker run -e ENV_TYPE=$TARGET -e SECRET_KEY=dummy-secret-key-$TARGET -v $(pwd):/var/task -v ~/.aws:/root/.aws --rm $DOCKER_IMAGE  bash -c "source /var/venv/bin/activate && python manage.py collectstatic --noinput"
fi

echo "Zappa deployment is done for $ACTION on $TARGET environment"

echo "Removing docker image"
docker rmi $DOCKER_IMAGE
