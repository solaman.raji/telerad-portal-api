from enum import Enum

from django.core.exceptions import ValidationError

from telerad_portal_api.custom_exception import DoesNotExist, InvalidUUID


class GroupName(Enum):
    ADMIN = "Admin"
    RADIOGRAPHER = "Radiographer"
    COORDINATOR = "Coordinator"
    RADIOLOGIST = "Radiologist"
    INDEPENDENT_RADIOLOGIST = "Independent Radiologist"
    PHYSICIAN = 'Physician'

    @staticmethod
    def groups_having_user_profile():
        return [
            GroupName.RADIOGRAPHER.value,
            GroupName.COORDINATOR.value,
            GroupName.RADIOLOGIST.value,
            GroupName.PHYSICIAN.value,
        ]

    @staticmethod
    def get_group_name_short_form(group_name):
        GROUP_NAME_SHORT_FORM = {
            GroupName.ADMIN.value: "Admin",
            GroupName.RADIOGRAPHER.value: "RG",
            GroupName.COORDINATOR.value: "COD",
            GroupName.RADIOLOGIST.value: "RL",
            GroupName.INDEPENDENT_RADIOLOGIST.value: "IR",
            GroupName.PHYSICIAN.value: "PHY",
        }

        return GROUP_NAME_SHORT_FORM[group_name]

    @classmethod
    def choices(cls):
        return ((choice.name, choice.value) for choice in GroupName)


class ViewMethod(Enum):
    TIMELINE_LIST = "timeline_list"
    ASSIGNMENT_LIST = "assignment_list"
    ASSIGNMENT_CREATE = "assignment_create"
    ASSIGNMENT_UPDATE = "assignment_update"
    ASSIGNMENT_STATUS = "assignment_status"
    REPORT_FETCH = "report_fetch"
    REPORT_CREATE = "report_create"
    REPORT_UPDATE = "report_update"
    STUDY_LIST = "study_list"
    STUDY_FETCH = "study_fetch"
    STUDY_UPDATE = "study_update"
    STUDY_DELETE = "study_delete"
    STUDY_TIMELINE_LIST = "study_timeline_list"
    PROCEDURE_FETCH = "procedure_fetch"
    REPORT_TEMPLATE_FETCH = "report_template_fetch"

    authorized_groups = {
        TIMELINE_LIST: [
            GroupName.COORDINATOR.value,
            GroupName.RADIOLOGIST.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value
        ],
        ASSIGNMENT_LIST: [
            GroupName.COORDINATOR.value,
            GroupName.RADIOLOGIST.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value
        ],
        ASSIGNMENT_CREATE: [
            GroupName.RADIOGRAPHER.value,
            GroupName.RADIOLOGIST.value
        ],
        ASSIGNMENT_UPDATE: [
            GroupName.RADIOGRAPHER.value,
            GroupName.COORDINATOR.value,
            GroupName.RADIOLOGIST.value
        ],
        ASSIGNMENT_STATUS: [
            GroupName.RADIOLOGIST.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value
        ],
        REPORT_FETCH: [
            GroupName.RADIOGRAPHER.value,
            GroupName.RADIOLOGIST.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value,
            GroupName.PHYSICIAN.value,
        ],
        REPORT_CREATE: [
            GroupName.RADIOLOGIST.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value
        ],
        REPORT_UPDATE: [
            GroupName.RADIOLOGIST.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value
        ],
        STUDY_LIST: [
            GroupName.RADIOGRAPHER.value,
            GroupName.RADIOLOGIST.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value
        ],
        STUDY_FETCH: [
            GroupName.RADIOGRAPHER.value,
            GroupName.RADIOLOGIST.value,
            GroupName.PHYSICIAN.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value
        ],
        STUDY_UPDATE: [
            GroupName.RADIOGRAPHER.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value
        ],
        STUDY_DELETE: [
            GroupName.RADIOGRAPHER.value
        ],
        STUDY_TIMELINE_LIST: [
            GroupName.RADIOGRAPHER.value,
            GroupName.RADIOLOGIST.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value
        ],
        PROCEDURE_FETCH: [
            GroupName.RADIOGRAPHER.value,
            GroupName.RADIOLOGIST.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value
        ],
        REPORT_TEMPLATE_FETCH: [
            GroupName.RADIOLOGIST.value,
            GroupName.INDEPENDENT_RADIOLOGIST.value
        ],
    }

    @staticmethod
    def get_authorized_groups_by_view_method(view_method_name):
        return ViewMethod.authorized_groups.value[view_method_name]


def get_group_count(user):
    return user.groups.all().count()


def get_group_name(user):
    return user.groups.all()[0].name


def has_user_profile(user):
    validation = True
    group_name = get_group_name(user)
    groups = GroupName.groups_having_user_profile()

    if group_name in groups and not hasattr(user, 'profile'):
        validation = False

    return validation


def group_validation(user, authorized_group_list):
    validation = True

    if not get_group_count(user):
        validation = False

    if get_group_name(user) is None:
        validation = False

    group_name = get_group_name(user)

    if group_name not in authorized_group_list:
        validation = False

    return validation


def serializer_error_mapping(errors):
    error_list = []

    for field, message in errors.items():
        error_list.append({
            "field": field,
            "message": message[0]
        })

    return error_list


def get_object_by_guid(model, guid):
    try:
        if model.__name__ == "User":
            obj = model.objects.get(profile__guid=guid)
        else:
            obj = model.objects.get(guid=guid)
        return obj
    except model.DoesNotExist:
        raise DoesNotExist("{model} does not exist".format(
            model=model.__name__
        ))
    except ValidationError:
        raise InvalidUUID()


def get_request_query_params(request_query_params, multiple_choice_filter_list):
    for item in multiple_choice_filter_list:
        if request_query_params.get(item):
            request_query_params[item] = request_query_params[item][0].split(",")
    return request_query_params


def is_physician(user):
    assert user is not None, 'User object should not be None'
    return user.groups.filter(name=GroupName.PHYSICIAN.value).count() > 0