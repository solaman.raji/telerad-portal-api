from telerad_portal_api.settings import ENV_TYPE

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'telerad_portal_dev',
        'USER': 'caretaker',
        'PASSWORD': 'zxc999zxc',
        'HOST': 'telerad-portal-dev-03.cl6dtc2x2soy.ap-southeast-1.rds.amazonaws.com',
        'PORT': '5432',
    }
}

ALLOWED_HOSTS = [
    '127.0.0.1',
    'localhost',
    'zappa',
    '.execute-api.ap-southeast-1.amazonaws.com',
    'dev-telerad-portal-api.alem.health',
]

AWS_STORAGE_BUCKET_NAME = f'telerad-portal-api-static-{ENV_TYPE}'

AWS_S3_CUSTOM_DOMAIN = f'{AWS_STORAGE_BUCKET_NAME}.s3.amazonaws.com'

STATIC_URL = f"https://{AWS_S3_CUSTOM_DOMAIN}/"

STATICFILES_LOCATION = 'static'
STATICFILES_STORAGE = 'custom_storages.StaticStorage'

MEDIA_URL = f"https://{AWS_S3_CUSTOM_DOMAIN}/"

MEDIAFILES_LOCATION = 'media'
DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'

TINYMCE_JS_URL = f"{STATIC_URL}{STATICFILES_LOCATION}/tiny_mce/tiny_mce.js"

AWS_AUTO_CREATE_BUCKET = True

AWS_ACCESS_KEY_ID = 'AKIAIEIZOMJHBR244G5A'
AWS_SECRET_ACCESS_KEY = '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'
AWS_REGION = 'ap-southeast-1'
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=3600',
}

VIEWER_URL = "http://dev-dcm4che-3.cloud.alemhealth.com/alemviewer/index.php?study={}"

AH_CONNECT_UPSTREAM_BUCKET = 'ah-connect-upstream-dev'

ENALBLE_SENTRY_LOGGING = True
RAVEN_CONFIG = {
    'dsn': 'http://4069b9ee29904c298cd849803eefefaa:d3b4de1b251f45ec923b347991800cdb@sentry.cloud.alemhealth.com:9000/2',
}

INVITATION_URL = "https://dev.connect.alem.health/signup/{invitation_token}"
RESET_PASSWORD_URL = "https://dev.connect.alem.health/reset-password/{reset_password_token}"
