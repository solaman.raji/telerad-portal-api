from calendar import timegm
from datetime import datetime

from rest_framework_jwt.settings import api_settings


def jwt_payload_handler(user):
    return {
        'guid': str(user.profile.guid) if hasattr(user, "profile") else None,
        'username': user.username,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'email': user.email,
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA,
        'user_group': user.groups.all()[0].name if user.groups.all().count() else None,
        'orig_iat': timegm(
            datetime.utcnow().utctimetuple()
        )
    }
