from rest_framework.exceptions import APIException
from rest_framework import status


class AlreadyExists(APIException):
    status_code = status.HTTP_409_CONFLICT
    default_detail = 'Object already exists.'
    default_code = 'already_exists'


class DoesNotExist(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'Object does not exist.'
    default_code = 'does_not_exist'


class InvalidUUID(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Invalid UUID for guid'
    default_code = 'invalid_uuid'


class InvalidParameter(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Invalid parameter.'
    default_code = 'invalid_parameter'


class UnauthorizedAccess(APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = 'Unauthorized access.'
    default_code = 'unauthorized_access'


class NoAssignment(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = 'Assignment for this study has already created.'
    default_code = 'no_assignment'


class InvalidStatus(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = 'Invalid status.'
    default_code = 'invalid_status'
