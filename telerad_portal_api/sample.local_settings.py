import os

from telerad_portal_api.settings import BASE_DIR

# Need to add configuration to use PostGres docker
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

SECRET_KEY = '70=1x4ej@59*e^x_02-0fu^@rrj!u&)bw974sk!-*yonoa*ge6'

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")

# Media Files
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

AWS_ACCESS_KEY_ID = ''
AWS_SECRET_ACCESS_KEY = ''
AWS_REGION = 'ap-southeast-1'
AH_CONNECT_UPSTREAM_BUCKET = 'ah-connect-upstream-local'


VIEWER_URL = "http://localhost:8080/alemviewer/index.php?study={}"
INVITATION_URL = ""
RESET_PASSWORD_URL = ""
